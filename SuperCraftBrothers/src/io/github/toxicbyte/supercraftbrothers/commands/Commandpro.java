/*  1:   */ package io.github.toxicbyte.supercraftbrothers.commands;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.PlayerStats;

/*  4:   */ import org.bukkit.Bukkit;
/*  5:   */ import org.bukkit.command.Command;
/*  6:   */ import org.bukkit.command.CommandExecutor;
/*  7:   */ import org.bukkit.command.CommandSender;
/*  8:   */ 
/*  9:   */ public class Commandpro
/* 10:   */   implements CommandExecutor
/* 11:   */ {
/* 12:   */   @SuppressWarnings("deprecation")
public boolean onCommand(CommandSender cs, Command cmd, String string, String[] strings)
/* 13:   */   {
/* 14:15 */     if (cs.hasPermission("scb.pro"))
/* 15:   */     {
/* 16:17 */       if ((strings.length > 1) || (strings.length < 1))
/* 17:   */       {
/* 18:18 */         cs.sendMessage("[§aSCB§r] Usage: /pro <player>");
/* 19:   */       }
/* 20:21 */       else if (Bukkit.getPlayer(strings[0]) == null)
/* 21:   */       {
/* 22:22 */         cs.sendMessage("[§aSCB§r] Player not found!");
/* 23:   */       }
/* 24:   */       else
/* 25:   */       {
/* 26:25 */         PlayerStats.setPlayerRank(strings[0], 3);
/* 27:26 */         cs.sendMessage("[§aSCB§r] Rank Updated");
/* 28:   */       }
/* 29:   */     }
/* 30:   */     else {
/* 31:31 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 32:   */     }
/* 33:32 */     return true;
/* 34:   */   }
/* 35:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.commands.Commandpro
 * JD-Core Version:    0.7.0.1
 */