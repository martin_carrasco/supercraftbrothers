/*  1:   */ package io.github.toxicbyte.supercraftbrothers.commands;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.SCBGameManager;

/*  4:   */ import org.bukkit.command.Command;
/*  5:   */ import org.bukkit.command.CommandExecutor;
/*  6:   */ import org.bukkit.command.CommandSender;
/*  7:   */ 
/*  8:   */ public class Commandcreatearena
/*  9:   */   implements CommandExecutor
/* 10:   */ {
/* 11:   */   public boolean onCommand(CommandSender cs, Command cmd, String string, String[] strings)
/* 12:   */   {
/* 13:14 */     if (cs.hasPermission("scb.createarena"))
/* 14:   */     {
/* 15:16 */       if ((strings.length > 1) || (strings.length == 0))
/* 16:   */       {
/* 17:17 */         cs.sendMessage("[§aSCB§r] Usage: /createarena <map name>");
/* 18:   */       }
/* 19:   */       else
/* 20:   */       {
/* 21:20 */         String mapName = strings[0];
/* 22:21 */         if (SCBGameManager.getInstance().getGame(mapName) != null)
/* 23:   */         {
/* 24:22 */           cs.sendMessage("[§aSCB§r] Map already exists");
/* 25:   */         }
/* 26:   */         else
/* 27:   */         {
/* 28:25 */           SCBGameManager.getInstance().createGame(mapName);
/* 29:26 */           cs.sendMessage("[§aSCB§r] Created map " + mapName);
/* 30:   */         }
/* 31:   */       }
/* 32:   */     }
/* 33:   */     else {
/* 34:31 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 35:   */     }
/* 36:32 */     return true;
/* 37:   */   }
/* 38:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.commands.Commandcreatearena
 * JD-Core Version:    0.7.0.1
 */