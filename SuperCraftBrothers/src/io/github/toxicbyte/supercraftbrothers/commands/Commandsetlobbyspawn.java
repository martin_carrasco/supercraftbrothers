/*  1:   */ package io.github.toxicbyte.supercraftbrothers.commands;
/*  4:   */ import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.SCBMap;

/*  6:   */ import org.bukkit.command.Command;
/*  7:   */ import org.bukkit.command.CommandExecutor;
/*  8:   */ import org.bukkit.command.CommandSender;
/*  9:   */ import org.bukkit.entity.Player;
/* 10:   */ 
/* 11:   */ public class Commandsetlobbyspawn
/* 12:   */   implements CommandExecutor
/* 13:   */ {
/* 14:   */   public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings)
/* 15:   */   {
/* 16:15 */     if (cs.hasPermission("scb.setlobbyspawn"))
/* 17:   */     {
/* 18:17 */       if (!(cs instanceof Player))
/* 19:   */       {
/* 20:18 */         cs.sendMessage("[SCB] Command only usable by player");
/* 21:   */       }
/* 22:   */       else
/* 23:   */       {
/* 24:21 */         Player player = (Player)cs;
/* 25:22 */         if ((strings.length > 1) || (strings.length < 1))
/* 26:   */         {
/* 27:24 */           cs.sendMessage("[§aSCB§r] Usage: /setlobbyspawn <map name>");
/* 28:   */         }
/* 29:   */         else
/* 30:   */         {
/* 31:28 */           String mapName = strings[0];
/* 32:29 */           if (SCBGameManager.getInstance().getGame(mapName) == null)
/* 33:   */           {
/* 34:31 */             cs.sendMessage("[§aSCB§r] Map doesn't exist");
/* 35:32 */             return true;
/* 36:   */           }
/* 37:34 */           SCBMap map = SCBGameManager.getInstance().getGame(mapName).getMap();
/* 38:35 */           if (map == null)
/* 39:   */           {
/* 40:37 */             cs.sendMessage("[§aSCB§r] Map doesn't exist");
/* 41:   */           }
/* 42:   */           else
/* 43:   */           {
/* 44:41 */             map.setClassLobby(player.getLocation());
/* 45:42 */             cs.sendMessage("[§aSCB§r] Set the lobby of " + mapName);
/* 46:   */           }
/* 47:   */         }
/* 48:   */       }
/* 49:   */     }
/* 50:   */     else {
/* 51:48 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 52:   */     }
/* 53:49 */     return true;
/* 54:   */   }
/* 55:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.commands.Commandsetlobbyspawn
 * JD-Core Version:    0.7.0.1
 */