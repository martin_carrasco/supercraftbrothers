/*  1:   */ package io.github.toxicbyte.supercraftbrothers.commands;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.PlayerStats;

/*  4:   */ import org.bukkit.Bukkit;
/*  5:   */ import org.bukkit.command.Command;
/*  6:   */ import org.bukkit.command.CommandExecutor;
/*  7:   */ import org.bukkit.command.CommandSender;
/*  9:   */ 
/* 10:   */ public class Commanddefault
/* 11:   */   implements CommandExecutor
/* 12:   */ {
/* 13:   */   @SuppressWarnings("deprecation")
public boolean onCommand(CommandSender cs, Command cmd, String string, String[] strings)
/* 14:   */   {
/* 15:15 */     if (cs.hasPermission("scb.default"))
/* 16:   */     {
/* 17:17 */       if ((strings.length > 1) || (strings.length < 1))
/* 18:   */       {
/* 19:18 */         cs.sendMessage("[§aSCB§r] Usage: /default <player>");
/* 20:   */       }
/* 21:21 */       else if (Bukkit.getPlayer(strings[0]) == null)
/* 22:   */       {
/* 23:22 */         cs.sendMessage("[§aSCB§r] Player not found!");
/* 24:   */       }
/* 25:   */       else
/* 26:   */       {
/* 27:25 */         PlayerStats.setPlayerRank(strings[0], 1);
/* 28:26 */         cs.sendMessage("[§aSCB§r] Rank Updated");
/* 29:27 */         Bukkit.getPlayer(strings[0]).setAllowFlight(false);
/* 30:   */       }
/* 31:   */     }
/* 32:   */     else {
/* 33:32 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 34:   */     }
/* 35:33 */     return true;
/* 36:   */   }
/* 37:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.commands.Commanddefault
 * JD-Core Version:    0.7.0.1
 */