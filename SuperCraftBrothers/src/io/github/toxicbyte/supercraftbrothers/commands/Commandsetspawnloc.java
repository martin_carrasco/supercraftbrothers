/*  1:   */ package io.github.toxicbyte.supercraftbrothers.commands;
/*  4:   */ import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.SCBMap;

/*  6:   */ import org.bukkit.command.Command;
/*  7:   */ import org.bukkit.command.CommandExecutor;
/*  8:   */ import org.bukkit.command.CommandSender;
/*  9:   */ import org.bukkit.entity.Player;
/* 10:   */ 
/* 11:   */ public class Commandsetspawnloc
/* 12:   */   implements CommandExecutor
/* 13:   */ {
	private int i;
/* 14:   */   public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings)
/* 15:   */   {
/* 16:15 */     if (cs.hasPermission("scb.givegem"))
/* 17:   */     {
/* 18:17 */       if (!(cs instanceof Player))
/* 19:   */       {
/* 20:18 */         cs.sendMessage("[SCB] Command only usable by player");
/* 21:   */       }
/* 22:   */       else
/* 23:   */       {
/* 24:21 */         Player player = (Player)cs;
/* 25:22 */         if ((strings.length > 2) || (strings.length < 1))
/* 26:   */         {
/* 27:24 */           cs.sendMessage("Usage: /setspawnloc <map name> <1|2|3|4>");
/* 28:   */         }
/* 29:   */         else
/* 30:   */         {
/* 31:   */           try
/* 32:   */           {
/* 33:31 */             i = Integer.parseInt(strings[1]);
/* 34:   */           }
/* 35:   */           catch (NumberFormatException ex)
/* 36:   */           {
/* 38:35 */             cs.sendMessage("[§aSCB§r] '" + strings[1] + "' is not a valid number");
/* 39:36 */             return true;
/* 40:   */           }
/* 42:38 */           if ((i > 0) && (i < 5))
/* 43:   */           {
/* 44:40 */             String mapName = strings[0];
/* 45:41 */             if (SCBGameManager.getInstance().getGame(mapName) == null)
/* 46:   */             {
/* 47:43 */               cs.sendMessage("[§SCB§r] Map doesn't exist");
/* 48:44 */               return true;
/* 49:   */             }
/* 50:46 */             SCBMap map = SCBGameManager.getInstance().getGame(mapName).getMap();
/* 51:47 */             if (map == null) {
/* 52:49 */               cs.sendMessage("[§aSCB§r] Map doesn't exist");
/* 53:   */             } else {
/* 54:53 */               switch (i)
/* 55:   */               {
/* 56:   */               case 1: 
/* 57:56 */                 map.setSp1(player.getLocation());
/* 58:57 */                 break;
/* 59:   */               case 2: 
/* 60:59 */                 map.setSp2(player.getLocation());
/* 61:60 */                 break;
/* 62:   */               case 3: 
/* 63:62 */                 map.setSp3(player.getLocation());
/* 64:63 */                 break;
/* 65:   */               case 4: 
/* 66:65 */                 map.setSp4(player.getLocation());
/* 67:66 */                 break;
/* 68:   */               default: 
/* 69:68 */                 cs.sendMessage("[§aSCB§r] Wow. How the heck did you get this message? You're amazing. Please tell me how you did it.");
/* 70:   */               }
/* 71:   */             }
/* 72:71 */             cs.sendMessage("[§aSCB§r] Created spawnpoint " + i + " for map " + mapName);
/* 73:   */           }
/* 74:   */           else
/* 75:   */           {
/* 76:75 */             cs.sendMessage("[§aSCB§r] " + i + " is not between 1 and 4");
/* 77:   */           }
/* 78:   */         }
/* 79:   */       }
/* 80:   */     }
/* 81:   */     else {
/* 82:81 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 83:   */     }
/* 84:82 */     return true;
/* 85:   */   }
/* 86:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.commands.Commandsetspawnloc
 * JD-Core Version:    0.7.0.1
 */