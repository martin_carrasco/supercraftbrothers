/*  1:   */ package io.github.toxicbyte.supercraftbrothers.commands;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.PlayerStats;

/*  4:   */ import org.bukkit.Bukkit;
/*  5:   */ import org.bukkit.command.Command;
/*  6:   */ import org.bukkit.command.CommandExecutor;
/*  7:   */ import org.bukkit.command.CommandSender;
/*  8:   */ 
/*  9:   */ public class Commandgivegem
/* 10:   */   implements CommandExecutor
/* 11:   */ {
/* 12:   */   @SuppressWarnings("deprecation")
public boolean onCommand(CommandSender cs, Command cmd, String string, String[] strings)
/* 13:   */   {
/* 14:15 */     if (cs.hasPermission("scb.givegem"))
/* 15:   */     {
/* 16:17 */       if ((strings.length > 2) || (strings.length < 1))
/* 17:   */       {
/* 18:18 */         cs.sendMessage("[§aSCB§r] Usage: /givegem <player> <gems>");
/* 19:   */       }
/* 20:21 */       else if (Bukkit.getPlayer(strings[0]) == null)
/* 21:   */       {
/* 22:22 */         cs.sendMessage("[§aSCB§r] Player not found!");
/* 23:   */       }
/* 24:   */       else
/* 25:   */       {
/* 26:25 */         int g = 0;
/* 27:   */         try
/* 28:   */         {
/* 29:27 */           g = Integer.parseInt(strings[1]);
/* 30:   */         }
/* 31:   */         catch (NumberFormatException e)
/* 32:   */         {
/* 33:29 */           cs.sendMessage("[§aSCB§r] '" + strings[1] + "' is not a valid number");
/* 34:30 */           return true;
/* 35:   */         }
/* 36:32 */         PlayerStats.setPlayerGems(strings[0], g, 1, true);
/* 37:33 */         cs.sendMessage("[§aSCB§r] Gems Updated");
/* 38:   */       }
/* 39:   */     }
/* 40:   */     else {
/* 41:38 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 42:   */     }
/* 43:39 */     return true;
/* 44:   */   }
/* 45:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.commands.Commandgivegem
 * JD-Core Version:    0.7.0.1
 */