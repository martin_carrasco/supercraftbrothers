/*   1:    */ package io.github.toxicbyte.supercraftbrothers.commands;
/*   2:    */ 
/*   3:    */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.PlayerStats;
import io.github.toxicbyte.supercraftbrothers.SCBGame;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.SuperCraftBrothers;

/*   9:    */ import java.util.ArrayList;
/*  12:    */ import java.util.List;

/*  13:    */ import org.bukkit.ChatColor;
/*  15:    */ import org.bukkit.command.CommandSender;
/*  16:    */ import org.bukkit.entity.Player;
/*  18:    */ 
/*  19:    */ public class SubCommands
/*  20:    */ {
/*  21:    */   private static SuperCraftBrothers scb;
/*  22:    */   
/*  23:    */   public static void setup(SuperCraftBrothers scb)
/*  24:    */   {
/*  25: 20 */     SubCommands.scb = scb;
/*  26:    */   }
/*  27:    */   
/*  28:    */   public static void help(CommandSender p)
/*  29:    */   {
/*  30: 24 */     p.sendMessage("[§aSCB§r]");
/*  31: 25 */     p.sendMessage("/createarena <arena> - Create an arena");
/*  32: 26 */     p.sendMessage("/deletearena <arena> - Delete an arena (BETA)");
/*  33: 27 */     p.sendMessage("/setlobbyspawn <arena> - Set lobby spawn, not the main lobby");
/*  34: 28 */     p.sendMessage("/setspawnloc <arena> <1|2|3|4> - Set a spawn point");
/*  35: 29 */     p.sendMessage("/setmainlobby - Set the main lobby");
/*  36: 30 */     p.sendMessage("/givegem <player> <gem> - Give players gems");
/*  37: 31 */     p.sendMessage("/default <player> - Set to DEFAULT");
/*  38: 32 */     p.sendMessage("/vip <player> - Set to VIP");
/*  39: 33 */     p.sendMessage("/pro <player> - Set to PRO");
/*  40: 34 */     p.sendMessage("---------------------------------------");
/*  41: 35 */     p.sendMessage("/scb leave - Leave the arena (BETA)");
/*  42: 36 */     p.sendMessage("/scb disable <arena> - Disable an arena (BETA)");
/*  43: 37 */     p.sendMessage("/scb enable <arena> - Enable an arena (BETA)");
/*  44: 38 */     p.sendMessage("/scb start - Force start the arena (BETA)");
/*  45: 39 */     p.sendMessage("/scb stop - Force stop the arena (BETA)");
/*  46:    */   }
/*  47:    */   
/*  48:    */   public static void leave(CommandSender cs)
/*  49:    */   {
/*  50: 44 */     if (!(cs instanceof Player))
/*  51:    */     {
/*  52: 45 */       cs.sendMessage("[§aSCB§r] This command may only be used by a player");
/*  53: 46 */       return;
/*  54:    */     }
/*  55: 50 */     Player p = (Player)cs;
/*  56: 51 */     CraftBrother cBro = SCBGameManager.getInstance().getCraftBrother(p);
/*  57: 52 */     if (cBro != null)
/*  58:    */     {
/*  59: 54 */       SCBGame game = cBro.getCurrentGame();
/*  60: 55 */       if (game != null)
/*  61:    */       {
/*  62: 57 */         if (game.isInGame()) {
/*  63: 59 */           game.leaveGame(p, false);
/*  64:    */         }
/*  65:    */       }
/*  66:    */       else {
/*  67: 63 */         p.sendMessage(ChatColor.GREEN + "You are not in a game!");
/*  68:    */       }
/*  69:    */     }
/*  70:    */     else
/*  71:    */     {
/*  72: 66 */       p.sendMessage(ChatColor.GREEN + "You are not in a game!");
/*  73:    */     }
/*  74:    */   }
/*  75:    */   
/*  76:    */   public static void start(CommandSender cs, String arena)
/*  77:    */   {
/*  78: 72 */     if ((cs instanceof Player))
/*  79:    */     {
/*  80: 74 */       if (cs.hasPermission("scb.start"))
/*  81:    */       {
/*  82: 76 */         Player p = (Player)cs;
/*  83: 77 */         CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(p);
/*  84: 78 */         if (arena == null)
/*  85:    */         {
/*  86: 80 */           if ((bro != null) && (bro.getCurrentGame() != null)) {
/*  87: 82 */             bro.getCurrentGame().startGame();
/*  88:    */           }
/*  89:    */         }
/*  90: 87 */         else if (SCBGameManager.getInstance().getGame(arena) != null)
/*  91:    */         {
/*  92: 89 */           SCBGameManager.getInstance().getGame(arena).startLobbyCountdown();
/*  93: 90 */           cs.sendMessage("[§aSCB§r] The game " + arena + " has been started");
/*  94:    */         }
/*  95:    */         else
/*  96:    */         {
/*  97: 93 */           cs.sendMessage("[§aSCB§r] Arena not found");
/*  98:    */         }
/*  99:    */       }
/* 100:    */       else
/* 101:    */       {
/* 102: 97 */         cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 103:    */       }
/* 104:    */     }
/* 105:101 */     else if (arena == null)
/* 106:    */     {
/* 107:102 */       cs.sendMessage("[SCB] No arena specified");
/* 108:    */     }
/* 109:105 */     else if (SCBGameManager.getInstance().getGame(arena) == null)
/* 110:    */     {
/* 111:106 */       cs.sendMessage("[SCB] Arena not found");
/* 112:    */     }
/* 113:    */     else
/* 114:    */     {
/* 115:109 */       SCBGameManager.getInstance().getGame(arena).startLobbyCountdown();
/* 116:110 */       cs.sendMessage("[SCB] The game " + arena + " has been started");
/* 117:    */     }
/* 118:    */   }
/* 119:    */   
/* 120:    */   public static void stop(CommandSender cs, String arena)
/* 121:    */   {
/* 122:118 */     if ((cs instanceof Player))
/* 123:    */     {
/* 124:120 */       if (cs.hasPermission("scb.stop"))
/* 125:    */       {
/* 126:122 */         Player p = (Player)cs;
/* 127:123 */         CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(p);
/* 128:124 */         if (arena == null)
/* 129:    */         {
/* 130:126 */           if ((bro != null) && (bro.getCurrentGame() != null)) {
/* 131:128 */             bro.getCurrentGame().stopGame();
/* 132:    */           } else {
/* 133:132 */             p.sendMessage("[§aSCB§r] You are not in a game!");
/* 134:    */           }
/* 135:    */         }
/* 136:137 */         else if (SCBGameManager.getInstance().getGame(arena) != null)
/* 137:    */         {
/* 138:139 */           SCBGameManager.getInstance().getGame(arena).stopGame();
/* 139:140 */           cs.sendMessage("[§aSCB§r] The game " + arena + " has been stopped");
/* 140:    */         }
/* 141:    */         else
/* 142:    */         {
/* 143:143 */           cs.sendMessage("[§aSCB§r] Arena not found");
/* 144:    */         }
/* 145:    */       }
/* 146:    */       else
/* 147:    */       {
/* 148:147 */         cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 149:    */       }
/* 150:    */     }
/* 151:151 */     else if (arena == null)
/* 152:    */     {
/* 153:152 */       cs.sendMessage("[SCB] No arena specified");
/* 154:    */     }
/* 155:155 */     else if (SCBGameManager.getInstance().getGame(arena) == null)
/* 156:    */     {
/* 157:156 */       cs.sendMessage("[SCB] Arena not found");
/* 158:    */     }
/* 159:    */     else
/* 160:    */     {
/* 161:159 */       SCBGameManager.getInstance().getGame(arena).stopGame();
/* 162:160 */       cs.sendMessage("[SCB] The game " + arena + " has been stopped");
/* 163:    */     }
/* 164:    */   }
/* 165:    */   
/* 166:    */   public static void tp(CommandSender cs, String mapname)
/* 167:    */   {
/* 168:168 */     if (!(cs instanceof Player))
/* 169:    */     {
/* 170:169 */       cs.sendMessage("[§aSCB§r] This command may only be used by a player");
/* 171:170 */       return;
/* 172:    */     }
/* 173:174 */     if (cs.hasPermission("scb.tp"))
/* 174:    */     {
/* 175:176 */       Player p = (Player)cs;
/* 176:177 */       if (mapname == null)
/* 177:    */       {
/* 178:178 */         p.sendMessage("[§aSCB§r] Usage: /scb tp <mapname>");
/* 179:    */       }
/* 180:    */       else
/* 181:    */       {
/* 182:181 */         SCBGame game = SCBGameManager.getInstance().getGame(mapname);
/* 183:182 */         if (game != null) {
/* 184:183 */           p.teleport(game.getMap().getClassLobby());
/* 185:    */         } else {
/* 186:185 */           p.sendMessage("[§aSCB§r] Map not found!");
/* 187:    */         }
/* 188:    */       }
/* 189:    */     }
/* 190:    */     else
/* 191:    */     {
/* 192:189 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 193:    */     }
/* 194:    */   }
/* 195:    */   
/* 196:    */   public static void enableArena(CommandSender cs, String mapname)
/* 197:    */   {
/* 198:195 */     if (mapname == null)
/* 199:    */     {
/* 200:196 */       cs.sendMessage("[§aSCB§r] Usage: /scb enable <mapname>");
/* 201:    */     }
/* 202:199 */     else if (cs.hasPermission("scb.enable"))
/* 203:    */     {
/* 204:201 */       SCBGame game = SCBGameManager.getInstance().getGame(mapname);
/* 205:202 */       if (game != null)
/* 206:    */       {
/* 207:204 */         game.enableArena();
/* 208:205 */         cs.sendMessage("[§aSCB§r] Map Enabled");
/* 209:    */       }
/* 210:    */       else
/* 211:    */       {
/* 212:208 */         cs.sendMessage("[§aSCB§r] Map not found!");
/* 213:    */       }
/* 214:    */     }
/* 215:    */     else
/* 216:    */     {
/* 217:211 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 218:    */     }
/* 219:    */   }
/* 220:    */   
/* 221:    */   public static void disableArena(CommandSender cs, String mapname)
/* 222:    */   {
/* 223:217 */     if (mapname == null)
/* 224:    */     {
/* 225:218 */       cs.sendMessage("[§aSCB§r] Usage: /scb disable <mapname>");
/* 226:    */     }
/* 227:221 */     else if (cs.hasPermission("scb.disable"))
/* 228:    */     {
/* 229:223 */       SCBGame game = SCBGameManager.getInstance().getGame(mapname);
/* 230:224 */       if (game != null)
/* 231:    */       {
/* 232:226 */         game.disableArena();
/* 233:227 */         cs.sendMessage("[§aSCB§r] Map Disabled");
/* 234:    */       }
/* 235:    */       else
/* 236:    */       {
/* 237:230 */         cs.sendMessage("[§aSCB§r] Map not found!");
/* 238:    */       }
/* 239:    */     }
/* 240:    */     else
/* 241:    */     {
/* 242:233 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 243:    */     }
/* 244:    */   }
/* 245:    */   
/* 246:    */   public static void reload(CommandSender cs)
/* 247:    */   {
/* 248:239 */     if (cs.hasPermission("scb.reload")) {
/* 249:240 */       SuperCraftBrothers.getInstance().reloadCfg();
/* 250:    */     } else {
/* 251:242 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 252:    */     }
/* 253:    */   }
/* 254:    */   
/* 259:    */   
/* 260:    */   @SuppressWarnings("unchecked")
public static void debug(CommandSender cs, String arg1, String arg2, String arg3)
/* 261:    */   {
/* 262:252 */     int enabled = 0;
/* 263:253 */     int disabled = 0;
/* 264:254 */     for (SCBGame game : SCBGameManager.getInstance().getAllGames()) {
/* 265:256 */       if (game.isDisabled()) {
/* 266:257 */         disabled++;
/* 267:    */       } else {
/* 268:259 */         enabled++;
/* 269:    */       }
/* 270:    */     }
/* 271:261 */     if (arg1 == null)
/* 272:    */     {
/* 273:263 */       cs.sendMessage("[§aSCB§r] This server is running SCB version " + scb.getDescription().getVersion());
/* 274:264 */       cs.sendMessage("[§aSCB§r] Implementing Bukkit API " + scb.getServer().getVersion().toString());
/* 275:265 */       cs.sendMessage("[§aSCB§r] There are currently " + SCBGameManager.getInstance().getAllGames().size() + " games");
/* 276:266 */       cs.sendMessage("[§aSCB§r] " + Integer.toString(enabled) + " are enabled, and " + Integer.toString(disabled) + " are disabled");
/* 277:267 */       cs.sendMessage("[§aSCB§r] Type /scb debug games for more info on games");
/* 278:    */     }
/* 279:269 */     else if (arg1.equalsIgnoreCase("games"))
/* 280:    */     {
/* 281:271 */       List<String> gamenames = new ArrayList<String>();
/* 282:272 */       for (SCBGame game : SCBGameManager.getInstance().getAllGames()) {
/* 283:274 */         gamenames.add(game.getName());
/* 284:    */       }
/* 285:276 */       cs.sendMessage("[§aSCB§r] There are currently " + SCBGameManager.getInstance().getAllGames().size() + " games.");
/* 286:277 */       cs.sendMessage("[§aSCB§r] The games are: " + gamenames.toString().replace('[', ' ').replace(']', ' '));
/* 287:278 */       cs.sendMessage("[§aSCB§r] Type /scb debug game <GameName> for more info on that arena");
/* 288:    */     }
/* 289:280 */     else if (arg1.equalsIgnoreCase("game"))
/* 290:    */     {
/* 291:282 */       if (arg2 == null)
/* 292:    */       {
/* 293:284 */         cs.sendMessage("[§aSCB§r] You did not specify a game!");
/* 294:    */       }
/* 295:288 */       else if (SCBGameManager.getInstance().getGame(arg2) == null)
/* 296:    */       {
/* 297:290 */         cs.sendMessage("[§aSCB§r] Game could not be found!");
/* 298:    */       }
/* 299:    */       else
/* 300:    */       {
/* 301:294 */         List<String> igs = new ArrayList<String>();
/* 302:295 */         Object ig = new ArrayList<Object>();
/* 303:296 */         SCBGame game = SCBGameManager.getInstance().getGame(arg2);
/* 304:297 */         for (CraftBrother bro : game.getHashMap(0).values()) {
/* 305:299 */           if (bro.getPlayer() != null) {
/* 306:300 */             igs.add(bro.getPlayer().getName());
/* 307:    */           }
/* 308:    */         }
/* 309:302 */         for (CraftBrother bro : game.getHashMap(1).values()) {
/* 310:304 */           if (bro.getPlayer() != null) {
/* 311:305 */             ((List<String>)ig).add(bro.getPlayer().getName());
/* 312:    */           }
/* 313:    */         }
/* 314:307 */         String status = null;
/* 315:308 */         if (game.isInLobby()) {
/* 316:309 */           status = "In Lobby";
/* 317:    */         }
/* 318:310 */         if (game.isInGame()) {
/* 319:311 */           status = "In Game";
/* 320:    */         }
/* 321:312 */         if (game.isDisabled()) {
/* 322:313 */           status = "Disabled";
/* 323:    */         }
/* 324:315 */         cs.sendMessage("[§aSCB§r] Game: " + game.getName());
/* 325:316 */         cs.sendMessage("[§aSCB§r] Status: " + status);
/* 326:317 */         cs.sendMessage("[§aSCB§r] Players in the game instance: " + game.getHashMap(0).size());
/* 327:318 */         cs.sendMessage("[§aSCB§r] Players in the game: " + game.getHashMap(1).size());
/* 328:319 */         cs.sendMessage("[§aSCB§r] Players in the game instance: " + igs.toString().replace('[', ' ').replace(']', ' '));
/* 329:320 */         cs.sendMessage("[§aSCB§r] Players in the game: " + ig.toString().replace('[', ' ').replace(']', ' '));
/* 330:    */       }
/* 331:    */     }
/* 332:324 */     else if (arg1.equalsIgnoreCase("player"))
/* 333:    */     {
/* 334:326 */       if (arg2 == null)
/* 335:    */       {
/* 336:328 */         cs.sendMessage("[§aSCB§r] You did not specify a player!");
/* 337:    */       }
/* 338:332 */       else if ((arg3 != null) && (arg3.equals("delete")))
/* 339:    */       {
/* 340:334 */         SCBGameManager.getInstance().removeCraftBrother(arg2);
/* 341:335 */         cs.sendMessage("[§aSCB§r] Player Removed!");
/* 342:    */       }
/* 343:    */       else
/* 344:    */       {
/* 345:339 */         boolean b = false;
/* 346:340 */         CraftBrother bro = null;
/* 347:341 */         if (SCBGameManager.getInstance().getCraftBrother(arg2) != null)
/* 348:    */         {
/* 349:343 */           b = true;
/* 350:344 */           bro = SCBGameManager.getInstance().getCraftBrother(arg2);
/* 351:    */         }
/* 352:346 */         cs.sendMessage("[§aSCB§r] Player: " + arg2);
/* 353:347 */         cs.sendMessage("[§aSCB§r] Rank: " + PlayerStats.getPlayerRank(arg2));
/* 354:348 */         cs.sendMessage("[§aSCB§r] Is in game: " + b);
/* 355:349 */         if (b) {
/* 356:351 */           cs.sendMessage("[§aSCB§r] Game: " + bro.getCurrentGame().getName());
/* 357:    */         }
/* 358:353 */         cs.sendMessage("[§aSCB§r] Gems: " + PlayerStats.getPlayerGems(arg2));
/* 359:354 */         Object characters = new ArrayList<Object>();
/* 360:355 */         characters = PlayerStats.getAllClasses(arg2);
/* 361:356 */         cs.sendMessage("[§aSCB§r] " + characters.toString().replace('[', ' ').replace(']', ' '));
/* 362:    */       }
/* 363:    */     }
/* 364:    */   }
/* 365:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.commands.SubCommands
 * JD-Core Version:    0.7.0.1
 */