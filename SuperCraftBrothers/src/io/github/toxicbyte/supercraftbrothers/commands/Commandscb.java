/*  1:   */ package io.github.toxicbyte.supercraftbrothers.commands;
/*  2:   */ 
/*  3:   */ import org.bukkit.command.Command;
/*  4:   */ import org.bukkit.command.CommandExecutor;
/*  5:   */ import org.bukkit.command.CommandSender;
/*  6:   */ 
/*  7:   */ public class Commandscb
/*  8:   */   implements CommandExecutor
/*  9:   */ {
/* 10: 9 */   String arg1 = null;
/* 11:10 */   String arg2 = null;
/* 12:11 */   String arg3 = null;
/* 13:12 */   String arg4 = null;
/* 14:   */   
/* 15:   */   public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings)
/* 16:   */   {
/* 17:17 */     if (strings.length != 0)
/* 18:   */     {
/* 19:19 */       if (strings.length >= 1) {
/* 20:20 */         this.arg1 = strings[0];
/* 21:   */       }
/* 22:21 */       if (strings.length >= 2) {
/* 23:22 */         this.arg2 = strings[1];
/* 24:   */       }
/* 25:23 */       if (strings.length >= 3) {
/* 26:24 */         this.arg3 = strings[2];
/* 27:   */       }
/* 28:25 */       if (strings.length >= 4) {
/* 29:26 */         this.arg4 = strings[3];
/* 30:   */       }
/* 31:28 */       if (this.arg1.equals("help")) {
/* 32:29 */         SubCommands.help(cs);
/* 33:30 */       } else if (this.arg1.equals("leave")) {
/* 34:31 */         SubCommands.leave(cs);
/* 35:32 */       } else if (this.arg1.equals("start")) {
/* 36:33 */         SubCommands.start(cs, this.arg2);
/* 37:34 */       } else if (this.arg1.equals("stop")) {
/* 38:35 */         SubCommands.stop(cs, this.arg2);
/* 39:36 */       } else if (this.arg1.equals("reload")) {
/* 40:37 */         SubCommands.reload(cs);
/* 41:38 */       } else if (this.arg1.equals("tp")) {
/* 42:39 */         SubCommands.tp(cs, this.arg2);
/* 43:40 */       } else if (this.arg1.equals("enable")) {
/* 44:41 */         SubCommands.enableArena(cs, this.arg2);
/* 45:42 */       } else if (this.arg1.equals("disable")) {
/* 46:43 */         SubCommands.disableArena(cs, this.arg2);
/* 47:44 */       } else if (this.arg1.equals("debug")) {
/* 50:47 */         SubCommands.debug(cs, this.arg2, this.arg3, this.arg4);
/* 51:   */       } else {
/* 52:49 */         cs.sendMessage("[SCB] Unknown command. Type /scb help for commands");
/* 53:   */       }
/* 54:   */     }
/* 55:   */     else
/* 56:   */     {
/* 57:53 */       cs.sendMessage("[SCB] v0.2.13-BETA");
/* 58:54 */       cs.sendMessage("Developed by: ToxicByte");
/* 59:   */     }
/* 60:56 */     return true;
/* 61:   */   }
/* 62:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.commands.Commandscb
 * JD-Core Version:    0.7.0.1
 */