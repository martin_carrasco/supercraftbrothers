/*  1:   */ package io.github.toxicbyte.supercraftbrothers.commands;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.PlayerStats;

/*  4:   */ import org.bukkit.Bukkit;
/*  5:   */ import org.bukkit.command.Command;
/*  6:   */ import org.bukkit.command.CommandExecutor;
/*  7:   */ import org.bukkit.command.CommandSender;
/*  8:   */ 
/*  9:   */ public class Commandvip
/* 10:   */   implements CommandExecutor
/* 11:   */ {
/* 12:   */   @SuppressWarnings("deprecation")
public boolean onCommand(CommandSender cs, Command cmd, String string, String[] strings)
/* 13:   */   {
/* 14:16 */     if (cs.hasPermission("scb.vip"))
/* 15:   */     {
/* 16:18 */       if ((strings.length > 1) || (strings.length < 1))
/* 17:   */       {
/* 18:19 */         cs.sendMessage("[§aSCB§r] Usage: /vip <player>");
/* 19:   */       }
/* 20:22 */       else if (Bukkit.getPlayer(strings[0]) == null)
/* 21:   */       {
/* 22:23 */         cs.sendMessage("[§aSCB§r] Player not found!");
/* 23:   */       }
/* 24:   */       else
/* 25:   */       {
/* 26:26 */         PlayerStats.setPlayerRank(strings[0], 2);
/* 27:27 */         cs.sendMessage("[§aSCB§r] Rank Updated");
/* 28:   */       }
/* 29:   */     }
/* 30:   */     else {
/* 31:32 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 32:   */     }
/* 33:33 */     return true;
/* 34:   */   }
/* 35:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.commands.Commandvip
 * JD-Core Version:    0.7.0.1
 */