/*  1:   */ package io.github.toxicbyte.supercraftbrothers.commands;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.SCBGameManager;

/*  4:   */ import org.bukkit.command.Command;
/*  5:   */ import org.bukkit.command.CommandExecutor;
/*  6:   */ import org.bukkit.command.CommandSender;
/*  7:   */ import org.bukkit.entity.Player;
/*  8:   */ 
/*  9:   */ public class Commandsetmainlobby
/* 10:   */   implements CommandExecutor
/* 11:   */ {
/* 12:   */   public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings)
/* 13:   */   {
/* 14:14 */     if (cs.hasPermission("scb.setlobbyspawn"))
/* 15:   */     {
/* 16:16 */       if (!(cs instanceof Player))
/* 17:   */       {
/* 18:17 */         cs.sendMessage("[SCB] Command only usable by player");
/* 19:   */       }
/* 20:   */       else
/* 21:   */       {
/* 22:20 */         Player player = (Player)cs;
/* 23:21 */         if (strings.length > 1)
/* 24:   */         {
/* 25:23 */           cs.sendMessage("[§aSCB§r] Usage: /setlobbyspawn");
/* 26:   */         }
/* 27:   */         else
/* 28:   */         {
/* 29:27 */           SCBGameManager.getInstance().setMainLobby(player.getLocation());
/* 30:28 */           player.sendMessage("[§aSCB§r] Set main lobby");
/* 31:   */         }
/* 32:   */       }
/* 33:   */     }
/* 34:   */     else {
/* 35:33 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 36:   */     }
/* 37:34 */     return true;
/* 38:   */   }
/* 39:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.commands.Commandsetmainlobby
 * JD-Core Version:    0.7.0.1
 */