/*  1:   */ package io.github.toxicbyte.supercraftbrothers.commands;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.SCBGameManager;

/*  4:   */ import org.bukkit.command.Command;
/*  5:   */ import org.bukkit.command.CommandExecutor;
/*  6:   */ import org.bukkit.command.CommandSender;
/*  7:   */ 
/*  8:   */ public class Commanddeletearena
/*  9:   */   implements CommandExecutor
/* 10:   */ {
/* 11:   */   public boolean onCommand(CommandSender cs, Command cmd, String string, String[] strings)
/* 12:   */   {
/* 13:10 */     if (cs.hasPermission("scb.deletearena"))
/* 14:   */     {
/* 15:12 */       if ((strings.length > 1) || (strings.length < 1))
/* 16:   */       {
/* 17:14 */         cs.sendMessage("[§aSCB§r] Usage: /deletearena <map name>");
/* 18:   */       }
/* 19:   */       else
/* 20:   */       {
/* 21:18 */         String mapName = strings[0];
/* 22:19 */         if (SCBGameManager.getInstance().getGame(mapName) == null)
/* 23:   */         {
/* 24:21 */           cs.sendMessage("[§aSCB§r] Map doesn't exist");
/* 25:   */         }
/* 26:   */         else
/* 27:   */         {
/* 28:25 */           SCBGameManager.getInstance().deleteGame(mapName);
/* 29:26 */           cs.sendMessage("[§aSCB§r] Deleted map " + mapName);
/* 30:   */         }
/* 31:   */       }
/* 32:   */     }
/* 33:   */     else {
/* 34:32 */       cs.sendMessage("[§aSCB§r] Not Enough Permissions");
/* 35:   */     }
/* 36:34 */     return true;
/* 37:   */   }
/* 38:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.commands.Commanddeletearena
 * JD-Core Version:    0.7.0.1
 */