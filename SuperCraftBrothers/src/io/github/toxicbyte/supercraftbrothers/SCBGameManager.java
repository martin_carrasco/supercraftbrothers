/*   1:    */ package io.github.toxicbyte.supercraftbrothers;
/*   2:    */ 
/*   3:    */ import io.github.toxicbyte.supercraftbrothers.utilities.LocationUtils;

/*   4:    */ import java.util.Collection;
/*   5:    */ import java.util.HashMap;
/*   6:    */ import java.util.List;

/*   7:    */ import org.bukkit.Location;
/*   8:    */ import org.bukkit.Material;
/*  11:    */ import org.bukkit.entity.Player;
/*  12:    */ 
/*  13:    */ public class SCBGameManager
/*  14:    */ {
/*  15: 13 */   private static final SCBGameManager instance = new SCBGameManager();
/*  16: 14 */   private HashMap<String, SCBGame> games = new HashMap<String, SCBGame>();
/*  17: 15 */   private HashMap<String, CraftBrother> bros = new HashMap<String, CraftBrother>();
/*  18:    */   private SuperCraftBrothers plugin;
/*  19:    */   private Location mainLobby;
/*  20:    */   
/*  21:    */   public static SCBGameManager getInstance()
/*  22:    */   {
/*  23: 20 */     return instance;
/*  24:    */   }
/*  25:    */   
/*  26:    */   public void setup(SuperCraftBrothers plugin)
/*  27:    */   {
/*  28: 24 */     this.plugin = plugin;
/*  29: 25 */     load();
/*  30:    */   }
/*  31:    */   
/*  32:    */   public void reloadCfg()
/*  33:    */   {
/*  34: 29 */     for (SCBGame game : this.games.values()) {
/*  35: 31 */       if (game.isInGame()) {
/*  36: 33 */         game.stopGame();
/*  37:    */       }
/*  38:    */     }
/*  39:    */   }
/*  40:    */   
/*  41:    */   public void load()
/*  42:    */   {
/*  43: 38 */     this.games.clear();
/*  44: 39 */     List<String> names = this.plugin.getConfig().getStringList("maps");
/*  45: 40 */     if (names != null) {
/*  46: 41 */       for (String name : names) {
/*  47: 42 */         addGame(new SCBGame(this.plugin, name, this.plugin.getConfig().getBoolean("map." + name + ".disabled")));
/*  48:    */       }
/*  49:    */     }
/*  50: 45 */     if (this.plugin.getConfig().isString("main-lobby")) {
/*  51: 46 */       this.mainLobby = LocationUtils.stringToLocation(this.plugin.getConfig().getString("main-lobby"));
/*  52:    */     }
/*  53:    */   }
/*  54:    */   
/*  55:    */   public SCBGame createGame(String name)
/*  56:    */   {
/*  57: 52 */     if (getGame(name) == null)
/*  58:    */     {
/*  59: 53 */       SCBGame game = (SCBGame)this.games.put(name, new SCBGame(this.plugin, name, false));
/*  60: 54 */       List<String> names = this.plugin.getConfig().getStringList("maps");
/*  61: 55 */       names.add(name);
/*  62: 56 */       this.plugin.getConfig().set("maps", names);
/*  63: 57 */       this.plugin.saveConfig();
/*  64: 58 */       return game;
/*  65:    */     }
/*  66: 60 */     return null;
/*  67:    */   }
/*  68:    */   
/*  69:    */   public boolean deleteGame(String name)
/*  70:    */   {
/*  71: 65 */     if (getGame(name) != null)
/*  72:    */     {
/*  73: 67 */       getGame(name).stopGame();
/*  74: 68 */       this.games.remove(name);
/*  75: 69 */       List<String> names = this.plugin.getConfig().getStringList("maps");
/*  76: 70 */       names.remove(name);
/*  77: 71 */       Location loc = LocationUtils.stringToLocation(this.plugin.getConfig().getString("map." + name + ".lobby-sign"));
/*  78: 72 */       if (loc != null) {
/*  79: 74 */         loc.getBlock().setType(Material.AIR);
/*  80:    */       }
/*  81: 76 */       this.plugin.getConfig().set("maps", names);
/*  82: 77 */       this.plugin.saveConfig();
/*  83: 78 */       return true;
/*  84:    */     }
/*  85: 80 */     return false;
/*  86:    */   }
/*  87:    */   
/*  88:    */   public CraftBrother getCraftBrother(Player player)
/*  89:    */   {
/*  90: 84 */     if (player == null) {
/*  91: 85 */       return null;
/*  92:    */     }
/*  93: 87 */     return getCraftBrother(player.getName());
/*  94:    */   }
/*  95:    */   
/*  96:    */   public CraftBrother getCraftBrother(String player)
/*  97:    */   {
/*  98: 91 */     if (player == null) {
/*  99: 92 */       return null;
/* 100:    */     }
/* 101: 94 */     if ((this.bros.containsKey(player)) && (this.bros.get(player) != null)) {
/* 102: 95 */       return (CraftBrother)this.bros.get(player);
/* 103:    */     }
/* 104: 97 */     return null;
/* 105:    */   }
/* 106:    */   
/* 107:    */   public CraftBrother addCraftBrother(String player)
/* 108:    */   {
/* 109:101 */     return (CraftBrother)this.bros.put(player, new CraftBrother(player, this.plugin));
/* 110:    */   }
/* 111:    */   
/* 112:    */   public void removeCraftBrother(String player)
/* 113:    */   {
/* 114:105 */     if (player == null) {
/* 115:106 */       return;
/* 116:    */     }
/* 117:108 */     if (this.bros.containsKey(player)) {
/* 118:109 */       this.bros.remove(player);
/* 119:    */     }
/* 120:    */   }
/* 121:    */   
/* 122:    */   public boolean isInGame(String player)
/* 123:    */   {
/* 124:114 */     return this.bros.containsKey(player);
/* 125:    */   }
/* 126:    */   
/* 127:    */   public SCBGame getGame(String game)
/* 128:    */   {
/* 129:118 */     if (this.games.containsKey(game)) {
/* 130:119 */       return (SCBGame)this.games.get(game);
/* 131:    */     }
/* 132:121 */     return null;
/* 133:    */   }
/* 134:    */   
/* 135:    */   public void addGame(SCBGame game)
/* 136:    */   {
/* 137:125 */     if (game == null) {
/* 138:126 */       return;
/* 139:    */     }
/* 140:128 */     if (!this.games.containsKey(game.getName())) {
/* 141:129 */       this.games.put(game.getName(), game);
/* 142:    */     }
/* 143:    */   }
/* 144:    */   
/* 145:    */   public void removeGame(SCBGame game)
/* 146:    */   {
/* 147:134 */     if (game == null) {
/* 148:135 */       return;
/* 149:    */     }
/* 150:137 */     removeGame(game.getName());
/* 151:    */   }
/* 152:    */   
/* 153:    */   public void removeGame(String game)
/* 154:    */   {
/* 155:141 */     if (game == null) {
/* 156:142 */       return;
/* 157:    */     }
/* 158:144 */     if (this.games.containsKey(game)) {
/* 159:145 */       this.games.remove(game);
/* 160:    */     }
/* 161:    */   }
/* 162:    */   
/* 163:    */   public Collection<SCBGame> getAllGames()
/* 164:    */   {
/* 165:150 */     return this.games.values();
/* 166:    */   }
/* 167:    */   
/* 168:    */   public Location getMainLobby()
/* 169:    */   {
/* 170:154 */     return this.mainLobby;
/* 171:    */   }
/* 172:    */   
/* 173:    */   public void setMainLobby(Location mainLobby)
/* 174:    */   {
/* 175:158 */     this.plugin.getConfig().set("main-lobby", LocationUtils.locationToString(mainLobby));
/* 176:159 */     this.plugin.saveConfig();
/* 177:160 */     this.mainLobby = mainLobby;
/* 178:    */   }
/* 179:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.SCBGameManager
 * JD-Core Version:    0.7.0.1
 */