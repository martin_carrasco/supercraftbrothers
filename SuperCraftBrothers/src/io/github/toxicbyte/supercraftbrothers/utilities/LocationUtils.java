/*  1:   */ package io.github.toxicbyte.supercraftbrothers.utilities;
/*  2:   */ 
/*  3:   */ import org.bukkit.Bukkit;
/*  4:   */ import org.bukkit.Location;
/*  6:   */ import org.bukkit.World;
/*  7:   */ 
/*  8:   */ public class LocationUtils
/*  9:   */ {
/* 10:   */   public static String locationToString(Location l)
/* 11:   */   {
/* 12:11 */     if (l == null) {
/* 13:12 */       return null;
/* 14:   */     }
/* 15:13 */     String w = l.getWorld().getName();
/* 16:14 */     double x = l.getX();
/* 17:15 */     double y = l.getY();
/* 18:16 */     double z = l.getZ();
/* 19:17 */     float p = l.getPitch();
/* 20:18 */     float ya = l.getYaw();
/* 21:19 */     return w + "~" + x + "~" + y + "~" + z + "~" + p + "~" + ya;
/* 22:   */   }
/* 23:   */   
/* 24:   */   public static Location stringToLocation(String s)
/* 25:   */   {
/* 26:23 */     if (s == null) {
/* 27:24 */       return null;
/* 28:   */     }
/* 29:25 */     String[] str = s.split("\\~");
/* 30:26 */     World w = Bukkit.getServer().getWorld(str[0]);
/* 31:27 */     double x = Double.parseDouble(str[1]);
/* 32:28 */     double y = Double.parseDouble(str[2]);
/* 33:29 */     double z = Double.parseDouble(str[3]);
/* 34:30 */     float p = Float.parseFloat(str[4]);
/* 35:31 */     float ya = Float.parseFloat(str[5]);
/* 36:32 */     return new Location(w, x, y, z, ya, p);
/* 37:   */   }
/* 38:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.utilities.LocationUtils
 * JD-Core Version:    0.7.0.1
 */