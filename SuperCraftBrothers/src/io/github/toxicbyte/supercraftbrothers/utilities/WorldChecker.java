/*  1:   */ package io.github.toxicbyte.supercraftbrothers.utilities;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.SCBGame;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;

/*  8:   */ import org.bukkit.Bukkit;
/*  9:   */ import org.bukkit.Location;
/* 11:   */ import org.bukkit.entity.Player;
/* 12:   */ 
/* 13:   */ public class WorldChecker
/* 14:   */ {
/* 15:   */   public static boolean main(Player p)
/* 16:   */   {
/* 17:15 */     Location l = SCBGameManager.getInstance().getMainLobby();
/* 18:16 */     if (l != null)
/* 19:   */     {
/* 20:18 */       if (p.getWorld().getName().equals(l.getWorld().getName())) {
/* 21:20 */         return true;
/* 22:   */       }
/* 23:   */     }
/* 24:   */     else {
/* 25:25 */       Bukkit.getLogger().severe("[SCB] No Main Lobby Set!");
/* 26:   */     }
/* 27:27 */     return false;
/* 28:   */   }
/* 29:   */   
/* 30:   */   public static boolean lobby(Player p)
/* 31:   */   {
/* 32:31 */     CraftBrother c = SCBGameManager.getInstance().getCraftBrother(p);
/* 33:32 */     if (c != null)
/* 34:   */     {
/* 35:34 */       SCBGame game = c.getCurrentGame();
/* 36:35 */       if (game != null)
/* 37:   */       {
/* 38:37 */         Location l = game.getMap().getClassLobby();
/* 39:38 */         if (l != null)
/* 40:   */         {
/* 41:40 */           if (c.getPlayer().getWorld().getName().equals(l.getWorld().getName())) {
/* 42:42 */             return true;
/* 43:   */           }
/* 44:   */         }
/* 45:   */         else {
/* 46:47 */           Bukkit.getLogger().severe("[SCB] No Class Lobby set for " + game.getName());
/* 47:   */         }
/* 48:   */       }
/* 49:   */     }
/* 50:51 */     return false;
/* 51:   */   }
/* 52:   */   
/* 53:   */   public static boolean game(Player p)
/* 54:   */   {
/* 55:55 */     CraftBrother c = SCBGameManager.getInstance().getCraftBrother(p);
/* 56:56 */     if (c != null)
/* 57:   */     {
/* 58:58 */       SCBGame game = c.getCurrentGame();
/* 59:59 */       if (game != null)
/* 60:   */       {
/* 61:61 */         String currentGameWorld = c.getCurrentGame().getMap().getSp1().getWorld().getName();
/* 62:62 */         if ((c != null) && (currentGameWorld != null) && (c.getPlayer().getWorld().getName().equals(currentGameWorld))) {
/* 63:64 */           return true;
/* 64:   */         }
/* 65:   */       }
/* 66:   */     }
/* 67:68 */     return false;
/* 68:   */   }
/* 69:   */   
/* 70:   */   public static boolean mainLobby(Player p)
/* 71:   */   {
/* 72:72 */     if ((main(p)) || (lobby(p))) {
/* 73:74 */       return true;
/* 74:   */     }
/* 75:76 */     return false;
/* 76:   */   }
/* 77:   */   
/* 78:   */   public static boolean mainLobbyGame(Player p)
/* 79:   */   {
/* 80:81 */     if ((main(p)) || (lobby(p)) || (game(p))) {
/* 81:83 */       return true;
/* 82:   */     }
/* 83:85 */     return false;
/* 84:   */   }
/* 85:   */   
/* 86:   */   public static boolean lobbyGame(Player p)
/* 87:   */   {
/* 88:89 */     if ((lobby(p)) || (game(p))) {
/* 89:91 */       return true;
/* 90:   */     }
/* 91:93 */     return false;
/* 92:   */   }
/* 93:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.utilities.WorldChecker
 * JD-Core Version:    0.7.0.1
 */