/*  1:   */ package io.github.toxicbyte.supercraftbrothers.utilities;
/*  2:   */ 
/*  3:   */ import org.bukkit.Material;
/*  4:   */ import org.bukkit.entity.Player;
/*  5:   */ import org.bukkit.inventory.ItemStack;
/*  7:   */ import org.bukkit.potion.PotionEffect;
/*  8:   */ 
/*  9:   */ public class ClassUtils
/* 10:   */ {
/* 11:11 */   static String[][] colors = {
/* 12:12 */     { "Blaze", "§4" }, 
/* 13:13 */     { "Cactus", "§2" }, 
/* 14:14 */     { "Creeper", "§a" }, 
/* 15:15 */     { "Enderman", "§5" }, 
/* 16:16 */     { "Skeleton", "§7" }, 
/* 17:17 */     { "Silverfish", "§7" }, 
/* 18:18 */     { "Spider", "§c" }, 
/* 19:19 */     { "Squid", "§1" }, 
/* 20:20 */     { "Wither", "§8" }, 
/* 21:21 */     { "Zombie", "§3" }, 
/* 22:22 */     { "ZombiePigman", "§a" }, 
/* 23:23 */     { "Dweller", "§0" }, 
/* 24:24 */     { "Pig", "§r" }, 
/* 25:25 */     { "Villager", "§8" }, 
/* 26:26 */     { "Jeb", "§3" }, 
/* 27:27 */     { "ButterBro", "§e" }, 
/* 28:28 */     { "Chicken", "§e" }, 
/* 29:29 */     { "Notch", "§0" }, 
/* 30:30 */     { "Witch", "§5" }, 
/* 31:31 */     { "WitherSkeleton", "§0" }, 
/* 32:32 */     { "Mollstam", "§0" }, 
/* 33:33 */     { "SethBling", "§4" }, 
/* 34:34 */     { "Ocelot", "§a" }, 
/* 35:35 */     { "Tnt", "§c" } };
/* 36:   */   
/* 37:   */   @Deprecated
/* 38:   */   public static String getColor(String name)
/* 39:   */   {
/* 40:40 */     for (int i = 0; i < colors.length; i++) {
/* 41:42 */       if (colors[i][0].equals(name)) {
/* 42:44 */         return colors[i][1];
/* 43:   */       }
/* 44:   */     }
/* 45:47 */     return "";
/* 46:   */   }
/* 47:   */   
/* 48:   */   public static String getName(String name)
/* 49:   */   {
/* 50:51 */     for (int i = 0; i < colors.length; i++) {
/* 51:53 */       if (colors[i][0].equals(name)) {
/* 52:55 */         return colors[i][1] + name;
/* 53:   */       }
/* 54:   */     }
/* 55:58 */     return "";
/* 56:   */   }
/* 57:   */   
/* 58:   */   public static void clearInventory(Player player)
/* 59:   */   {
/* 60:61 */     player.getInventory().clear();
/* 61:62 */     player.getInventory().setHelmet(new ItemStack(Material.AIR));
/* 62:63 */     player.getInventory().setChestplate(new ItemStack(Material.AIR));
/* 63:64 */     player.getInventory().setLeggings(new ItemStack(Material.AIR));
/* 64:65 */     player.getInventory().setBoots(new ItemStack(Material.AIR));
/* 65:66 */     player.setItemOnCursor(new ItemStack(Material.AIR));
/* 66:68 */     for (PotionEffect effect : player.getActivePotionEffects()) {
/* 67:69 */       player.removePotionEffect(effect.getType());
/* 68:   */     }
/* 69:   */   }
/* 70:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.utilities.ClassUtils
 * JD-Core Version:    0.7.0.1
 */