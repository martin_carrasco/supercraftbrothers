/*   1:    */ package io.github.toxicbyte.supercraftbrothers;
/*   2:    */ 
/*   3:    */ import io.github.toxicbyte.supercraftbrothers.commands.Commandcreatearena;
import io.github.toxicbyte.supercraftbrothers.commands.Commanddefault;
import io.github.toxicbyte.supercraftbrothers.commands.Commanddeletearena;
import io.github.toxicbyte.supercraftbrothers.commands.Commandgivegem;
import io.github.toxicbyte.supercraftbrothers.commands.Commandpro;
import io.github.toxicbyte.supercraftbrothers.commands.Commandscb;
import io.github.toxicbyte.supercraftbrothers.commands.Commandsetlobbyspawn;
import io.github.toxicbyte.supercraftbrothers.commands.Commandsetmainlobby;
import io.github.toxicbyte.supercraftbrothers.commands.Commandsetspawnloc;
import io.github.toxicbyte.supercraftbrothers.commands.Commandvip;
import io.github.toxicbyte.supercraftbrothers.commands.SubCommands;
import io.github.toxicbyte.supercraftbrothers.listeners.DBlockBreakListener;
import io.github.toxicbyte.supercraftbrothers.listeners.DBlockPlaceListener;
import io.github.toxicbyte.supercraftbrothers.listeners.DInventoryClickListener;
import io.github.toxicbyte.supercraftbrothers.listeners.DPlayerCommandPreprocessListener;
import io.github.toxicbyte.supercraftbrothers.listeners.DPlayerDropItemListener;
import io.github.toxicbyte.supercraftbrothers.listeners.DPlayerPickupItemListener;
import io.github.toxicbyte.supercraftbrothers.listeners.EntityBowFireListener;
import io.github.toxicbyte.supercraftbrothers.listeners.EntityExplodeListener;
import io.github.toxicbyte.supercraftbrothers.listeners.FoodLevelListener;
import io.github.toxicbyte.supercraftbrothers.listeners.PlayerDamageListener;
import io.github.toxicbyte.supercraftbrothers.listeners.PlayerDeathListener;
import io.github.toxicbyte.supercraftbrothers.listeners.PlayerEggThrow;
import io.github.toxicbyte.supercraftbrothers.listeners.PlayerInteractListener;
import io.github.toxicbyte.supercraftbrothers.listeners.PlayerJoinListener;
import io.github.toxicbyte.supercraftbrothers.listeners.PlayerQuitListener;
import io.github.toxicbyte.supercraftbrothers.listeners.PlayerRespawnListener;
import io.github.toxicbyte.supercraftbrothers.listeners.PlayerToggleFlightListener;
import io.github.toxicbyte.supercraftbrothers.listeners.PlayerWorldChangeListener;
import io.github.toxicbyte.supercraftbrothers.listeners.SCBDeathListener;
import io.github.toxicbyte.supercraftbrothers.listeners.SignChangeListener;
import io.github.toxicbyte.supercraftbrothers.menus.ClassMenu;

/*  38:    */ import java.io.File;

/*  39:    */ import org.bukkit.Bukkit;
/*  42:    */ import org.bukkit.configuration.file.FileConfiguration;
/*  43:    */ import org.bukkit.configuration.file.YamlConfiguration;
/*  44:    */ import org.bukkit.plugin.PluginManager;
/*  45:    */ import org.bukkit.plugin.java.JavaPlugin;
/*  47:    */ 
/*  48:    */ public class SuperCraftBrothers
/*  49:    */   extends JavaPlugin
/*  50:    */ {
/*  51:    */   private static SuperCraftBrothers instance;
/*  52:    */   
/*  53:    */   public static SuperCraftBrothers getInstance()
/*  54:    */   {
/*  55: 19 */     return instance;
/*  56:    */   }
/*  57:    */   
/*  58: 20 */   public boolean update = false;
/*  59:    */   public String name;
/*  60:    */   
/*  61:    */   public void onEnable()
/*  62:    */   {
/*  63: 26 */     saveDefaultConfig();
/*  64: 27 */     instance = this;
/*  65: 28 */     registerListeners();
/*  66: 29 */     registerCommands();
/*  67: 30 */     SCBGameManager.getInstance().setup(this);
/*  68: 31 */     SubCommands.setup(this);
/*  69: 32 */     HUBSigns.setup(this);
/*  70: 33 */     InventoryConfig.setup(this);
/*  71: 34 */     new ClassMenu(this);
/*  76:    */   }
/*  77:    */   
/*  78:    */   public void onDisable()
/*  79:    */   {
/*  80: 43 */     instance = null;
/*  81: 44 */     getServer().getScheduler().cancelTasks(this);
/*  82: 45 */     saveConfig();
/*  83:    */   }
/*  84:    */   
/*  85:    */   public void registerCommands()
/*  86:    */   {
/*  87: 49 */     getCommand("scb").setExecutor(new Commandscb());
/*  88: 50 */     getCommand("createarena").setExecutor(new Commandcreatearena());
/*  89: 51 */     getCommand("deletearena").setExecutor(new Commanddeletearena());
/*  90: 52 */     getCommand("setmainlobby").setExecutor(new Commandsetmainlobby());
/*  91: 53 */     getCommand("setlobbyspawn").setExecutor(new Commandsetlobbyspawn());
/*  92: 54 */     getCommand("setspawnloc").setExecutor(new Commandsetspawnloc());
/*  93: 55 */     getCommand("givegem").setExecutor(new Commandgivegem());
/*  94: 56 */     getCommand("vip").setExecutor(new Commandvip());
/*  95: 57 */     getCommand("pro").setExecutor(new Commandpro());
/*  96: 58 */     getCommand("default").setExecutor(new Commanddefault());
/*  97:    */   }
/*  98:    */   
/*  99:    */   public void registerListeners()
/* 100:    */   {
/* 101: 62 */     PluginManager pm = getServer().getPluginManager();
/* 102: 63 */     pm.registerEvents(new PlayerDamageListener(), this);
/* 103: 64 */     pm.registerEvents(new PlayerInteractListener(this), this);
/* 104: 65 */     pm.registerEvents(new SignChangeListener(), this);
/* 105: 66 */     pm.registerEvents(new PlayerDeathListener(), this);
/* 106: 67 */     pm.registerEvents(new PlayerRespawnListener(), this);
/* 107: 68 */     pm.registerEvents(new PlayerQuitListener(), this);
/* 108: 69 */     pm.registerEvents(new SCBDeathListener(this), this);
/* 109: 70 */     pm.registerEvents(new EntityBowFireListener(), this);
/* 110: 71 */     pm.registerEvents(new PlayerJoinListener(this), this);
/* 111: 72 */     pm.registerEvents(new PlayerToggleFlightListener(this), this);
/* 112: 73 */     pm.registerEvents(new PlayerEggThrow(), this);
/* 113: 74 */     pm.registerEvents(new FoodLevelListener(), this);
/* 114: 75 */     pm.registerEvents(new EntityExplodeListener(), this);
/* 115: 76 */     pm.registerEvents(new PlayerWorldChangeListener(), this);
/* 116:    */     
/* 117:    */ 
/* 118: 79 */     pm.registerEvents(new DBlockBreakListener(), this);
/* 119: 80 */     pm.registerEvents(new DBlockPlaceListener(), this);
/* 120: 81 */     pm.registerEvents(new DInventoryClickListener(), this);
/* 121: 82 */     pm.registerEvents(new DPlayerDropItemListener(), this);
/* 122: 83 */     pm.registerEvents(new DPlayerPickupItemListener(), this);
/* 123: 84 */     pm.registerEvents(new DPlayerCommandPreprocessListener(), this);
/* 124:    */   }
/* 125:    */   
/* 126:    */   public void reloadCfg()
/* 127:    */   {
/* 128: 88 */     SCBGameManager.getInstance().reloadCfg();
/* 129: 89 */     Bukkit.getPluginManager().disablePlugin(this);
/* 130: 90 */     Bukkit.getPluginManager().enablePlugin(this);
/* 131:    */   }
/* 132:    */   
/* 137:    */   
/* 138:    */   public FileConfiguration getPlayerConfig(String p)
/* 139:    */   {
/* 140: 98 */     File file = new File(getDataFolder() + File.separator + "users" + File.separator + p + ".yml");
/* 141: 99 */     FileConfiguration c = YamlConfiguration.loadConfiguration(file);
/* 142:100 */     return c;
/* 143:    */   }
/* 144:    */   
/* 145:    */   public String getPlayerFile(String p)
/* 146:    */   {
/* 147:104 */     return getDataFolder() + File.separator + "users" + File.separator + p + ".yml";
/* 148:    */   }
/* 149:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.SuperCraftBrothers
 * JD-Core Version:    0.7.0.1
 */