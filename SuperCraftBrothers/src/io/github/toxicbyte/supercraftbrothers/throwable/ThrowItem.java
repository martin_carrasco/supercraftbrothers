/*   1:    */ package io.github.toxicbyte.supercraftbrothers.throwable;
/*   2:    */ 
/*   3:    */ import io.github.toxicbyte.supercraftbrothers.SuperCraftBrothers;

/*   4:    */ import java.util.HashMap;

/*   5:    */ import org.bukkit.Location;
/*   6:    */ import org.bukkit.Material;
/*   9:    */ import org.bukkit.entity.Entity;
/*  10:    */ import org.bukkit.entity.EntityType;
/*  11:    */ import org.bukkit.entity.Item;
/*  12:    */ import org.bukkit.entity.Player;
/*  13:    */ import org.bukkit.inventory.ItemStack;
/*  14:    */ import org.bukkit.material.SpawnEgg;
/*  15:    */ import org.bukkit.scheduler.BukkitRunnable;
/*  16:    */ import org.bukkit.util.Vector;
/*  17:    */ 
/*  18:    */ public class ThrowItem
/*  19:    */ {
/*  20:    */   private SuperCraftBrothers scb;
/*  21:    */   private ItemStack i;
/*  22:    */   private ThrowReason t;
/*  23:    */   private Player s;
/*  24:    */   private Location l;
/*  25:    */   private Vector v;
/*  26:    */   private Boolean b;
/*  27:    */   private Boolean p;
/*  28: 28 */   public static HashMap<Integer, Double> shooterhealth = new HashMap<Integer, Double>();
/*  29: 29 */   public static HashMap<Item, EntityType> eggs = new HashMap<Item, EntityType>();
/*  30:    */   
/*  31:    */   public ThrowItem(SuperCraftBrothers scb, ItemStack i, ThrowReason t, Player s, Location l, Vector v, boolean b, boolean p)
/*  32:    */   {
/*  33: 32 */     this.scb = scb;
/*  34: 33 */     this.i = i;
/*  35: 34 */     this.t = t;
/*  36: 35 */     this.s = s;
/*  37: 36 */     this.l = l;
/*  38: 37 */     this.v = v;
/*  39: 38 */     this.b = Boolean.valueOf(b);
/*  40: 39 */     this.p = Boolean.valueOf(p);
/*  41: 40 */     throwItem();
/*  42:    */   }
/*  43:    */   
/*  44:    */   public void throwItem()
/*  45:    */   {
/*  46: 45 */     this.l.setY(this.l.getY() - 0.5D);
/*  47: 46 */     final Item itemdrop = this.l.getWorld().dropItem(this.l, this.i);
/*  48: 47 */     itemdrop.setVelocity(this.v);
/*  49: 48 */     itemdrop.setPickupDelay(2147483647);
/*  50: 50 */     if (this.i.equals(new ItemStack(Material.REDSTONE_COMPARATOR))) {
/*  51: 52 */       shooterhealth.put(Integer.valueOf(itemdrop.getEntityId()), Double.valueOf(this.s.getHealth()));
/*  52:    */     }
/*  53: 54 */     if ((this.i.getData() instanceof SpawnEgg))
/*  54:    */     {
/*  55: 56 */       EntityType segg = ((SpawnEgg)this.i.getData()).getSpawnedType();
/*  56: 57 */       eggs.put(itemdrop, segg);
/*  57:    */     }
/*  58: 59 */     BukkitRunnable br = new BukkitRunnable()
/*  59:    */     {
/*  60:    */       public void run()
/*  61:    */       {
/*  62:    */         Location pos2;
/*  63: 63 */         if (ThrowItem.this.b.booleanValue())
/*  64:    */         {
/*  65: 65 */           Location pos1 = itemdrop.getLocation().add(-0.25D, 0.25D, 0.25D);
/*  66: 66 */           pos2 = itemdrop.getLocation().add(0.25D, -0.25D, -0.25D);
/*  67: 67 */           if ((pos1.getBlock().getType() != Material.AIR) || (pos2.getBlock().getType() != Material.AIR)) {
/*  68: 69 */             if (ThrowItem.this.t.getEvent().onHitByBlock(itemdrop))
/*  69:    */             {
/*  70: 71 */               cancel();
/*  71: 72 */               itemdrop.remove();
/*  72:    */             }
/*  73:    */           }
/*  74:    */         }
/*  75: 76 */         if (ThrowItem.this.p.booleanValue()) {
/*  76: 78 */           for (Entity entity : itemdrop.getNearbyEntities(1.5D, 1.5D, 1.5D)) {
/*  77: 80 */             if (!entity.equals(ThrowItem.this.s)) {
/*  78: 82 */               if (ThrowItem.this.t.getEvent().onHitByEntity(entity, itemdrop))
/*  79:    */               {
/*  80: 84 */                 itemdrop.remove();
/*  81: 85 */                 cancel();
/*  82:    */               }
/*  83:    */             }
/*  84:    */           }
/*  85:    */         }
/*  86:    */       }
/*  87: 91 */     };
/*  88: 92 */     br.runTaskTimer(this.scb, 1L, 1L);
/*  89:    */   }
/*  90:    */   
/*  91:    */   public static HashMap<Integer, Double> getShooterHealth()
/*  92:    */   {
/*  93: 97 */     return shooterhealth;
/*  94:    */   }
/*  95:    */   
/*  96:    */   public static HashMap<Item, EntityType> getEggs()
/*  97:    */   {
/*  98:101 */     return eggs;
/*  99:    */   }
/* 100:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.throwable.ThrowItem
 * JD-Core Version:    0.7.0.1
 */