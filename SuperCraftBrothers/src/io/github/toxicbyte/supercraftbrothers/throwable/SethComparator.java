/*  1:   */ package io.github.toxicbyte.supercraftbrothers.throwable;
/*  4:   */ import org.bukkit.entity.Entity;
/*  5:   */ import org.bukkit.entity.Item;
/*  6:   */ import org.bukkit.entity.LivingEntity;
/*  7:   */ import org.bukkit.entity.Player;
/*  8:   */ 
/*  9:   */ public class SethComparator
/* 10:   */   implements EntityHitEvent
/* 11:   */ {
/* 12:   */   public boolean onHitByEntity(Entity e, Item i)
/* 13:   */   {
/* 14:13 */     if ((e instanceof LivingEntity)) {
/* 15:15 */       if ((e instanceof Player))
/* 16:   */       {
/* 17:17 */         Player victim = (Player)e;
/* 18:18 */         double vh = victim.getHealth();
/* 19:19 */         double ph = ((Double)ThrowItem.getShooterHealth().get(Integer.valueOf(i.getEntityId()))).doubleValue();
/* 20:20 */         if (ph > vh)
/* 21:   */         {
/* 22:22 */           double nh = ph - vh;
/* 23:23 */           if (nh < vh) {
/* 24:24 */             victim.setHealth(vh - nh);
/* 25:   */           } else {
/* 26:26 */             victim.setHealth(0.0D);
/* 27:   */           }
/* 28:   */         }
/* 29:   */         else
/* 30:   */         {
/* 31:30 */           double nh = vh - ph;
/* 32:31 */           if (nh > vh) {
/* 33:32 */             victim.setHealth(vh + nh);
/* 34:   */           } else {
/* 35:34 */             victim.setHealth(20.0D);
/* 36:   */           }
/* 37:   */         }
/* 38:36 */         return true;
/* 39:   */       }
/* 40:   */     }
/* 41:39 */     return false;
/* 42:   */   }
/* 43:   */   
/* 44:   */   public boolean onHitByBlock(Item item)
/* 45:   */   {
/* 46:45 */     return false;
/* 47:   */   }
/* 48:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.throwable.SethComparator
 * JD-Core Version:    0.7.0.1
 */