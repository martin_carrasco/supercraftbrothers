/*  1:   */ package io.github.toxicbyte.supercraftbrothers.throwable;
/*  5:   */ import org.bukkit.entity.Entity;
/*  6:   */ import org.bukkit.entity.EntityType;
/*  7:   */ import org.bukkit.entity.Item;
/*  8:   */ import org.bukkit.entity.LivingEntity;
/*  9:   */ import org.bukkit.inventory.ItemStack;
/* 10:   */ 
/* 11:   */ public class Spawnegg
/* 12:   */   implements EntityHitEvent
/* 13:   */ {
/* 14:   */   public boolean onHitByEntity(Entity e, Item item)
/* 15:   */   {
/* 16:15 */     if ((e instanceof LivingEntity))
/* 17:   */     {
/* 18:17 */       ItemStack i = item.getItemStack();
/* 19:18 */       if (ThrowItem.getEggs().containsKey(i))
/* 20:   */       {
/* 21:20 */         EntityType entity1 = (EntityType)ThrowItem.getEggs().get(i);
/* 22:21 */         item.getWorld().spawnEntity(e.getLocation(), entity1);
/* 23:22 */         return true;
/* 24:   */       }
/* 25:24 */       return false;
/* 26:   */     }
/* 27:26 */     return false;
/* 28:   */   }
/* 29:   */   
/* 30:   */   public boolean onHitByBlock(Item item)
/* 31:   */   {
/* 32:32 */     if (ThrowItem.getEggs().containsKey(item))
/* 33:   */     {
/* 34:34 */       EntityType entity1 = (EntityType)ThrowItem.getEggs().get(item);
/* 35:35 */       ThrowItem.getEggs().remove(item);
/* 36:36 */       item.getWorld().spawnEntity(item.getLocation(), entity1);
/* 37:37 */       return true;
/* 38:   */     }
/* 39:39 */     return false;
/* 40:   */   }
/* 41:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.throwable.Spawnegg
 * JD-Core Version:    0.7.0.1
 */