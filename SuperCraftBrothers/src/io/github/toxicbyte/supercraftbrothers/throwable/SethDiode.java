/*  1:   */ package io.github.toxicbyte.supercraftbrothers.throwable;
/*  2:   */ 
/*  3:   */ import org.bukkit.entity.Entity;
/*  4:   */ import org.bukkit.entity.Item;
/*  5:   */ import org.bukkit.entity.LivingEntity;
/*  6:   */ import org.bukkit.entity.Player;
/*  7:   */ 
/*  8:   */ public class SethDiode
/*  9:   */   implements EntityHitEvent
/* 10:   */ {
/* 11:   */   public boolean onHitByEntity(Entity e, Item i)
/* 12:   */   {
/* 13:14 */     if ((e instanceof LivingEntity)) {
/* 14:16 */       if ((e instanceof Player))
/* 15:   */       {
/* 16:18 */         Player p = (Player)e;
/* 17:19 */         double h = p.getHealth();
/* 18:20 */         double ld = p.getLastDamage();
/* 19:21 */         if (h > ld) {
/* 20:22 */           p.setHealth(h - ld);
/* 21:   */         } else {
/* 22:24 */           p.setHealth(0.0D);
/* 23:   */         }
/* 24:25 */         return true;
/* 25:   */       }
/* 26:   */     }
/* 27:28 */     return false;
/* 28:   */   }
/* 29:   */   
/* 30:   */   public boolean onHitByBlock(Item item)
/* 31:   */   {
/* 32:34 */     return false;
/* 33:   */   }
/* 34:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.throwable.SethDiode
 * JD-Core Version:    0.7.0.1
 */