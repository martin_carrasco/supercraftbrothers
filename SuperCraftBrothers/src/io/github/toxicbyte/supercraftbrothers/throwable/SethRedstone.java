/*  1:   */ package io.github.toxicbyte.supercraftbrothers.throwable;
/*  5:   */ import org.bukkit.entity.Entity;
/*  6:   */ import org.bukkit.entity.Item;
/*  7:   */ 
/*  8:   */ public class SethRedstone
/*  9:   */   implements EntityHitEvent
/* 10:   */ {
/* 11:   */   public boolean onHitByEntity(Entity e, Item i)
/* 12:   */   {
/* 13:11 */     double x = i.getLocation().getX();
/* 14:12 */     double y = i.getLocation().getY();
/* 15:13 */     double z = i.getLocation().getZ();
/* 16:14 */     i.getWorld().createExplosion(x, y, z, 1.0F, false, false);
/* 17:15 */     return true;
/* 18:   */   }
/* 19:   */   
/* 20:   */   public boolean onHitByBlock(Item item)
/* 21:   */   {
/* 22:21 */     return false;
/* 23:   */   }
/* 24:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.throwable.SethRedstone
 * JD-Core Version:    0.7.0.1
 */