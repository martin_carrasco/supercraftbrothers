/*  1:   */ package io.github.toxicbyte.supercraftbrothers.throwable;
/*  5:   */ import org.bukkit.entity.Entity;
/*  6:   */ import org.bukkit.entity.Item;
/*  7:   */ import org.bukkit.entity.LivingEntity;
/*  8:   */ import org.bukkit.entity.Player;
/*  9:   */ 
/* 10:   */ public class TNT
/* 11:   */   implements EntityHitEvent
/* 12:   */ {
/* 13:   */   public boolean onHitByEntity(Entity e, Item item)
/* 14:   */   {
/* 15:13 */     if ((e instanceof LivingEntity))
/* 16:   */     {
/* 17:15 */       double x = item.getLocation().getX();
/* 18:16 */       double y = item.getLocation().getY();
/* 19:17 */       double z = item.getLocation().getZ();
/* 20:18 */       if ((e instanceof Player))
/* 21:   */       {
/* 22:20 */         Player p = (Player)e;
/* 23:21 */         p.setHealth(1.0D);
/* 24:   */       }
/* 25:23 */       item.getLocation().getWorld().createExplosion(x, y, z, 2.0F, false, false);
/* 26:24 */       return true;
/* 27:   */     }
/* 28:26 */     return false;
/* 29:   */   }
/* 30:   */   
/* 31:   */   public boolean onHitByBlock(Item item)
/* 32:   */   {
/* 33:32 */     double x = item.getLocation().getX();
/* 34:33 */     double y = item.getLocation().getY();
/* 35:34 */     double z = item.getLocation().getZ();
/* 36:35 */     item.getLocation().getWorld().createExplosion(x, y, z, 5.0F, false, false);
/* 37:36 */     return true;
/* 38:   */   }
/* 39:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.throwable.TNT
 * JD-Core Version:    0.7.0.1
 */