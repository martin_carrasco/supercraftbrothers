package io.github.toxicbyte.supercraftbrothers.throwable;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;

public abstract interface EntityHitEvent
{
  public abstract boolean onHitByEntity(Entity paramEntity, Item paramItem);
  
  public abstract boolean onHitByBlock(Item paramItem);
}


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.throwable.EntityHitEvent
 * JD-Core Version:    0.7.0.1
 */