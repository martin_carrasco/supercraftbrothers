/*  1:   */ package io.github.toxicbyte.supercraftbrothers.throwable;
/*  5:   */ import org.bukkit.entity.Entity;
/*  6:   */ import org.bukkit.entity.Item;
/*  7:   */ import org.bukkit.entity.LivingEntity;
/*  8:   */ import org.bukkit.entity.Player;
/*  9:   */ 
/* 10:   */ public class DwellerBone
/* 11:   */   implements EntityHitEvent
/* 12:   */ {
/* 13:   */   public boolean onHitByEntity(Entity e, Item i)
/* 14:   */   {
/* 15:14 */     if ((e instanceof LivingEntity))
/* 16:   */     {
/* 17:16 */       double x = i.getLocation().getX();
/* 18:17 */       double y = i.getLocation().getY();
/* 19:18 */       double z = i.getLocation().getZ();
/* 20:19 */       if ((e instanceof Player))
/* 21:   */       {
/* 22:21 */         Player p = (Player)e;
/* 23:22 */         p.setHealth(0.5D);
/* 24:   */       }
/* 25:24 */       i.getLocation().getWorld().createExplosion(x, y, z, 3.0F, false, false);
/* 26:25 */       return true;
/* 27:   */     }
/* 28:27 */     return false;
/* 29:   */   }
/* 30:   */   
/* 31:   */   public boolean onHitByBlock(Item item)
/* 32:   */   {
/* 33:33 */     double x = item.getLocation().getBlockX() + 0.5D;
/* 34:34 */     double y = item.getLocation().getBlockY() + 0.5D;
/* 35:35 */     double z = item.getLocation().getBlockZ() + 0.5D;
/* 36:36 */     item.getLocation().getWorld().createExplosion(x, y, z, 3.0F, false, false);
/* 37:37 */     return true;
/* 38:   */   }
/* 39:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.throwable.DwellerBone
 * JD-Core Version:    0.7.0.1
 */