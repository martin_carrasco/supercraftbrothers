/*  1:   */ package io.github.toxicbyte.supercraftbrothers.throwable;
/*  2:   */ 
/*  3:   */ public enum ThrowReason
/*  4:   */ {
/*  5: 5 */   DWELLER(new DwellerBone()),  SPAWNEGG(new Spawnegg()),  TNT(new TNT()),  SETHDIODE(new SethDiode()),  SETHCOMPARATOR(new SethComparator()),  SETHREDSTONE(new SethRedstone());
/*  6:   */   
/*  7:   */   private EntityHitEvent e;
/*  8:   */   
/*  9:   */   private ThrowReason(EntityHitEvent e)
/* 10:   */   {
/* 11:16 */     this.e = e;
/* 12:   */   }
/* 13:   */   
/* 14:   */   public EntityHitEvent getEvent()
/* 15:   */   {
/* 16:21 */     return this.e;
/* 17:   */   }
/* 18:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.throwable.ThrowReason
 * JD-Core Version:    0.7.0.1
 */