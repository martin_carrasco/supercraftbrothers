/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  4:   */ import io.github.toxicbyte.supercraftbrothers.PlayerStats;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.SuperCraftBrothers;
import io.github.toxicbyte.supercraftbrothers.utilities.WorldChecker;

/*  8:   */ import org.bukkit.GameMode;
/* 10:   */ import org.bukkit.Material;
/* 12:   */ import org.bukkit.block.BlockFace;
/* 13:   */ import org.bukkit.entity.Player;
/* 14:   */ import org.bukkit.event.EventHandler;
/* 15:   */ import org.bukkit.event.EventPriority;
/* 16:   */ import org.bukkit.event.Listener;
/* 17:   */ import org.bukkit.event.player.PlayerToggleFlightEvent;
/* 18:   */ import org.bukkit.scheduler.BukkitRunnable;
/* 20:   */ 
/* 21:   */ public class PlayerToggleFlightListener
/* 22:   */   implements Listener
/* 23:   */ {
/* 24:   */   private SuperCraftBrothers scb;
/* 25:   */   
/* 26:   */   public PlayerToggleFlightListener(SuperCraftBrothers scb)
/* 27:   */   {
/* 28:19 */     this.scb = scb;
/* 29:   */   }
/* 30:   */   
/* 31:   */   @EventHandler(priority=EventPriority.MONITOR)
/* 32:   */   public void onFly(PlayerToggleFlightEvent event)
/* 33:   */   {
/* 34:25 */     Player player = event.getPlayer();
/* 35:26 */     if (player.getGameMode() != GameMode.CREATIVE) {
/* 36:28 */       if (WorldChecker.mainLobbyGame(player))
/* 37:   */       {
/* 38:30 */         if ((SCBGameManager.getInstance().getCraftBrother(player) != null) && (SCBGameManager.getInstance().getCraftBrother(player).isSpectating())) {
/* 39:32 */           return;
/* 40:   */         }
/* 41:34 */         if (WorldChecker.game(player))
/* 42:   */         {
/* 43:36 */           event.setCancelled(true);
/* 44:37 */           player.setAllowFlight(false);
/* 45:38 */           doubleJump(player);
/* 46:   */         }
/* 47:40 */         if (WorldChecker.mainLobby(player)) {
/* 48:42 */           if (PlayerStats.getPlayerRank(player.getName()) > 1)
/* 49:   */           {
/* 50:44 */             event.setCancelled(true);
/* 51:45 */             player.setAllowFlight(false);
/* 52:46 */             doubleJump(player);
/* 53:   */           }
/* 54:   */         }
/* 55:   */       }
/* 56:   */     }
/* 57:   */   }
/* 58:   */   
/* 59:   */   public void doubleJump(final Player player)
/* 60:   */   {
/* 61:55 */     player.setFlying(false);
/* 62:56 */     player.setVelocity(player.getLocation().getDirection().multiply(0.1D).setY(0.82D));
/* 63:57 */     BukkitRunnable br = new BukkitRunnable()
/* 64:   */     {
/* 65:   */       public void run()
/* 66:   */       {
/* 67:62 */         if (!player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().equals(Material.AIR))
/* 68:   */         {
/* 69:64 */           player.setAllowFlight(true);
/* 70:65 */           cancel();
/* 71:   */         }
/* 72:   */       }
/* 73:68 */     };
/* 74:69 */     br.runTaskTimer(this.scb, 0L, 1L);
/* 75:   */   }
/* 76:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.PlayerToggleFlightListener
 * JD-Core Version:    0.7.0.1
 */