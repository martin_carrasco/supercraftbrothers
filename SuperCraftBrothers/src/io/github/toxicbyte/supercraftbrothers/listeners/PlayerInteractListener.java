/*   1:    */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*   2:    */ 
/*   3:    */ /*   8:    */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.PlayerStats;
import io.github.toxicbyte.supercraftbrothers.SCBGame;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.SuperCraftBrothers;
import io.github.toxicbyte.supercraftbrothers.menus.ClassMenu;
import io.github.toxicbyte.supercraftbrothers.menus.GemClassMenu;
import io.github.toxicbyte.supercraftbrothers.menus.GemClassShop;
import io.github.toxicbyte.supercraftbrothers.menus.HatShop;
import io.github.toxicbyte.supercraftbrothers.menus.VIPClassMenu;
import io.github.toxicbyte.supercraftbrothers.throwable.ThrowItem;
import io.github.toxicbyte.supercraftbrothers.throwable.ThrowReason;
import io.github.toxicbyte.supercraftbrothers.utilities.WorldChecker;

/*  16:    */ import java.util.HashMap;
/*  17:    */ import java.util.Random;

/*  19:    */ import org.bukkit.ChatColor;
/*  20:    */ import org.bukkit.Location;
/*  21:    */ import org.bukkit.Material;
/*  23:    */ import org.bukkit.block.BlockFace;
/*  24:    */ import org.bukkit.block.Sign;
/*  25:    */ import org.bukkit.enchantments.Enchantment;
/*  26:    */ import org.bukkit.entity.Entity;
/*  27:    */ import org.bukkit.entity.Player;
/*  28:    */ import org.bukkit.entity.SmallFireball;
/*  29:    */ import org.bukkit.entity.ThrownPotion;
/*  30:    */ import org.bukkit.event.EventHandler;
/*  31:    */ import org.bukkit.event.EventPriority;
/*  32:    */ import org.bukkit.event.Listener;
/*  33:    */ import org.bukkit.event.block.Action;
/*  34:    */ import org.bukkit.event.player.PlayerInteractEvent;
/*  35:    */ import org.bukkit.inventory.ItemStack;
/*  38:    */ import org.bukkit.inventory.meta.PotionMeta;
/*  39:    */ import org.bukkit.material.SpawnEgg;
/*  40:    */ import org.bukkit.potion.Potion;
/*  41:    */ import org.bukkit.potion.PotionEffect;
/*  42:    */ import org.bukkit.potion.PotionEffectType;
/*  43:    */ import org.bukkit.potion.PotionType;
/*  44:    */ import org.bukkit.util.Vector;
/*  45:    */ 
/*  46:    */ public class PlayerInteractListener
/*  47:    */   implements Listener
/*  48:    */ {
/*  49: 28 */   public static HashMap<String, Integer> witchJump = new HashMap<String, Integer>();
/*  50: 29 */   public static HashMap<String, GemClassMenu> gcmmenus = new HashMap<String, GemClassMenu>();
/*  51: 30 */   public static HashMap<String, VIPClassMenu> vipmenus = new HashMap<String, VIPClassMenu>();
/*  52: 31 */   public static HashMap<String, GemClassShop> gcsmenus = new HashMap<String, GemClassShop>();
/*  53: 32 */   public static HashMap<String, HatShop> hsmenus = new HashMap<String, HatShop>();
/*  54:    */   public SuperCraftBrothers plugin;
/*  55:    */   
/*  56:    */   public PlayerInteractListener(SuperCraftBrothers scb)
/*  57:    */   {
/*  58: 35 */     this.plugin = scb;
/*  59:    */   }
/*  60:    */   
/*  61: 36 */   public Random random = new Random();
/*  62:    */   
/*  63:    */   @EventHandler(priority=EventPriority.HIGHEST)
/*  64:    */   public void onPlayerInteract(PlayerInteractEvent e)
/*  65:    */   {
/*  66: 41 */     Player p = e.getPlayer();
/*  67: 42 */     if (WorldChecker.mainLobby(p))
/*  68:    */     {
/*  69: 44 */       if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
/*  70: 46 */         if (e.getClickedBlock().getType().equals(Material.WALL_SIGN))
/*  71:    */         {
/*  72: 48 */           Sign s = (Sign)e.getClickedBlock().getState();
/*  73: 49 */           ClickSign(p, s);
/*  74:    */         }
/*  75:    */       }
/*  76: 52 */       if ((e.getAction().equals(Action.RIGHT_CLICK_AIR)) || (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)))
/*  77:    */       {
/*  78: 54 */         ItemStack i = e.getItem();
/*  79: 55 */         if ((i != null) && (i.getItemMeta() != null) && (i.getItemMeta().getDisplayName() != null)) {
/*  80: 57 */           GemShop(p, i.getItemMeta().getDisplayName());
/*  81:    */         }
/*  82:    */       }
/*  83:    */     }
/*  84: 61 */     if (WorldChecker.game(p)) {
/*  85: 63 */       if ((e.hasItem()) && ((e.getAction().equals(Action.RIGHT_CLICK_AIR)) || (e.getAction().equals(Action.RIGHT_CLICK_BLOCK))))
/*  86:    */       {
/*  87: 65 */         if ((e.getItem().getData() instanceof SpawnEgg)) {
/*  88: 67 */           Pokeballs(e);
/*  89:    */         }
/*  90: 69 */         if (e.getItem().getType().equals(Material.EYE_OF_ENDER)) {
/*  91: 71 */           e.setCancelled(true);
/*  92:    */         }
/*  93: 73 */         if (e.getItem().getType().equals(Material.BONE)) {
/*  94: 75 */           Dweller(e);
/*  95:    */         }
/*  96: 77 */         if (e.getItem().getType().equals(Material.WHEAT)) {
/*  97: 79 */           Witch(e);
/*  98:    */         }
/*  99: 81 */         if (e.getItem().getType().equals(Material.BAKED_POTATO)) {
/* 100: 83 */           Villager(e);
/* 101:    */         }
/* 102: 85 */         if (e.getItem().getType().equals(Material.RED_ROSE)) {
/* 103: 87 */           Butterbro(e);
/* 104:    */         }
/* 105: 89 */         if (e.getItem().getType().equals(Material.YELLOW_FLOWER)) {
/* 106: 91 */           Ocelot(e);
/* 107:    */         }
/* 108: 93 */         if (e.getItem().getType().equals(Material.GRASS)) {
/* 109: 95 */           Notch(e);
/* 110:    */         }
/* 111: 97 */         if (e.getItem().getType().equals(Material.WOOL)) {
/* 112: 99 */           Jeb(e);
/* 113:    */         }
/* 114:101 */         if ((e.getItem().getType().equals(Material.EMPTY_MAP)) || (e.getItem().getType().equals(Material.MAP))) {
/* 115:103 */           Mollstam(e);
/* 116:    */         }
/* 117:105 */         if (e.getItem().getType().equals(Material.TNT)) {
/* 118:107 */           TNT(e);
/* 119:    */         }
/* 120:109 */         if (e.getItem().getType().equals(Material.DIODE)) {
/* 121:111 */           SethblingDiode(e);
/* 122:    */         }
/* 123:113 */         if (e.getItem().getType().equals(Material.REDSTONE_COMPARATOR)) {
/* 124:115 */           SethblingComparator(e);
/* 125:    */         }
/* 126:117 */         if (e.getItem().getType().equals(Material.REDSTONE)) {
/* 127:119 */           SethblingRedstone(e);
/* 128:    */         }
/* 129:    */       }
/* 130:    */     }
/* 131:    */   }
/* 132:    */   
/* 133:    */   public void ClickSign(Player p, Sign s)
/* 134:    */   {
/* 135:127 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(p);
/* 136:128 */     if (s.getLine(0).equalsIgnoreCase(ChatColor.RED + "Lobby"))
/* 137:    */     {
/* 138:130 */       String mapName = ChatColor.stripColor(s.getLine(1));
/* 139:131 */       SCBGame game = SCBGameManager.getInstance().getGame(mapName);
/* 140:132 */       if (game != null)
/* 141:    */       {
/* 142:134 */         if (bro == null) {
/* 143:135 */           SCBGameManager.getInstance().getGame(mapName).joinLobby(p);
/* 144:    */         } else {
/* 145:137 */           this.plugin.getLogger().severe(bro.getPlayer().getName() + " is already in a game!");
/* 146:    */         }
/* 147:    */       }
/* 148:    */       else {
/* 149:141 */         this.plugin.getLogger().severe("The game " + mapName + " doesn't exist!");
/* 150:    */       }
/* 151:    */     }
/* 152:144 */     if (s.getLine(0).equalsIgnoreCase(ChatColor.GREEN + "In Progress")) {
/* 153:146 */       p.sendMessage(ChatColor.RED + "This game is in progress! Try joining a game that has not started yet.");
/* 154:    */     }
/* 155:148 */     if (s.getLine(0).equalsIgnoreCase(ChatColor.BLUE + "Disabled")) {
/* 156:150 */       p.sendMessage(ChatColor.GREEN + "This arena is disabled. Why not try another one?");
/* 157:    */     }
/* 158:152 */     if (s.getLine(0).equalsIgnoreCase("Click To Go")) {
/* 159:154 */       if ((bro != null) && (bro.isInLobby())) {
/* 160:156 */         bro.getCurrentGame().leaveLobby(p, false);
/* 161:    */       }
/* 162:    */     }
/* 163:159 */     if (s.getLine(1).equalsIgnoreCase(ChatColor.BLACK + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "SPECTATE")) {
/* 164:161 */       if ((PlayerStats.getPlayerRank(p.getName()) > 1) && (bro != null) && (bro.isInLobby()) && (bro.getCurrentGame().isInGame())) {
/* 165:162 */         bro.getCurrentGame().spectate(p);
/* 166:    */       }
/* 167:    */     }
/* 168:164 */     if (s.getLine(1).equalsIgnoreCase(ChatColor.BLACK + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "CHARACTER")) {
/* 169:166 */       if ((bro != null) && (bro.getCurrentGame() != null)) {
/* 170:168 */         ClassMenu.openMenu(p);
/* 171:    */       }
/* 172:    */     }
/* 173:171 */     if (s.getLine(1).equalsIgnoreCase(ChatColor.BLACK + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "GEM")) {
/* 174:173 */       if ((bro != null) && (bro.getCurrentGame() != null)) {
/* 175:175 */         if (gcmmenus.get(p.getName()) != null)
/* 176:    */         {
/* 177:177 */           GemClassMenu gm = (GemClassMenu)gcmmenus.get(p.getName());
/* 178:178 */           gm.openMenu();
/* 179:    */         }
/* 180:    */         else
/* 181:    */         {
/* 182:182 */           GemClassMenu gm = new GemClassMenu(this.plugin, p);
/* 183:183 */           gcmmenus.put(p.getName(), gm);
/* 184:184 */           gm.openMenu();
/* 185:    */         }
/* 186:    */       }
/* 187:    */     }
/* 188:189 */     if (s.getLine(1).equalsIgnoreCase(ChatColor.BLACK + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "VIP")) {
/* 189:191 */       if ((bro != null) && (bro.getCurrentGame() != null)) {
/* 190:193 */         if (vipmenus.get(p.getName()) != null)
/* 191:    */         {
/* 192:195 */           VIPClassMenu vm = (VIPClassMenu)vipmenus.get(p.getName());
/* 193:196 */           vm.openMenu();
/* 194:    */         }
/* 195:    */         else
/* 196:    */         {
/* 197:200 */           VIPClassMenu vm = new VIPClassMenu(this.plugin, p);
/* 198:201 */           vipmenus.put(p.getName(), vm);
/* 199:202 */           vm.openMenu();
/* 200:    */         }
/* 201:    */       }
/* 202:    */     }
/* 203:    */   }
/* 204:    */   
/* 205:    */   public void GemShop(Player p, String s)
/* 206:    */   {
/* 207:210 */     if (s.equalsIgnoreCase(ChatColor.GREEN + "Character Gem Shop " + ChatColor.GRAY + "(Click to Open)")) {
/* 208:212 */       if (gcsmenus.get(p.getName()) != null)
/* 209:    */       {
/* 210:214 */         GemClassShop gs = (GemClassShop)gcsmenus.get(p.getName());
/* 211:215 */         gs.openMenu();
/* 212:    */       }
/* 213:    */       else
/* 214:    */       {
/* 215:219 */         GemClassShop gs = new GemClassShop(this.plugin, p);
/* 216:220 */         gcsmenus.put(p.getName(), gs);
/* 217:221 */         gs.openMenu();
/* 218:    */       }
/* 219:    */     }
/* 220:225 */     if (s.equalsIgnoreCase(ChatColor.GREEN + "Hat Shop " + ChatColor.GRAY + "(Click to Open)")) {
/* 221:227 */       if (hsmenus.get(p.getName()) != null)
/* 222:    */       {
/* 223:229 */         HatShop hs = (HatShop)hsmenus.get(p.getName());
/* 224:230 */         hs.openMenu();
/* 225:    */       }
/* 226:    */       else
/* 227:    */       {
/* 228:234 */         HatShop hs = new HatShop(this.plugin, p);
/* 229:235 */         hsmenus.put(p.getName(), hs);
/* 230:236 */         hs.openMenu();
/* 231:    */       }
/* 232:    */     }
/* 233:    */   }
/* 234:    */   
/* 235:    */   public void Dweller(PlayerInteractEvent e)
/* 236:    */   {
/* 237:243 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 238:244 */     if ((bro != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equalsIgnoreCase("Dweller")))
/* 239:    */     {
/* 240:246 */       ItemStack i = new ItemStack(Material.BONE);
/* 241:247 */       ThrowReason a = ThrowReason.DWELLER;
/* 242:248 */       Player p = e.getPlayer();
/* 243:249 */       Location l = e.getPlayer().getEyeLocation();
/* 244:250 */       Vector v = e.getPlayer().getLocation().getDirection().multiply(2.5D);
/* 245:251 */       new ThrowItem(this.plugin, i, a, p, l, v, true, true);
/* 246:252 */       e.getPlayer().getItemInHand().setType(Material.WOOD_SWORD);
/* 247:253 */       e.getPlayer().getItemInHand().removeEnchantment(Enchantment.DAMAGE_ALL);
/* 248:254 */       e.getPlayer().getItemInHand().removeEnchantment(Enchantment.KNOCKBACK);
/* 249:    */     }
/* 250:    */   }
/* 251:    */   
/* 252:    */   public void TNT(PlayerInteractEvent e)
/* 253:    */   {
/* 254:259 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 255:260 */     if ((bro != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equalsIgnoreCase("TNT")))
/* 256:    */     {
/* 257:262 */       ItemStack i = new ItemStack(Material.TNT);
/* 258:263 */       ThrowReason a = ThrowReason.TNT;
/* 259:264 */       Player p = e.getPlayer();
/* 260:265 */       Location l = e.getPlayer().getEyeLocation();
/* 261:266 */       Vector v = e.getPlayer().getLocation().getDirection().multiply(2.5D);
/* 262:267 */       new ThrowItem(this.plugin, i, a, p, l, v, true, true);
/* 263:268 */       if (e.getItem().getAmount() > 1) {
/* 264:269 */         e.getItem().setAmount(e.getItem().getAmount() - 1);
/* 265:    */       } else {
/* 266:271 */         e.getPlayer().getInventory().remove(e.getItem());
/* 267:    */       }
/* 268:    */     }
/* 269:    */   }
/* 270:    */   
/* 271:    */   public void Witch(PlayerInteractEvent e)
/* 272:    */   {
/* 273:276 */     Player p = e.getPlayer();
/* 274:277 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(p);
/* 275:278 */     if ((bro != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equals("Witch"))) {
/* 276:280 */       if (!witchJump.containsKey(p.getName()))
/* 277:    */       {
/* 278:282 */         witchJump.put(p.getName(), Integer.valueOf(1));
/* 279:283 */         p.setLevel(1);
/* 280:284 */         p.setVelocity(new Vector(p.getVelocity().getX(), 1.0D, p.getVelocity().getZ()));
/* 281:    */       }
/* 282:288 */       else if (((Integer)witchJump.get(p.getName())).intValue() < 4)
/* 283:    */       {
/* 284:290 */         if (p.getLocation().getBlock().getRelative(BlockFace.DOWN).getType() != Material.AIR)
/* 285:    */         {
/* 286:292 */           witchJump.remove(p.getName());
/* 287:293 */           witchJump.put(p.getName(), Integer.valueOf(0));
/* 288:    */         }
/* 289:295 */         p.setVelocity(new Vector(p.getVelocity().getX(), 1.0D, p.getVelocity().getZ()));
/* 290:296 */         int jump = ((Integer)witchJump.get(p.getName())).intValue() + 1;
/* 291:297 */         witchJump.put(p.getName(), Integer.valueOf(jump));
/* 292:298 */         p.setLevel(jump);
/* 293:    */       }
/* 294:302 */       else if (p.getLocation().getBlock().getRelative(BlockFace.DOWN).getType() != Material.AIR)
/* 295:    */       {
/* 296:304 */         witchJump.remove(p.getName());
/* 297:305 */         witchJump.put(p.getName(), Integer.valueOf(0));
/* 298:306 */         p.setVelocity(new Vector(p.getVelocity().getX(), 1.0D, p.getVelocity().getZ()));
/* 299:307 */         int jump = ((Integer)witchJump.get(p.getName())).intValue() + 1;
/* 300:308 */         witchJump.put(p.getName(), Integer.valueOf(jump));
/* 301:309 */         p.setLevel(jump);
/* 302:    */       }
/* 303:    */     }
/* 304:    */   }
/* 305:    */   
/* 306:    */   public void Villager(PlayerInteractEvent e)
/* 307:    */   {
/* 308:317 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 309:318 */     if ((bro != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equalsIgnoreCase("villager")))
/* 310:    */     {
/* 311:320 */       e.setCancelled(true);
/* 312:321 */       e.getPlayer().getInventory().clear();
/* 313:322 */       Potion potion = new Potion(PotionType.SLOWNESS, 2).splash();
/* 314:323 */       ItemStack itemStack = new ItemStack(Material.POTION);
/* 315:324 */       potion.apply(itemStack);
/* 316:325 */       PotionMeta meta = (PotionMeta)itemStack.getItemMeta();
/* 317:326 */       meta.addCustomEffect(new PotionEffect(PotionEffectType.SLOW, 200, 2), true);
/* 318:327 */       itemStack.setItemMeta(meta);
/* 319:328 */       ThrownPotion thrownPotion = (ThrownPotion)e.getPlayer().launchProjectile(ThrownPotion.class);
/* 320:329 */       thrownPotion.setItem(itemStack);
/* 321:    */     }
/* 322:    */   }
/* 323:    */   
/* 324:    */   public void Butterbro(PlayerInteractEvent e)
/* 325:    */   {
/* 326:334 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 327:335 */     if ((bro != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equalsIgnoreCase("butterbro")))
/* 328:    */     {
/* 329:337 */       e.setCancelled(true);
/* 330:338 */       if (e.getItem().getAmount() > 1) {
/* 331:339 */         e.getItem().setAmount(e.getItem().getAmount() - 1);
/* 332:    */       } else {
/* 333:341 */         e.getPlayer().getInventory().clear();
/* 334:    */       }
/* 335:342 */       e.getPlayer().launchProjectile(SmallFireball.class);
/* 336:    */     }
/* 337:    */   }
/* 338:    */   
/* 339:    */   public void Ocelot(PlayerInteractEvent e)
/* 340:    */   {
/* 341:347 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 342:348 */     if ((bro != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equalsIgnoreCase("ocelot")))
/* 343:    */     {
/* 344:350 */       e.setCancelled(true);
/* 345:351 */       if (e.getItem().getAmount() > 1) {
/* 346:352 */         e.getItem().setAmount(e.getItem().getAmount() - 1);
/* 347:    */       } else {
/* 348:354 */         e.getPlayer().getInventory().clear();
/* 349:    */       }
/* 350:355 */       e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 2), true);
/* 351:    */     }
/* 352:    */   }
/* 353:    */   
/* 354:    */   public void Notch(PlayerInteractEvent e)
/* 355:    */   {
/* 356:360 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 357:361 */     if ((bro != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equalsIgnoreCase("notch")))
/* 358:    */     {
/* 359:363 */       e.setCancelled(true);
/* 360:364 */       if (e.getItem().getAmount() > 1) {
/* 361:365 */         e.getItem().setAmount(e.getItem().getAmount() - 1);
/* 362:    */       } else {
/* 363:367 */         e.getPlayer().getInventory().clear();
/* 364:    */       }
/* 365:369 */       int i = 0;
/* 366:370 */       SCBGame game = bro.getCurrentGame();
/* 367:371 */       for (Entity entity : bro.getPlayer().getNearbyEntities(200.0D, 200.0D, 200.0D)) {
/* 368:373 */         if ((entity instanceof Player)) {
/* 369:375 */           if ((SCBGameManager.getInstance().getCraftBrother((Player)entity) != null) && (SCBGameManager.getInstance().getCraftBrother((Player)entity).getCurrentGame().equals(game))) {
/* 370:377 */             if (i < 2)
/* 371:    */             {
/* 372:379 */               ((Player)entity).teleport(e.getPlayer());
/* 373:380 */               i++;
/* 374:    */             }
/* 375:    */             else
/* 376:    */             {
/* 377:383 */               return;
/* 378:    */             }
/* 379:    */           }
/* 380:    */         }
/* 381:    */       }
/* 382:    */     }
/* 383:    */   }
/* 384:    */   
/* 385:    */   public void Jeb(PlayerInteractEvent e)
/* 386:    */   {
/* 387:391 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 388:392 */     if ((bro != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equalsIgnoreCase("jeb")))
/* 389:    */     {
/* 390:394 */       e.setCancelled(true);
/* 391:395 */       if (e.getItem().getAmount() > 1) {
/* 392:396 */         e.getItem().setAmount(e.getItem().getAmount() - 1);
/* 393:    */       } else {
/* 394:398 */         e.getPlayer().getInventory().clear();
/* 395:    */       }
/* 396:400 */       SCBGame game = bro.getCurrentGame();
/* 397:401 */       for (Entity entity : bro.getPlayer().getNearbyEntities(200.0D, 200.0D, 200.0D)) {
/* 398:403 */         if ((entity instanceof Player)) {
/* 399:405 */           if ((SCBGameManager.getInstance().getCraftBrother((Player)entity) != null) && (SCBGameManager.getInstance().getCraftBrother((Player)entity).getCurrentGame().equals(game)))
/* 400:    */           {
/* 401:407 */             Player p = (Player)entity;
/* 402:408 */             float yaw = p.getLocation().getYaw();
/* 403:    */             String str;
/* 404:409 */             switch ((str = getDirection(Float.valueOf(yaw))).hashCode())
/* 405:    */             {
/* 406:    */             case 2152477: 
/* 407:409 */               if (str.equals("East")) {}
/* 408:    */               break;
/* 409:    */             case 2692559: 
/* 410:409 */               if (str.equals("West")) {}
/* 411:    */               break;
/* 412:    */             case 75454693: 
/* 413:409 */               if (str.equals("North")) {
/* 414:    */                 break;
/* 415:    */               }
/* 416:    */               break;
/* 417:    */             case 80075181: 
/* 418:409 */               if (!str.equals("South"))
/* 419:    */               {
								p.setVelocity(new Vector(p.getVelocity().getX(), 0.8D, 3.0D));
/* 421:413 */                 continue;
/* 422:    */                 
/* 424:    */               }
/* 425:    */               else
/* 426:    */               {
/* 427:418 */                 p.setVelocity(new Vector(p.getVelocity().getX(), 0.8D, -3.0D));
/* 428:419 */                 continue;
/* 429:    */          
/* 431:    */               }
/* 433:    */             }
/* 434:    */           }
/* 435:    */         }
/* 436:    */       }
/* 437:    */     }
/* 438:    */   }
/* 439:    */   
/* 440:    */   public void Mollstam(PlayerInteractEvent e)
/* 441:    */   {
/* 442:435 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 443:436 */     if ((bro != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equalsIgnoreCase("mollstam")))
/* 444:    */     {
/* 445:438 */       int i = this.random.nextInt(10) + 1;
/* 446:439 */       if ((i > 0) && (i < 4)) {
/* 447:440 */         i = 1;
/* 448:    */       }
/* 449:441 */       if ((i > 3) && (i < 7)) {
/* 450:442 */         i = 2;
/* 451:    */       }
/* 452:443 */       if ((i > 6) && (i < 10)) {
/* 453:444 */         i = 3;
/* 454:    */       }
/* 455:445 */       switch (i)
/* 456:    */       {
/* 457:    */       case 1: 
/* 458:448 */         if (e.getPlayer().getHealth() < 13.0D) {
/* 459:449 */           e.getPlayer().setHealth(e.getPlayer().getHealth() + 7.0D);
/* 460:    */         } else {
/* 461:451 */           e.getPlayer().setHealth(20.0D);
/* 462:    */         }
/* 463:452 */         e.getPlayer().sendMessage("§aThe power of Mollstam heals you...");
/* 464:453 */         break;
/* 465:    */       case 2: 
/* 466:455 */         e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 200, 3));
/* 467:456 */         e.getPlayer().sendMessage("§aThe power of Mollstam regenerates you...");
/* 468:457 */         break;
/* 469:    */       case 3: 
/* 470:459 */         if (e.getPlayer().getNearbyEntities(200.0D, 200.0D, 200.0D) == null) {
/* 471:460 */           e.getItem().setAmount(e.getItem().getAmount() + 1);
/* 472:    */         }
/* 473:461 */         for (Entity entity : e.getPlayer().getNearbyEntities(200.0D, 200.0D, 200.0D)) {
/* 474:463 */           if ((entity instanceof Player))
/* 475:    */           {
/* 476:465 */             Player victim = (Player)entity;
/* 477:466 */             CraftBrother brother = SCBGameManager.getInstance().getCraftBrother(victim);
/* 478:467 */             if (!brother.getCurrentGame().equals(bro.getCurrentGame())) {
/* 479:    */               break;
/* 480:    */             }
/* 481:469 */             if ((!victim.equals(e.getPlayer())) && (victim.getHealth() > 7.0D))
/* 482:    */             {
/* 483:470 */               victim.setHealth(victim.getHealth() - 7.0D); break;
/* 484:    */             }
/* 485:472 */             victim.setHealth(0.0D);
/* 486:    */             
/* 487:474 */             break;
/* 488:    */           }
/* 489:    */         }
/* 490:477 */         e.getPlayer().sendMessage("§aThe power of Mollstam has weakened your enemies...");
/* 491:478 */         break;
/* 492:    */       case 10: 
/* 493:480 */         bro.setLivesLeft(bro.getLivesLeft() + 1);
/* 494:481 */         bro.getCurrentGame().arenaStats();
/* 495:482 */         e.getPlayer().sendMessage("§aThe power of Mollstam has given you an extra life...");
/* 496:    */       }
/* 497:485 */       if (e.getItem().getAmount() > 1) {
/* 498:487 */         e.getItem().setAmount(e.getItem().getAmount() - 1);
/* 499:    */       } else {
/* 500:491 */         e.getPlayer().getInventory().remove(e.getItem());
/* 501:    */       }
/* 502:    */     }
/* 503:    */   }
/* 504:    */   
/* 505:    */   public void Pokeballs(PlayerInteractEvent e)
/* 506:    */   {
/* 507:497 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 508:498 */     if (bro != null)
/* 509:    */     {
/* 510:500 */       e.setCancelled(true);
/* 511:501 */       int amount = e.getPlayer().getItemInHand().getAmount();
/* 512:502 */       ItemStack i = e.getItem();
/* 513:503 */       i.setAmount(1);
/* 514:504 */       ThrowReason a = ThrowReason.SPAWNEGG;
/* 515:505 */       Player p = e.getPlayer();
/* 516:506 */       Location l = e.getPlayer().getEyeLocation();
/* 517:507 */       Vector v = e.getPlayer().getEyeLocation().getDirection().multiply(2.5D);
/* 518:508 */       new ThrowItem(this.plugin, i, a, p, l, v, true, true);
/* 519:509 */       if (amount > 1) {
/* 520:510 */         e.getPlayer().getItemInHand().setAmount(amount - 1);
/* 521:    */       } else {
/* 522:512 */         e.getPlayer().getInventory().remove(e.getItem());
/* 523:    */       }
/* 524:    */     }
/* 525:    */   }
/* 526:    */   
/* 527:    */   public void SethblingDiode(PlayerInteractEvent e)
/* 528:    */   {
/* 529:517 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 530:518 */     if ((bro != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equalsIgnoreCase("Sethbling")))
/* 531:    */     {
/* 532:520 */       ItemStack i = new ItemStack(Material.DIODE);
/* 533:521 */       ThrowReason a = ThrowReason.SETHDIODE;
/* 534:522 */       Player p = e.getPlayer();
/* 535:523 */       Location l = e.getPlayer().getEyeLocation();
/* 536:524 */       Vector v = e.getPlayer().getLocation().getDirection().multiply(2.5D);
/* 537:525 */       new ThrowItem(this.plugin, i, a, p, l, v, true, true);
/* 538:526 */       if (e.getItem().getAmount() > 1) {
/* 539:527 */         e.getItem().setAmount(e.getItem().getAmount() - 1);
/* 540:    */       } else {
/* 541:529 */         e.getPlayer().getInventory().remove(e.getItem());
/* 542:    */       }
/* 543:    */     }
/* 544:    */   }
/* 545:    */   
/* 546:    */   public void SethblingComparator(PlayerInteractEvent e)
/* 547:    */   {
/* 548:534 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 549:535 */     if ((bro != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equalsIgnoreCase("Sethbling")))
/* 550:    */     {
/* 551:537 */       ItemStack i = new ItemStack(Material.REDSTONE_COMPARATOR);
/* 552:538 */       ThrowReason a = ThrowReason.SETHCOMPARATOR;
/* 553:539 */       Player p = e.getPlayer();
/* 554:540 */       Location l = e.getPlayer().getEyeLocation();
/* 555:541 */       Vector v = e.getPlayer().getLocation().getDirection().multiply(2.5D);
/* 556:542 */       new ThrowItem(this.plugin, i, a, p, l, v, true, true);
/* 557:543 */       if (e.getItem().getAmount() > 1) {
/* 558:544 */         e.getItem().setAmount(e.getItem().getAmount() - 1);
/* 559:    */       } else {
/* 560:546 */         e.getPlayer().getInventory().remove(e.getItem());
/* 561:    */       }
/* 562:    */     }
/* 563:    */   }
/* 564:    */   
/* 565:    */   public void SethblingRedstone(PlayerInteractEvent e)
/* 566:    */   {
/* 567:551 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 568:552 */     if ((bro != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equalsIgnoreCase("Sethbling")))
/* 569:    */     {
/* 570:554 */       ItemStack i = new ItemStack(Material.REDSTONE);
/* 571:555 */       ThrowReason a = ThrowReason.SETHREDSTONE;
/* 572:556 */       Player p = e.getPlayer();
/* 573:557 */       Location l = e.getPlayer().getEyeLocation();
/* 574:558 */       Vector v = e.getPlayer().getLocation().getDirection().multiply(2.5D);
/* 575:    */       
/* 576:560 */       new ThrowItem(this.plugin, i, a, p, l, v, false, true);
/* 577:561 */       if (e.getItem().getAmount() > 1) {
/* 578:562 */         e.getItem().setAmount(e.getItem().getAmount() - 1);
/* 579:    */       } else {
/* 580:564 */         e.getPlayer().getInventory().remove(e.getItem());
/* 581:    */       }
/* 582:    */     }
/* 583:    */   }
/* 584:    */   
/* 585:    */   public String getDirection(Float yaw)
/* 586:    */   {
/* 587:569 */     yaw = Float.valueOf(yaw.floatValue() / 90.0F);
/* 588:570 */     yaw = Float.valueOf(Math.round(yaw.floatValue()));
/* 589:572 */     if ((yaw.floatValue() == -4.0F) || (yaw.floatValue() == 0.0F) || (yaw.floatValue() == 4.0F)) {
/* 590:572 */       return "South";
/* 591:    */     }
/* 592:573 */     if ((yaw.floatValue() == -1.0F) || (yaw.floatValue() == 3.0F)) {
/* 593:573 */       return "East";
/* 594:    */     }
/* 595:574 */     if ((yaw.floatValue() == -2.0F) || (yaw.floatValue() == 2.0F)) {
/* 596:574 */       return "North";
/* 597:    */     }
/* 598:575 */     if ((yaw.floatValue() == -3.0F) || (yaw.floatValue() == 1.0F)) {
/* 599:575 */       return "West";
/* 600:    */     }
/* 601:576 */     return "";
/* 602:    */   }
/* 603:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.PlayerInteractListener
 * JD-Core Version:    0.7.0.1
 */