/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.utilities.WorldChecker;

/*  6:   */ import org.bukkit.Bukkit;
/*  7:   */ import org.bukkit.entity.Player;
/*  8:   */ import org.bukkit.event.EventHandler;
/*  9:   */ import org.bukkit.event.EventPriority;
/* 10:   */ import org.bukkit.event.Listener;
/* 11:   */ import org.bukkit.event.player.PlayerPickupItemEvent;
/* 12:   */ 
/* 13:   */ public class DPlayerPickupItemListener
/* 14:   */   implements Listener
/* 15:   */ {
/* 16:   */   @EventHandler(priority=EventPriority.HIGH)
/* 17:   */   public void PlayerPickupItemListener(PlayerPickupItemEvent event)
/* 18:   */   {
/* 19:27 */     if (SCBGameManager.getInstance().getMainLobby() == null)
/* 20:   */     {
/* 21:29 */       Bukkit.getLogger().severe("[SCB] You have not set a main lobby yet!");
/* 22:30 */       return;
/* 23:   */     }
/* 24:32 */     Player player = event.getPlayer();
/* 25:33 */     if (WorldChecker.mainLobbyGame(player))
/* 26:   */     {
/* 27:35 */       if (player.hasPermission("scb.bypass.playerpickupitemevent")) {
/* 28:36 */         return;
/* 29:   */       }
/* 30:38 */       event.setCancelled(true);
/* 31:   */     }
/* 32:   */   }
/* 33:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.DPlayerPickupItemListener
 * JD-Core Version:    0.7.0.1
 */