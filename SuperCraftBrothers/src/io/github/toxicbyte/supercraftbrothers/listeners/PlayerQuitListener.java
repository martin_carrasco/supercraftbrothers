/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.PlayerStats;
import io.github.toxicbyte.supercraftbrothers.SCBGame;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;

/*  9:   */ import org.bukkit.event.EventHandler;
/* 10:   */ import org.bukkit.event.EventPriority;
/* 11:   */ import org.bukkit.event.Listener;
/* 12:   */ import org.bukkit.event.player.PlayerQuitEvent;
/* 13:   */ 
/* 14:   */ public class PlayerQuitListener
/* 15:   */   implements Listener
/* 16:   */ {
/* 17:   */   @EventHandler(priority=EventPriority.MONITOR)
/* 18:   */   public void onPlayerQuit(PlayerQuitEvent event)
/* 19:   */   {
/* 20:18 */     PlayerStats.setPlayerSpawning(event.getPlayer().getName(), true);
/* 21:19 */     String name = event.getPlayer().getName();
/* 22:20 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(event.getPlayer());
/* 23:21 */     if (bro != null)
/* 24:   */     {
/* 25:23 */       SCBGame game = bro.getCurrentGame();
/* 26:24 */       if (game.isInGame()) {
/* 27:26 */         game.leaveGame(event.getPlayer(), true);
/* 28:   */       } else {
/* 29:30 */         game.leaveLobby(event.getPlayer(), true);
/* 30:   */       }
/* 31:   */     }
/* 32:33 */     for (SCBGame game : SCBGameManager.getInstance().getAllGames())
/* 33:   */     {
/* 34:35 */       if (game.getHashMap(0).containsKey(name)) {
/* 35:37 */         game.getHashMap(0).remove(name);
/* 36:   */       }
/* 37:39 */       if (game.getHashMap(1).containsKey(name)) {
/* 38:41 */         game.getHashMap(1).remove(name);
/* 39:   */       }
/* 40:   */     }
/* 41:44 */     if (PlayerInteractListener.gcmmenus.containsKey(event.getPlayer().getName())) {
/* 42:46 */       PlayerInteractListener.gcmmenus.remove(event.getPlayer().getName());
/* 43:   */     }
/* 44:48 */     if (PlayerInteractListener.gcsmenus.containsKey(event.getPlayer().getName())) {
/* 45:50 */       PlayerInteractListener.gcsmenus.remove(event.getPlayer().getName());
/* 46:   */     }
/* 47:52 */     if (PlayerInteractListener.hsmenus.containsKey(event.getPlayer().getName())) {
/* 48:54 */       PlayerInteractListener.hsmenus.remove(event.getPlayer().getName());
/* 49:   */     }
/* 50:56 */     if (PlayerInteractListener.vipmenus.containsKey(event.getPlayer().getName())) {
/* 51:58 */       PlayerInteractListener.vipmenus.remove(event.getPlayer().getName());
/* 52:   */     }
/* 53:   */   }
/* 54:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.PlayerQuitListener
 * JD-Core Version:    0.7.0.1
 */