/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.events.SCBDeathEvent;
import io.github.toxicbyte.supercraftbrothers.utilities.WorldChecker;

/*  8:   */ import org.bukkit.Bukkit;
/* 10:   */ import org.bukkit.event.EventHandler;
/* 11:   */ import org.bukkit.event.EventPriority;
/* 12:   */ import org.bukkit.event.Listener;
/* 13:   */ import org.bukkit.event.entity.PlayerDeathEvent;
/* 15:   */ 
/* 16:   */ public class PlayerDeathListener
/* 17:   */   implements Listener
/* 18:   */ {
/* 19:   */   @EventHandler(priority=EventPriority.MONITOR)
/* 20:   */   public void onPlayerDeath(PlayerDeathEvent event)
/* 21:   */   {
/* 22:26 */     if (WorldChecker.mainLobbyGame(event.getEntity()))
/* 23:   */     {
/* 24:28 */       if (SCBGameManager.getInstance().isInGame(event.getEntity().getName()))
/* 25:   */       {
/* 26:30 */         CraftBrother bro1 = SCBGameManager.getInstance().getCraftBrother(event.getEntity());
/* 27:31 */         CraftBrother bro2 = SCBGameManager.getInstance().getCraftBrother(bro1.getLastDamagedBy());
/* 28:32 */         bro1.setDeaths(bro1.getDeaths() + 1);
/* 29:33 */         String deathmessage = event.getDeathMessage();
/* 30:34 */         Bukkit.getPluginManager().callEvent(new SCBDeathEvent(bro1, bro2, bro1.getCurrentGame(), deathmessage));
/* 31:35 */         bro1.setLastDamagedBy(null);
/* 32:   */       }
/* 33:37 */       event.setDeathMessage(null);
/* 34:38 */       event.setDroppedExp(0);
/* 35:39 */       event.getDrops().clear();
/* 36:   */     }
/* 37:   */   }
/* 38:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.PlayerDeathListener
 * JD-Core Version:    0.7.0.1
 */