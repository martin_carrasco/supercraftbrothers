/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ /*  8:   */ import io.github.toxicbyte.supercraftbrothers.GemScoreboard;
import io.github.toxicbyte.supercraftbrothers.InventoryConfig;
import io.github.toxicbyte.supercraftbrothers.PlayerStats;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.SuperCraftBrothers;
import io.github.toxicbyte.supercraftbrothers.utilities.WorldChecker;

/*  9:   */ import java.io.File;
/* 10:   */ import java.io.IOException;

/* 12:   */ import org.bukkit.Bukkit;
/* 14:   */ import org.bukkit.configuration.file.FileConfiguration;
/* 15:   */ import org.bukkit.configuration.file.YamlConfiguration;
/* 17:   */ import org.bukkit.event.EventHandler;
/* 18:   */ import org.bukkit.event.EventPriority;
/* 19:   */ import org.bukkit.event.Listener;
/* 20:   */ import org.bukkit.event.player.PlayerJoinEvent;
/* 22:   */ 
/* 23:   */ public class PlayerJoinListener
/* 24:   */   implements Listener
/* 25:   */ {
/* 26:   */   private SuperCraftBrothers plugin;
/* 27:   */   
/* 28:   */   public PlayerJoinListener(SuperCraftBrothers superCraftBrothers)
/* 29:   */   {
/* 30:22 */     this.plugin = superCraftBrothers;
/* 31:   */   }
/* 32:   */   
/* 33:   */   @EventHandler(priority=EventPriority.HIGH)
/* 34:   */   public void onPlayerJoin(final PlayerJoinEvent event)
/* 35:   */   {
/* 36:28 */     File userfiles = new File("plugins" + File.separator + "SuperCraftBrothers" + File.separator + "users" + File.separator + event.getPlayer().getName() + ".yml");
/* 37:29 */     if (!userfiles.exists())
/* 38:   */     {
/* 39:31 */       FileConfiguration config = YamlConfiguration.loadConfiguration(userfiles);
/* 40:32 */       config.set("rank", "DEFAULT");
/* 41:33 */       config.set("unlocked_characters", null);
/* 42:34 */       config.set("unlocked_maps", null);
/* 43:35 */       config.set("gems", Integer.valueOf(0));
/* 44:36 */       config.set("kills", Integer.valueOf(0));
/* 45:37 */       config.set("deaths", Integer.valueOf(0));
/* 46:38 */       config.set("wins", Integer.valueOf(0));
/* 47:39 */       config.set("quitingame", Boolean.valueOf(false));
/* 48:   */       try
/* 49:   */       {
/* 50:42 */         config.save(userfiles);
/* 51:43 */         Bukkit.getLogger().info("[SCB] Created new player file for " + event.getPlayer().getName());
/* 52:   */       }
/* 53:   */       catch (IOException e)
/* 54:   */       {
/* 55:47 */         e.printStackTrace();
/* 56:   */       }
/* 57:   */     }
/* 58:52 */     else if (PlayerStats.getPlayerSpawning(event.getPlayer().getName()))
/* 59:   */     {
/* 60:54 */       event.getPlayer().teleport(SCBGameManager.getInstance().getMainLobby());
/* 61:   */     }
/* 62:57 */     if (WorldChecker.mainLobbyGame(event.getPlayer()))
/* 63:   */     {
/* 64:59 */       InventoryConfig.giveItems(event.getPlayer());
/* 65:60 */       GemScoreboard.addPlayer(event.getPlayer());
/* 66:61 */       event.getPlayer().setAllowFlight(true);
/* 67:   */     }
/* 68:63 */     if (event.getPlayer().hasPermission("scb.notify")) {
/* 69:65 */       SuperCraftBrothers.getInstance().getServer().getScheduler().runTaskLater(this.plugin, new Runnable()
/* 70:   */       {
/* 71:   */         public void run()
/* 72:   */         {
/* 73:70 */           if (SuperCraftBrothers.getInstance().update)
/* 74:   */           {
/* 75:72 */             event.getPlayer().sendMessage("[§aSCB§r] An update is available: " + SuperCraftBrothers.getInstance().name);
/* 76:73 */             event.getPlayer().sendMessage("Type /scb update if you would like to update.");
/* 77:74 */             event.getPlayer().sendMessage("After updating, please restart the server for changes to take effect");
/* 78:   */           }
/* 79:   */         }
/* 80:77 */       }, 20L);
/* 81:   */     }
/* 82:   */   }
/* 83:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.PlayerJoinListener
 * JD-Core Version:    0.7.0.1
 */