/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.utilities.WorldChecker;

/*  6:   */ import org.bukkit.Bukkit;
/*  7:   */ import org.bukkit.ChatColor;
/*  9:   */ import org.bukkit.block.Sign;
/* 10:   */ import org.bukkit.entity.Player;
/* 11:   */ import org.bukkit.event.EventHandler;
/* 12:   */ import org.bukkit.event.EventPriority;
/* 13:   */ import org.bukkit.event.Listener;
/* 14:   */ import org.bukkit.event.block.BlockBreakEvent;
/* 15:   */ 
/* 16:   */ public class DBlockBreakListener
/* 17:   */   implements Listener
/* 18:   */ {
/* 19:   */   @EventHandler(priority=EventPriority.HIGH)
/* 20:   */   public void BlockBreak(BlockBreakEvent event)
/* 21:   */   {
/* 22:29 */     if (SCBGameManager.getInstance().getMainLobby() == null)
/* 23:   */     {
/* 24:31 */       Bukkit.getLogger().severe("[SCB] You have not set a main lobby yet!");
/* 25:32 */       return;
/* 26:   */     }
/* 27:34 */     Player player = event.getPlayer();
/* 28:35 */     if (WorldChecker.mainLobbyGame(player)) {
/* 29:37 */       if (player.hasPermission("scb.bypass.blockbreakevent")) {
/* 30:38 */         event.setCancelled(false);
/* 31:   */       } else {
/* 32:40 */         event.setCancelled(true);
/* 33:   */       }
/* 34:   */     }
/* 35:   */   }
/* 36:   */   
/* 37:   */   @EventHandler(priority=EventPriority.HIGH)
/* 38:   */   public void SignBreakEvent(BlockBreakEvent e)
/* 39:   */   {
/* 40:47 */     if (SCBGameManager.getInstance().getMainLobby() == null)
/* 41:   */     {
/* 42:49 */       Bukkit.getLogger().severe("[SCB] You have not set a main lobby yet!");
/* 43:50 */       return;
/* 44:   */     }
/* 45:52 */     if (((e.getBlock().getState() instanceof Sign)) && (e.getPlayer().hasPermission("scb.deletearena")))
/* 46:   */     {
/* 47:54 */       Sign s = (Sign)e.getBlock().getState();
/* 48:55 */       if ((s.getLine(0).equals(ChatColor.RED + "Lobby")) || 
/* 49:56 */         (s.getLine(0).equals(ChatColor.BLUE + "Disabled")))
/* 50:   */       {
/* 51:58 */         String mapname = s.getLine(1);
/* 52:59 */         if (mapname != null) {
/* 53:60 */           SCBGameManager.getInstance().deleteGame(mapname);
/* 54:   */         }
/* 55:   */       }
/* 56:   */     }
/* 57:   */   }
/* 58:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.DBlockBreakListener
 * JD-Core Version:    0.7.0.1
 */