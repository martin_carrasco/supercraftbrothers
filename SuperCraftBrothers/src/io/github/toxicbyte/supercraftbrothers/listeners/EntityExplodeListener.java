/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.SCBGame;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;

/*  8:   */ import org.bukkit.event.EventHandler;
/*  9:   */ import org.bukkit.event.EventPriority;
/* 10:   */ import org.bukkit.event.Listener;
/* 11:   */ import org.bukkit.event.entity.EntityExplodeEvent;
/* 12:   */ 
/* 13:   */ public class EntityExplodeListener
/* 14:   */   implements Listener
/* 15:   */ {
/* 16:   */   @EventHandler(priority=EventPriority.HIGH, ignoreCancelled=true)
/* 17:   */   public void onEntityExplode(EntityExplodeEvent event)
/* 18:   */   {
/* 19:16 */     for (SCBGame game : SCBGameManager.getInstance().getAllGames())
/* 20:   */     {
/* 21:18 */       String world = game.getMap().getClassLobby().getWorld().getName();
/* 22:19 */       if (event.getLocation().getWorld().getName().equalsIgnoreCase(world)) {
/* 23:21 */         event.setCancelled(true);
/* 24:   */       }
/* 25:   */     }
/* 26:   */   }
/* 27:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.EntityExplodeListener
 * JD-Core Version:    0.7.0.1
 */