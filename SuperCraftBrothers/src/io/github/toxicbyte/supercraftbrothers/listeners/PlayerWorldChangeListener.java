/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.GemScoreboard;
import io.github.toxicbyte.supercraftbrothers.PlayerStats;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.utilities.WorldChecker;

/*  8:   */ import org.bukkit.Bukkit;
/* 10:   */ import org.bukkit.event.EventHandler;
/* 11:   */ import org.bukkit.event.EventPriority;
/* 12:   */ import org.bukkit.event.Listener;
/* 13:   */ import org.bukkit.event.player.PlayerChangedWorldEvent;
/* 15:   */ 
/* 16:   */ public class PlayerWorldChangeListener
/* 17:   */   implements Listener
/* 18:   */ {
/* 19:   */   @EventHandler(priority=EventPriority.HIGH)
/* 20:   */   public void PlayerWorldChange(PlayerChangedWorldEvent e)
/* 21:   */   {
/* 22:20 */     if (!WorldChecker.mainLobbyGame(e.getPlayer()))
/* 23:   */     {
/* 24:22 */       e.getPlayer().setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
/* 25:23 */       e.getPlayer().setAllowFlight(false);
/* 26:   */     }
/* 27:   */     else
/* 28:   */     {
/* 29:27 */       CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 30:28 */       if (bro == null)
/* 31:   */       {
/* 32:30 */         if (PlayerStats.getPlayerRank(e.getPlayer().getName()) > 1) {
/* 33:32 */           e.getPlayer().setAllowFlight(true);
/* 34:   */         }
/* 35:   */       }
/* 36:37 */       else if (bro.isInLobby())
/* 37:   */       {
/* 38:39 */         if (PlayerStats.getPlayerRank(e.getPlayer().getName()) > 1) {
/* 39:41 */           e.getPlayer().setAllowFlight(true);
/* 40:   */         }
/* 41:   */       }
/* 42:   */       else {
/* 43:46 */         e.getPlayer().setAllowFlight(true);
/* 44:   */       }
/* 45:49 */       GemScoreboard.addPlayer(e.getPlayer());
/* 46:   */     }
/* 47:   */   }
/* 48:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.PlayerWorldChangeListener
 * JD-Core Version:    0.7.0.1
 */