/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.utilities.WorldChecker;

/*  4:   */ import org.bukkit.entity.Player;
/*  5:   */ import org.bukkit.event.EventHandler;
/*  6:   */ import org.bukkit.event.EventPriority;
/*  7:   */ import org.bukkit.event.Listener;
/*  8:   */ import org.bukkit.event.entity.FoodLevelChangeEvent;
/*  9:   */ 
/* 10:   */ public class FoodLevelListener
/* 11:   */   implements Listener
/* 12:   */ {
/* 13:   */   @EventHandler(priority=EventPriority.HIGH)
/* 14:   */   public void HungerLoss(FoodLevelChangeEvent e)
/* 15:   */   {
/* 16:16 */     if ((e.getEntity() instanceof Player))
/* 17:   */     {
/* 18:18 */       Player p = (Player)e.getEntity();
/* 19:19 */       if (WorldChecker.mainLobbyGame(p)) {
/* 20:20 */         e.setFoodLevel(20);
/* 21:   */       }
/* 22:   */     }
/* 23:   */   }
/* 24:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.FoodLevelListener
 * JD-Core Version:    0.7.0.1
 */