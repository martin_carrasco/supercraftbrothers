/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.GemScoreboard;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.SuperCraftBrothers;

/* 10:   */ import java.util.Random;

/* 11:   */ import org.bukkit.Bukkit;
/* 12:   */ import org.bukkit.ChatColor;
/* 13:   */ import org.bukkit.Location;
/* 15:   */ import org.bukkit.event.EventHandler;
/* 16:   */ import org.bukkit.event.EventPriority;
/* 17:   */ import org.bukkit.event.Listener;
/* 18:   */ import org.bukkit.event.player.PlayerRespawnEvent;
/* 20:   */ 
/* 21:   */ public class PlayerRespawnListener
/* 22:   */   implements Listener
/* 23:   */ {
/* 24:20 */   private Random rand = new Random();
/* 25:   */   
/* 26:   */   @EventHandler(priority=EventPriority.MONITOR)
/* 27:   */   public void onPlayerRespawn(final PlayerRespawnEvent event)
/* 28:   */   {
/* 29:25 */     if (SCBGameManager.getInstance().getCraftBrother(event.getPlayer()) != null)
/* 30:   */     {
/* 31:27 */       final CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(event.getPlayer());
/* 32:28 */       if (bro.getReason().equals(CraftBrother.respawnreason.TOLOBBY))
/* 33:   */       {
/* 34:30 */         event.setRespawnLocation(bro.getCurrentGame().getMap().getClassLobby());
/* 35:31 */         Bukkit.getScheduler().runTask(SuperCraftBrothers.getInstance(), new Runnable()
/* 36:   */         {
/* 37:   */           public void run()
/* 38:   */           {
/* 39:36 */             SCBGameManager.getInstance().getCraftBrother(event.getPlayer()).setInLobby(true);
/* 40:37 */             event.getPlayer().setAllowFlight(true);
/* 41:38 */             event.getPlayer().setLastDamage(0.0D);
/* 42:39 */             GemScoreboard.addPlayer(event.getPlayer());
/* 43:   */           }
/* 44:   */         });
/* 45:   */       }
/* 46:43 */       if (bro.getReason().equals(CraftBrother.respawnreason.TOGAME))
/* 47:   */       {
/* 48:45 */         bro.setRespawning(false);
/* 49:46 */         if (bro.getLivesLeft() > 0)
/* 50:   */         {
/* 51:   */           Location resp;
/* 55:49 */           switch (this.rand.nextInt(4))
/* 56:   */           {
/* 57:   */           case 0: 
/* 58:51 */             resp = bro.getCurrentGame().getMap().getSp1();
/* 59:52 */             break;
/* 60:   */           case 1: 
/* 61:54 */             resp = bro.getCurrentGame().getMap().getSp2();
/* 62:55 */             break;
/* 63:   */           case 2: 
/* 64:57 */             resp = bro.getCurrentGame().getMap().getSp3();
/* 65:58 */             break;
/* 66:   */           case 3: 
/* 67:   */           default: 
/* 68:61 */             resp = bro.getCurrentGame().getMap().getSp4();
/* 69:   */           }
/* 70:64 */           event.setRespawnLocation(resp);
/* 71:65 */           event.getPlayer().sendMessage(ChatColor.RED + "You have " + bro.getLivesLeft() + (bro.getLivesLeft() == 1 ? " life" : " lives") + " remaining");
/* 72:   */           
/* 73:67 */           Bukkit.getScheduler().runTask(SuperCraftBrothers.getInstance(), new Runnable()
/* 74:   */           {
/* 75:   */             public void run()
/* 76:   */             {
/* 77:71 */               bro.getPlayer().setAllowFlight(true);
/* 78:72 */               bro.getPlayer().setLastDamage(0.0D);
/* 79:73 */               bro.heal();
/* 80:74 */               bro.apply();
/* 81:75 */               bro.getCurrentGame().arenaStats();
/* 82:   */             }
/* 83:   */           });
/* 84:   */         }
/* 85:   */       }
/* 86:80 */       if (bro.getReason().equals(CraftBrother.respawnreason.TOMAIN))
/* 87:   */       {
/* 88:82 */         event.setRespawnLocation(SCBGameManager.getInstance().getMainLobby());
/* 89:83 */         GemScoreboard.addPlayer(event.getPlayer());
/* 90:   */       }
/* 91:   */     }
/* 92:   */   }
/* 93:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.PlayerRespawnListener
 * JD-Core Version:    0.7.0.1
 */