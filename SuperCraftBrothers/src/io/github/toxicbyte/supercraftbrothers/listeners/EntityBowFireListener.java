/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ /*  5:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.utilities.WorldChecker;

/*  6:   */ import java.util.HashMap;

/*  7:   */ import org.bukkit.Color;
/*  8:   */ import org.bukkit.FireworkEffect;
/* 13:   */ import org.bukkit.entity.Entity;
/* 14:   */ import org.bukkit.entity.Firework;
/* 15:   */ import org.bukkit.entity.Player;
/* 17:   */ import org.bukkit.entity.WitherSkull;
/* 18:   */ import org.bukkit.event.EventHandler;
/* 19:   */ import org.bukkit.event.EventPriority;
/* 20:   */ import org.bukkit.event.Listener;
/* 21:   */ import org.bukkit.event.entity.EntityShootBowEvent;
/* 22:   */ import org.bukkit.event.entity.ProjectileHitEvent;
/* 24:   */ import org.bukkit.inventory.meta.FireworkMeta;
/* 26:   */ 
/* 27:   */ public class EntityBowFireListener
/* 28:   */   implements Listener
/* 29:   */ {
/* 30:21 */   HashMap<String, Entity> witherskeleton = new HashMap<String, Entity>();
/* 31:   */   
/* 32:   */   @EventHandler(priority=EventPriority.HIGH)
/* 33:   */   public void onBowFire(EntityShootBowEvent e)
/* 34:   */   {
/* 35:26 */     if ((e.getEntity() instanceof Player))
/* 36:   */     {
/* 37:28 */       Player p = (Player)e.getEntity();
/* 38:29 */       if ((WorldChecker.game(p)) && (SCBGameManager.getInstance().isInGame(p.getName())))
/* 39:   */       {
/* 40:31 */         CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(p);
/* 41:32 */         if (bro != null)
/* 42:   */         {
/* 43:34 */           if (SCBGameManager.getInstance().getCraftBrother(p).getCurrentClass().equals("Wither"))
/* 44:   */           {
/* 45:36 */             e.setCancelled(true);
/* 46:37 */             if (e.getForce() > 0.6D)
/* 47:   */             {
/* 48:39 */               WitherSkull ws = (WitherSkull)p.launchProjectile(WitherSkull.class);
/* 49:40 */               ws.setVelocity(ws.getLocation().getDirection().normalize().multiply(0.7D));
/* 50:   */             }
/* 51:   */           }
/* 52:43 */           if (SCBGameManager.getInstance().getCraftBrother(p).getCurrentClass().equals("WitherSkeleton")) {
/* 53:45 */             if (e.getBow().getDurability() == 0)
/* 54:   */             {
/* 55:47 */               this.witherskeleton.put(p.getName(), e.getProjectile());
/* 56:48 */               e.getBow().setDurability((short)360);
/* 57:49 */               SCBGameManager.getInstance().getCraftBrother(p).startBowRegen();
/* 58:   */             }
/* 59:   */             else
/* 60:   */             {
/* 61:53 */               e.setCancelled(true);
/* 62:   */             }
/* 63:   */           }
/* 64:   */         }
/* 65:   */       }
/* 66:   */     }
/* 67:   */   }
/* 68:   */   
/* 69:   */   @EventHandler(priority=EventPriority.HIGH)
/* 70:   */   public void onPH(ProjectileHitEvent event)
/* 71:   */   {
/* 72:64 */     Entity entity = event.getEntity();
/* 73:65 */     if ((event.getEntity().getShooter() instanceof Player))
/* 74:   */     {
/* 75:67 */       Player shooter = (Player)event.getEntity().getShooter();
/* 76:68 */       if ((this.witherskeleton.get(shooter.getName()) != null) && (this.witherskeleton.get(shooter.getName()) == event.getEntity()))
/* 77:   */       {
/* 78:70 */         double x = entity.getLocation().getX();
/* 79:71 */         double y = entity.getLocation().getY();
/* 80:72 */         double z = entity.getLocation().getZ();
/* 81:73 */         Firework fw = (Firework)event.getEntity().getWorld().spawn(event.getEntity().getLocation(), Firework.class);
/* 82:74 */         event.getEntity().getWorld().createExplosion(x, y, z, 7.0F, false, false);
/* 83:75 */         FireworkMeta fm = fw.getFireworkMeta();
/* 84:76 */         FireworkEffect effect = FireworkEffect.builder().flicker(true).trail(true).withColor(Color.GREEN).withFade(Color.LIME).with(FireworkEffect.Type.CREEPER).build();
/* 85:77 */         fm.addEffect(effect);
/* 86:78 */         fm.setPower(2);
/* 87:79 */         fw.setFireworkMeta(fm);
/* 88:80 */         this.witherskeleton.remove(shooter.getName());
/* 89:81 */         entity.remove();
/* 90:   */       }
/* 91:   */     }
/* 92:   */   }
/* 93:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.EntityBowFireListener
 * JD-Core Version:    0.7.0.1
 */