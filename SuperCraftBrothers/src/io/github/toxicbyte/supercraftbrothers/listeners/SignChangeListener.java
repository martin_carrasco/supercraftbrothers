/*   1:    */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*   4:    */ import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.SCBMap;

/*   6:    */ import org.bukkit.ChatColor;
/*   8:    */ import org.bukkit.entity.Player;
/*   9:    */ import org.bukkit.event.EventHandler;
/*  10:    */ import org.bukkit.event.EventPriority;
/*  11:    */ import org.bukkit.event.Listener;
/*  12:    */ import org.bukkit.event.block.SignChangeEvent;
/*  13:    */ 
/*  14:    */ public class SignChangeListener
/*  15:    */   implements Listener
/*  16:    */ {
/*  17:    */   @EventHandler(priority=EventPriority.HIGH)
/*  18:    */   public void onSignChange(SignChangeEvent event)
/*  19:    */   {
/*  20: 18 */     Player player = event.getPlayer();
/*  21: 19 */     if (event.getLine(0).equalsIgnoreCase("[SCB]"))
/*  22:    */     {
/*  23: 21 */       player = event.getPlayer();
/*  24: 22 */       if (player.hasPermission("scb.create"))
/*  25:    */       {
/*  26: 23 */         if (event.getLine(2).equalsIgnoreCase(""))
/*  27:    */         {
/*  28: 24 */           String map = event.getLine(1);
/*  29: 25 */           if (SCBGameManager.getInstance().getGame(map) == null) {
/*  30: 26 */             return;
/*  31:    */           }
/*  32: 27 */           SCBMap m = SCBGameManager.getInstance().getGame(map).getMap();
/*  33: 28 */           m.setLobbySign(event.getBlock().getLocation());
/*  34: 29 */           player.sendMessage(ChatColor.GREEN + "Successfully created HUB sign for the map " + m.getGame().getName() + ".");
/*  35:    */         }
/*  36:    */       }
/*  37:    */       else {
/*  38: 32 */         player.sendMessage(ChatColor.RED + "[SCB] You do not have permission to do that");
/*  39:    */       }
/*  40:    */     }
/*  41: 35 */     if (event.getLine(0).equalsIgnoreCase("[LOBBY]"))
/*  42:    */     {
/*  43: 36 */       player = event.getPlayer();
/*  44: 37 */       if (player.hasPermission("scb.lobby"))
/*  45:    */       {
/*  46: 38 */         if (event.getLine(2).equalsIgnoreCase(""))
/*  47:    */         {
/*  48: 39 */           event.setLine(0, "Click to Go");
/*  49: 40 */           event.setLine(1, ChatColor.BOLD + "Back to lobby");
/*  50: 41 */           event.setLine(2, "");
/*  51: 42 */           event.setLine(3, "");
/*  52: 43 */           player.sendMessage(ChatColor.GREEN + "Successfully created LOBBY sign.");
/*  53:    */         }
/*  54:    */       }
/*  55:    */       else {
/*  56: 46 */         player.sendMessage(ChatColor.RED + "[SCB] You do not have permission to do that");
/*  57:    */       }
/*  58:    */     }
/*  59: 49 */     if (event.getLine(0).equalsIgnoreCase("[CLASS]"))
/*  60:    */     {
/*  61: 50 */       player = event.getPlayer();
/*  62: 51 */       if (player.hasPermission("scb.classnormal"))
/*  63:    */       {
/*  64: 52 */         if (event.getLine(2).equalsIgnoreCase(""))
/*  65:    */         {
/*  66: 53 */           event.setLine(0, "");
/*  67: 54 */           event.setLine(1, ChatColor.BLACK + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "CHARACTER");
/*  68: 55 */           event.setLine(2, "");
/*  69: 56 */           event.setLine(3, "");
/*  70: 57 */           player.sendMessage(ChatColor.GREEN + "Successfully created CLASS sign.");
/*  71:    */         }
/*  72:    */       }
/*  73:    */       else {
/*  74: 60 */         player.sendMessage(ChatColor.RED + "[SCB] You do not have permission to do that");
/*  75:    */       }
/*  76:    */     }
/*  77: 63 */     if (event.getLine(0).equalsIgnoreCase("[GEMCLASS]"))
/*  78:    */     {
/*  79: 64 */       player = event.getPlayer();
/*  80: 65 */       if (player.hasPermission("scb.classgem"))
/*  81:    */       {
/*  82: 66 */         if (event.getLine(2).equalsIgnoreCase(""))
/*  83:    */         {
/*  84: 67 */           event.setLine(0, "");
/*  85: 68 */           event.setLine(1, ChatColor.BLACK + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "GEM");
/*  86: 69 */           event.setLine(2, ChatColor.BLACK + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "CHARACTER");
/*  87: 70 */           event.setLine(3, "");
/*  88: 71 */           player.sendMessage(ChatColor.GREEN + "Successfully created GEM CLASS sign.");
/*  89:    */         }
/*  90:    */       }
/*  91:    */       else {
/*  92: 74 */         player.sendMessage(ChatColor.RED + "[SCB] You do not have permission to do that");
/*  93:    */       }
/*  94:    */     }
/*  95: 77 */     if (event.getLine(0).equalsIgnoreCase("[VIPCLASS]"))
/*  96:    */     {
/*  97: 78 */       player = event.getPlayer();
/*  98: 79 */       if (player.hasPermission("scb.classvip"))
/*  99:    */       {
/* 100: 80 */         if (event.getLine(2).equalsIgnoreCase(""))
/* 101:    */         {
/* 102: 81 */           event.setLine(0, "");
/* 103: 82 */           event.setLine(1, ChatColor.BLACK + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "VIP");
/* 104: 83 */           event.setLine(2, ChatColor.BLACK + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "CHARACTER");
/* 105: 84 */           event.setLine(3, "");
/* 106: 85 */           player.sendMessage(ChatColor.GREEN + "Successfully created VIP CLASS sign.");
/* 107:    */         }
/* 108:    */       }
/* 109:    */       else {
/* 110: 88 */         player.sendMessage(ChatColor.RED + "[SCB] You do not have permission to do that");
/* 111:    */       }
/* 112:    */     }
/* 113: 91 */     if (event.getLine(0).equalsIgnoreCase("[SPEC]"))
/* 114:    */     {
/* 115: 92 */       player = event.getPlayer();
/* 116: 93 */       if (player.hasPermission("scb.spectate"))
/* 117:    */       {
/* 118: 94 */         if (event.getLine(2).equalsIgnoreCase(""))
/* 119:    */         {
/* 120: 95 */           event.setLine(0, "");
/* 121: 96 */           event.setLine(1, ChatColor.BLACK + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "SPECTATE");
/* 122: 97 */           event.setLine(2, ChatColor.GREEN + "" + ChatColor.UNDERLINE + "VIP Only");
/* 123: 98 */           event.setLine(3, "");
/* 124: 99 */           player.sendMessage(ChatColor.GREEN + "Successfully created SPECTATION sign.");
/* 125:    */         }
/* 126:    */       }
/* 127:    */       else {
/* 128:102 */         player.sendMessage(ChatColor.RED + "[SCB] You do not have permission to do that");
/* 129:    */       }
/* 130:    */     }
/* 131:    */   }
/* 132:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.SignChangeListener
 * JD-Core Version:    0.7.0.1
 */