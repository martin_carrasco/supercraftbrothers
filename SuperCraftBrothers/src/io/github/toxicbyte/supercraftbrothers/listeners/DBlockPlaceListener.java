/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;

/*  8:   */ import org.bukkit.Bukkit;
/* 10:   */ import org.bukkit.Material;
/* 13:   */ import org.bukkit.entity.Player;
/* 14:   */ import org.bukkit.entity.TNTPrimed;
/* 15:   */ import org.bukkit.event.EventHandler;
/* 16:   */ import org.bukkit.event.EventPriority;
/* 17:   */ import org.bukkit.event.Listener;
/* 18:   */ import org.bukkit.event.block.BlockPlaceEvent;
/* 19:   */ import org.bukkit.inventory.ItemStack;
/* 20:   */ 
/* 21:   */ public class DBlockPlaceListener
/* 22:   */   implements Listener
/* 23:   */ {
/* 24:   */   @EventHandler(priority=EventPriority.HIGH)
/* 25:   */   public void BlockPlace(BlockPlaceEvent event)
/* 26:   */   {
/* 27:30 */     if (SCBGameManager.getInstance().getMainLobby() == null)
/* 28:   */     {
/* 29:32 */       Bukkit.getLogger().severe("[SCB] You have not set a main lobby yet!");
/* 30:33 */       return;
/* 31:   */     }
/* 32:35 */     Player player = event.getPlayer();
/* 33:36 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(player);
/* 34:37 */     String mainlobby = SCBGameManager.getInstance().getMainLobby().getWorld().getName();
/* 35:38 */     if (bro == null)
/* 36:   */     {
/* 37:40 */       if (player.hasPermission("scb.bypass.blockplaceevent")) {
/* 38:41 */         return;
/* 39:   */       }
/* 40:42 */       if (player.getWorld().getName().equalsIgnoreCase(mainlobby)) {
/* 41:43 */         event.setCancelled(true);
/* 42:   */       }
/* 43:   */     }
/* 44:   */     else
/* 45:   */     {
/* 46:47 */       String lobbyworld = bro.getCurrentGame().getMap().getClassLobby().getWorld().getName();
/* 47:48 */       if (player.getWorld().getName().equalsIgnoreCase(lobbyworld))
/* 48:   */       {
/* 49:50 */         if (event.getBlock().getType() == Material.TNT)
/* 50:   */         {
/* 51:52 */           event.setCancelled(true);
/* 52:53 */           event.getBlock().getWorld().spawn(event.getBlock().getLocation().add(0.5D, 0.25D, 0.5D), TNTPrimed.class);
/* 53:54 */           ItemStack i = event.getPlayer().getItemInHand();
/* 54:55 */           i.setAmount(i.getAmount() - 1);
/* 55:56 */           event.getPlayer().setItemInHand(i);
/* 56:   */         }
/* 57:59 */         if (player.hasPermission("scb.bypass.blockplaceevent")) {
/* 58:60 */           return;
/* 59:   */         }
/* 60:62 */         event.setCancelled(true);
/* 61:   */       }
/* 62:   */     }
/* 63:   */   }
/* 64:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.DBlockPlaceListener
 * JD-Core Version:    0.7.0.1
 */