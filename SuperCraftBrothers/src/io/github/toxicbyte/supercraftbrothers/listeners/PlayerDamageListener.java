/*   1:    */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*   2:    */ 
/*   3:    */ /*   6:    */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.utilities.WorldChecker;

/*   7:    */ import java.util.Random;

/*   8:    */ import org.bukkit.entity.Player;
/*   9:    */ import org.bukkit.event.EventHandler;
/*  10:    */ import org.bukkit.event.EventPriority;
/*  11:    */ import org.bukkit.event.Listener;
/*  12:    */ import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
/*  14:    */ import org.bukkit.potion.PotionEffect;
/*  15:    */ import org.bukkit.potion.PotionEffectType;
/*  16:    */ 
/*  17:    */ public class PlayerDamageListener
/*  18:    */   implements Listener
/*  19:    */ {
/*  20: 19 */   Random random = new Random();
/*  21:    */   
/*  22:    */   @EventHandler(priority=EventPriority.HIGH)
/*  23:    */   public void CheckEBDEE(EntityDamageByEntityEvent e)
/*  24:    */   {
/*  25: 23 */     if ((e.getEntity() instanceof Player)) {
/*  26: 25 */       if ((e.getDamager() instanceof Player))
/*  27:    */       {
/*  28: 27 */         Player damaged = (Player)e.getEntity();
/*  29: 28 */         Player damager = (Player)e.getDamager();
/*  30: 29 */         if ((damaged.getLastDamageCause() != null) && (damaged.getLastDamageCause().equals(EntityDamageEvent.DamageCause.FALL)))
/*  31:    */         {
/*  32: 31 */           e.setCancelled(true);
/*  33: 32 */           return;
/*  34:    */         }
/*  35: 34 */         if ((WorldChecker.mainLobbyGame(damaged)) && (WorldChecker.mainLobbyGame(damager)))
/*  36:    */         {
/*  37: 36 */           CraftBrother CBdamaged = SCBGameManager.getInstance().getCraftBrother(damaged);
/*  38: 37 */           CraftBrother CBdamager = SCBGameManager.getInstance().getCraftBrother(damager);
/*  39: 38 */           if ((CBdamaged != null) && (CBdamager != null))
/*  40:    */           {
/*  41: 40 */             if ((CBdamaged.getCurrentGame().isInGame()) && (CBdamager.getCurrentGame().isInGame()))
/*  42:    */             {
/*  43: 42 */               if (CBdamaged.getCurrentGame().equals(CBdamager.getCurrentGame()))
/*  44:    */               {
/*  45: 44 */                 CBdamaged.setLastDamagedBy(CBdamager.getPlayer().getName());
/*  46:    */                 String str1;
/*  47: 45 */                 switch ((str1 = CBdamaged.getCurrentClass()).hashCode())
/*  48:    */                 {
/*  49:    */                 case 80238: 
/*  50: 45 */                   if (str1.equals("Pig")) {
/*  51: 48 */                     Pig(damaged);
/*  52:    */                   }
/*  53:    */                   break;
/*  54:    */                 }
/*  55:    */                 String str2;
/*  56: 51 */                 switch ((str2 = CBdamager.getCurrentClass()).hashCode())
/*  57:    */                 {
/*  58:    */                 case -1938719552: 
/*  59: 51 */                   if (str2.equals("Ocelot")) {
/*  60:    */                     break;
/*  61:    */                   }
/*  62:    */                   break;
/*  63:    */                 case -1812086011: 
/*  64: 51 */                   if (str2.equals("Spider")) {}
/*  65:    */                   break;
/*  66:    */                 case 80134418: 
/*  67: 51 */                   if (!str2.equals("Squid"))
/*  68:    */                   {
/*  69: 54 */                     return;
/*  70:    */                   }
/*  71:    */                   else
/*  72:    */                   {
/*  73: 57 */                     Squid(damaged);
/*  74: 58 */                     return;
/*  75:    */                     
/*  77:    */                   }
/*  79:    */                 }
/*  80:    */               }
/*  81:    */               else
/*  82:    */               {
/*  83: 65 */                 e.setCancelled(true);
/*  84:    */               }
/*  85:    */             }
/*  86:    */             else {
/*  87: 70 */               e.setCancelled(true);
/*  88:    */             }
/*  89:    */           }
/*  90:    */           else {
/*  91: 75 */             e.setCancelled(true);
/*  92:    */           }
/*  93:    */         }
/*  94:    */       }
/*  95:    */     }
/*  96:    */   }
/*  97:    */   
/*  98:    */   public void Pig(Player p)
/*  99:    */   {
/* 100: 83 */     p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 1));
/* 101:    */   }
/* 102:    */   
/* 103:    */   public void Ocelot(Player p)
/* 104:    */   {
/* 105: 87 */     p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 100, 3));
/* 106:    */   }
/* 107:    */   
/* 108:    */   public void Squid(Player p)
/* 109:    */   {
/* 110: 91 */     int i = this.random.nextInt(9);
/* 111: 92 */     if ((i < 9) && (i > 6)) {
/* 112: 93 */       p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 60, 1));
/* 113:    */     }
/* 114: 94 */     i = this.random.nextInt(9);
/* 115: 95 */     if ((i < 8) && (i > 6)) {
/* 116: 96 */       p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 60, 1));
/* 117:    */     }
/* 118:    */   }
/* 119:    */   
/* 120:    */   public void Spider(Player p)
/* 121:    */   {
/* 122:100 */     int i = this.random.nextInt(9);
/* 123:101 */     if ((i < 9) && (i > 5)) {
/* 124:102 */       p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 60, 1));
/* 125:    */     }
/* 126:    */   }
/* 127:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.PlayerDamageListener
 * JD-Core Version:    0.7.0.1
 */