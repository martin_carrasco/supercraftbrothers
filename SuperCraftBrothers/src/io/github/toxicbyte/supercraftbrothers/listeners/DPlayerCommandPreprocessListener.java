/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.utilities.WorldChecker;

/*  6:   */ import org.bukkit.Bukkit;
/*  7:   */ import org.bukkit.entity.Player;
/*  8:   */ import org.bukkit.event.EventHandler;
/*  9:   */ import org.bukkit.event.EventPriority;
/* 10:   */ import org.bukkit.event.Listener;
/* 11:   */ import org.bukkit.event.player.PlayerCommandPreprocessEvent;
/* 12:   */ 
/* 13:   */ public class DPlayerCommandPreprocessListener
/* 14:   */   implements Listener
/* 15:   */ {
/* 16:   */   @EventHandler(priority=EventPriority.HIGH)
/* 17:   */   public void PlayerCommandPreprocessListener(PlayerCommandPreprocessEvent event)
/* 18:   */   {
/* 19:27 */     if (SCBGameManager.getInstance().getMainLobby() == null)
/* 20:   */     {
/* 21:29 */       Bukkit.getLogger().severe("[SCB] You have not set a main lobby yet!");
/* 22:30 */       return;
/* 23:   */     }
/* 24:32 */     Player player = event.getPlayer();
/* 25:33 */     if (WorldChecker.lobbyGame(player))
/* 26:   */     {
/* 27:35 */       String p = "scb.bypass.commandblocker";
/* 28:36 */       String nc = event.getMessage().replace("/", ".").replace(" ", ".");
/* 29:37 */       if ((player.hasPermission(p)) || (player.hasPermission(p + nc))) {
/* 30:38 */         return;
/* 31:   */       }
/* 32:41 */       event.getPlayer().sendMessage("[§aSCB§r] Commands are disabled in-arena");
/* 33:42 */       event.setCancelled(true);
/* 34:   */     }
/* 35:   */   }
/* 36:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.DPlayerCommandPreprocessListener
 * JD-Core Version:    0.7.0.1
 */