/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.utilities.ClassUtils;

/*  7:   */ import org.bukkit.ChatColor;
/*  9:   */ import org.bukkit.event.EventHandler;
/* 10:   */ import org.bukkit.event.EventPriority;
/* 11:   */ import org.bukkit.event.Listener;
/* 12:   */ import org.bukkit.event.player.AsyncPlayerChatEvent;
/* 13:   */ 
/* 14:   */ public class AsyncPlayerChatListener
/* 15:   */   implements Listener
/* 16:   */ {
/* 17:   */   @EventHandler(priority=EventPriority.MONITOR)
/* 18:   */   public void SCBChat(AsyncPlayerChatEvent e)
/* 19:   */   {
/* 20:18 */     CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 21:19 */     if (bro != null)
/* 22:   */     {
/* 23:21 */       String cBClass = bro.getCurrentClass();
/* 24:22 */       String message = e.getMessage();
/* 25:23 */       String newMessage = "[" + ClassUtils.getName(cBClass) + ChatColor.RESET + "]" + "<" + e.getPlayer().getDisplayName() + ">" + message;
/* 26:24 */       e.setCancelled(true);
/* 27:25 */       bro.getCurrentGame().broadcast(newMessage);
/* 28:   */     }
/* 29:   */   }
/* 30:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.AsyncPlayerChatListener
 * JD-Core Version:    0.7.0.1
 */