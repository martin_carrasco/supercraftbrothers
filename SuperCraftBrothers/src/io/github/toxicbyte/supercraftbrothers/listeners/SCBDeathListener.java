/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.SCBGame;
import io.github.toxicbyte.supercraftbrothers.SuperCraftBrothers;
import io.github.toxicbyte.supercraftbrothers.events.SCBDeathEvent;
import net.minecraft.server.v1_7_R4.PacketPlayInClientCommand;

import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
/* 12:   */ import org.bukkit.event.EventHandler;
/* 13:   */ import org.bukkit.event.EventPriority;
/* 14:   */ import org.bukkit.event.Listener;
/* 17:   */ 
/* 18:   */ public class SCBDeathListener
/* 19:   */   implements Listener
/* 20:   */ {
/* 21:   */   private SuperCraftBrothers scb;
/* 22:   */   
/* 23:   */   public SCBDeathListener(SuperCraftBrothers scb)
/* 24:   */   {
/* 25:20 */     this.scb = scb;
/* 26:   */   }
/* 27:   */   
/* 28:   */   @EventHandler(priority=EventPriority.HIGH)
/* 29:   */   public void onSCBDeath(final SCBDeathEvent event)
/* 30:   */   {
/* 31:26 */     SCBGame game = event.getGame();
/* 32:27 */     CraftBrother killed = event.getKilled();
/* 33:28 */     CraftBrother killer = event.getKiller();
/* 34:29 */     String deathmessage = event.getDeathMessage();
/* 35:30 */     killed.setLivesLeft(killed.getLivesLeft() - 1);
/* 36:31 */     killed.setRespawning(true);
/* 37:32 */     if ((killer != null) && (killed != null)) {
/* 38:34 */       killer.setKills(killer.getKills() + 1);
/* 39:   */     }
/* 40:36 */     if (game != null) {
/* 41:38 */       game.broadcast(deathmessage);
/* 42:   */     }
/* 43:40 */     if (killed.getLivesLeft() <= 0) {
/* 44:42 */       game.playerEliminated(killed);
/* 45:   */     }
/* 46:44 */     this.scb.getServer().getScheduler().runTaskLater(this.scb, new Runnable()
			     {
			       public void run()
			       {
			         PacketPlayInClientCommand packet = new PacketPlayInClientCommand();
			         CraftPlayer cp = (CraftPlayer)event.getKilled().getPlayer();
			         if (cp != null) {
			           cp.getHandle().playerConnection.a(packet);
			         }
			       }
			     }, 10L);
			   }
			 }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.SCBDeathListener
 * JD-Core Version:    0.7.0.1
 */