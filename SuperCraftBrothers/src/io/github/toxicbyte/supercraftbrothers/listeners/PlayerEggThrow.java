/*  1:   */ package io.github.toxicbyte.supercraftbrothers.listeners;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.utilities.WorldChecker;

/*  8:   */ import org.bukkit.entity.Egg;
/*  9:   */ import org.bukkit.event.EventHandler;
/* 10:   */ import org.bukkit.event.EventPriority;
/* 11:   */ import org.bukkit.event.Listener;
/* 12:   */ import org.bukkit.event.player.PlayerEggThrowEvent;
/* 13:   */ 
/* 14:   */ public class PlayerEggThrow
/* 15:   */   implements Listener
/* 16:   */ {
/* 17:   */   @EventHandler(priority=EventPriority.HIGH)
/* 18:   */   public void ChickenEgg(PlayerEggThrowEvent e)
/* 19:   */   {
/* 20:18 */     if (WorldChecker.game(e.getPlayer()))
/* 21:   */     {
/* 22:20 */       CraftBrother bro = SCBGameManager.getInstance().getCraftBrother(e.getPlayer());
/* 23:21 */       if ((bro != null) && (bro.getCurrentGame() != null) && (bro.getCurrentClass() != null) && (bro.getCurrentClass().equals("Chicken")))
/* 24:   */       {
/* 25:23 */         e.setHatching(false);
/* 26:24 */         Egg egg = e.getEgg();
/* 27:25 */         double x = egg.getLocation().getX();
/* 28:26 */         double y = egg.getLocation().getY();
/* 29:27 */         double z = egg.getLocation().getZ();
/* 30:28 */         egg.getWorld().createExplosion(x, y, z, 1.0F, false, false);
/* 31:   */       }
/* 32:   */     }
/* 33:   */   }
/* 34:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.listeners.PlayerEggThrow
 * JD-Core Version:    0.7.0.1
 */