/*   1:    */ package io.github.toxicbyte.supercraftbrothers;
/*   2:    */ 
/*   3:    */ import io.github.toxicbyte.supercraftbrothers.utilities.LocationUtils;

/*   4:    */ import org.bukkit.Location;
/*   5:    */ import org.bukkit.configuration.Configuration;
/*   6:    */ 
/*   7:    */ public class SCBMap
/*   8:    */ {
/*   9:    */   private Location sp1;
/*  10:    */   private Location sp2;
/*  11:    */   private Location sp3;
/*  12:    */   private Location sp4;
/*  13:    */   private Location spec;
/*  14:    */   private Location lobbySign;
/*  15:    */   private Location classLobby;
/*  16:    */   private SCBGame game;
/*  17:    */   private final SuperCraftBrothers plugin;
/*  18:    */   
/*  19:    */   public SCBMap(SCBGame game, SuperCraftBrothers plugin)
/*  20:    */   {
/*  21: 18 */     this.game = game;
/*  22: 19 */     this.plugin = plugin;
/*  23:    */   }
/*  24:    */   
/*  25:    */   public void load()
/*  26:    */   {
/*  27: 23 */     Configuration config = this.plugin.getConfig();
/*  28: 24 */     if (config.isString("map." + this.game.getName() + ".sp1")) {
/*  29: 25 */       this.sp1 = LocationUtils.stringToLocation(config.getString("map." + this.game.getName() + ".sp1"));
/*  30:    */     } else {
/*  31: 27 */       this.sp1 = null;
/*  32:    */     }
/*  33: 29 */     if (config.isString("map." + this.game.getName() + ".sp2")) {
/*  34: 30 */       this.sp2 = LocationUtils.stringToLocation(config.getString("map." + this.game.getName() + ".sp2"));
/*  35:    */     } else {
/*  36: 32 */       this.sp2 = null;
/*  37:    */     }
/*  38: 34 */     if (config.isString("map." + this.game.getName() + ".sp3")) {
/*  39: 35 */       this.sp3 = LocationUtils.stringToLocation(config.getString("map." + this.game.getName() + ".sp3"));
/*  40:    */     } else {
/*  41: 37 */       this.sp3 = null;
/*  42:    */     }
/*  43: 39 */     if (config.isString("map." + this.game.getName() + ".sp4")) {
/*  44: 40 */       this.sp4 = LocationUtils.stringToLocation(config.getString("map." + this.game.getName() + ".sp4"));
/*  45:    */     } else {
/*  46: 42 */       this.sp4 = null;
/*  47:    */     }
/*  48: 44 */     if (config.isString("map." + this.game.getName() + ".spec")) {
/*  49: 45 */       this.spec = LocationUtils.stringToLocation(config.getString("map." + this.game.getName() + ".spec"));
/*  50:    */     } else {
/*  51: 47 */       this.spec = null;
/*  52:    */     }
/*  53: 49 */     if (config.isString("map." + this.game.getName() + ".lobby-sign")) {
/*  54: 50 */       this.lobbySign = LocationUtils.stringToLocation(config.getString("map." + this.game.getName() + ".lobby-sign"));
/*  55:    */     } else {
/*  56: 52 */       this.lobbySign = null;
/*  57:    */     }
/*  58: 54 */     if (config.isString("map." + this.game.getName() + ".class-lobby")) {
/*  59: 55 */       this.classLobby = LocationUtils.stringToLocation(config.getString("map." + this.game.getName() + ".class-lobby"));
/*  60:    */     } else {
/*  61: 57 */       this.classLobby = null;
/*  62:    */     }
/*  63:    */   }
/*  64:    */   
/*  65:    */   public void save()
/*  66:    */   {
/*  67: 62 */     Configuration config = this.plugin.getConfig();
/*  68: 63 */     if (this.sp1 != null) {
/*  69: 64 */       config.set("map." + this.game.getName() + ".sp1", LocationUtils.locationToString(this.sp1));
/*  70:    */     } else {
/*  71: 66 */       config.set("map." + this.game.getName() + ".sp1", null);
/*  72:    */     }
/*  73: 68 */     if (this.sp2 != null) {
/*  74: 69 */       config.set("map." + this.game.getName() + ".sp2", LocationUtils.locationToString(this.sp2));
/*  75:    */     } else {
/*  76: 71 */       config.set("map." + this.game.getName() + ".sp2", null);
/*  77:    */     }
/*  78: 73 */     if (this.sp3 != null) {
/*  79: 74 */       config.set("map." + this.game.getName() + ".sp3", LocationUtils.locationToString(this.sp3));
/*  80:    */     } else {
/*  81: 76 */       config.set("map." + this.game.getName() + ".sp3", null);
/*  82:    */     }
/*  83: 78 */     if (this.sp4 != null) {
/*  84: 79 */       config.set("map." + this.game.getName() + ".sp4", LocationUtils.locationToString(this.sp4));
/*  85:    */     } else {
/*  86: 81 */       config.set("map." + this.game.getName() + ".sp4", null);
/*  87:    */     }
/*  88: 83 */     if (this.spec != null) {
/*  89: 84 */       config.set("map." + this.game.getName() + ".spec", LocationUtils.locationToString(this.spec));
/*  90:    */     } else {
/*  91: 86 */       config.set("map." + this.game.getName() + ".spec", null);
/*  92:    */     }
/*  93: 88 */     if (this.lobbySign != null) {
/*  94: 89 */       config.set("map." + this.game.getName() + ".lobby-sign", LocationUtils.locationToString(this.lobbySign));
/*  95:    */     } else {
/*  96: 91 */       config.set("map." + this.game.getName() + ".lobby-sign", null);
/*  97:    */     }
/*  98: 93 */     if (this.classLobby != null) {
/*  99: 94 */       config.set("map." + this.game.getName() + ".class-lobby", LocationUtils.locationToString(this.classLobby));
/* 100:    */     } else {
/* 101: 96 */       config.set("map." + this.game.getName() + ".class-lobby", null);
/* 102:    */     }
/* 103: 98 */     this.plugin.saveConfig();
/* 104:    */   }
/* 105:    */   
/* 106:    */   public SCBGame getGame()
/* 107:    */   {
/* 108:102 */     return this.game;
/* 109:    */   }
/* 110:    */   
/* 111:    */   public Location getSp1()
/* 112:    */   {
/* 113:106 */     return this.sp1;
/* 114:    */   }
/* 115:    */   
/* 116:    */   public void setSp1(Location sp1)
/* 117:    */   {
/* 118:110 */     this.sp1 = sp1;
/* 119:111 */     save();
/* 120:    */   }
/* 121:    */   
/* 122:    */   public Location getSp2()
/* 123:    */   {
/* 124:115 */     return this.sp2;
/* 125:    */   }
/* 126:    */   
/* 127:    */   public void setSp2(Location sp2)
/* 128:    */   {
/* 129:119 */     this.sp2 = sp2;
/* 130:120 */     save();
/* 131:    */   }
/* 132:    */   
/* 133:    */   public Location getSp3()
/* 134:    */   {
/* 135:124 */     return this.sp3;
/* 136:    */   }
/* 137:    */   
/* 138:    */   public void setSp3(Location sp3)
/* 139:    */   {
/* 140:128 */     this.sp3 = sp3;
/* 141:129 */     save();
/* 142:    */   }
/* 143:    */   
/* 144:    */   public Location getSp4()
/* 145:    */   {
/* 146:133 */     return this.sp4;
/* 147:    */   }
/* 148:    */   
/* 149:    */   public void setSp4(Location sp4)
/* 150:    */   {
/* 151:137 */     this.sp4 = sp4;
/* 152:138 */     save();
/* 153:    */   }
/* 154:    */   
/* 155:    */   public Location getSpec()
/* 156:    */   {
/* 157:142 */     return this.spec;
/* 158:    */   }
/* 159:    */   
/* 160:    */   public void setSpec(Location spec)
/* 161:    */   {
/* 162:146 */     this.spec = spec;
/* 163:147 */     save();
/* 164:    */   }
/* 165:    */   
/* 166:    */   public Location getLobbySign()
/* 167:    */   {
/* 168:151 */     return this.lobbySign;
/* 169:    */   }
/* 170:    */   
/* 171:    */   public void setLobbySign(Location lobbySign)
/* 172:    */   {
/* 173:155 */     this.lobbySign = lobbySign;
/* 174:156 */     save();
/* 175:    */   }
/* 176:    */   
/* 177:    */   public Location getClassLobby()
/* 178:    */   {
/* 179:160 */     return this.classLobby;
/* 180:    */   }
/* 181:    */   
/* 182:    */   public void setClassLobby(Location classLobby)
/* 183:    */   {
/* 184:164 */     this.classLobby = classLobby;
/* 185:165 */     save();
/* 186:    */   }
/* 187:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.SCBMap
 * JD-Core Version:    0.7.0.1
 */