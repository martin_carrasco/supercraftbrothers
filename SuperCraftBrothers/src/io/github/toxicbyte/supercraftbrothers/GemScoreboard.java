/*  1:   */ package io.github.toxicbyte.supercraftbrothers;
/*  2:   */ 
/*  3:   */ import org.bukkit.Bukkit;
/*  4:   */ import org.bukkit.entity.Player;
/*  5:   */ import org.bukkit.scoreboard.DisplaySlot;
/*  6:   */ import org.bukkit.scoreboard.Objective;
/*  7:   */ import org.bukkit.scoreboard.Score;
/*  8:   */ import org.bukkit.scoreboard.Scoreboard;
/* 10:   */ 
/* 11:   */ public class GemScoreboard
/* 12:   */ {
/* 13:   */   @SuppressWarnings("deprecation")
public static void addPlayer(Player player)
/* 14:   */   {
/* 15:11 */     String sp = player.getName();
/* 16:12 */     Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
/* 17:13 */     int rank = PlayerStats.getPlayerRank(player.getName());
/* 18:14 */     String srank = null;
/* 19:15 */     switch (rank)
/* 20:   */     {
/* 21:   */     case 1: 
/* 22:18 */       srank = "DEFAULT";
/* 23:19 */       break;
/* 24:   */     case 2: 
/* 25:21 */       srank = "VIP";
/* 26:22 */       break;
/* 27:   */     case 3: 
/* 28:24 */       srank = "PRO";
/* 29:   */     }
/* 30:27 */     int gems = PlayerStats.getPlayerGems(player.getName());
/* 31:28 */     Objective objective = board.registerNewObjective("gems", "dummy");
/* 32:29 */     objective.setDisplaySlot(DisplaySlot.SIDEBAR);
/* 33:30 */     objective.setDisplayName(sp + " (" + srank + ") ");
/* 34:31 */     Score score = objective.getScore(Bukkit.getOfflinePlayer("Gems:"));
/* 35:32 */     score.setScore(gems);
/* 36:33 */     if ((player != null) && (player.getName() != null)) {
/* 37:34 */       player.setScoreboard(board);
/* 38:   */     }
/* 39:   */   }
/* 40:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.GemScoreboard
 * JD-Core Version:    0.7.0.1
 */