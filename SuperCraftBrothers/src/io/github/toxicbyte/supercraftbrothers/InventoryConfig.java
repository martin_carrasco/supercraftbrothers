/*   1:    */ package io.github.toxicbyte.supercraftbrothers;
/*   2:    */ 
/*   3:    */ /*   4:    */ import io.github.toxicbyte.supercraftbrothers.utilities.ClassUtils;

import java.util.ArrayList;
/*   5:    */ import java.util.List;

/*   6:    */ import org.bukkit.ChatColor;
/*   7:    */ import org.bukkit.Material;
/*   8:    */ import org.bukkit.configuration.file.FileConfiguration;
/*   9:    */ import org.bukkit.entity.Player;
/*  10:    */ import org.bukkit.inventory.ItemStack;
/*  12:    */ import org.bukkit.inventory.meta.BookMeta;
/*  13:    */ import org.bukkit.inventory.meta.ItemMeta;
/*  11:    */ 
/*  14:    */ 
/*  15:    */ public class InventoryConfig
/*  16:    */ {
/*  17: 18 */   public static FileConfiguration c = null;
/*  18:    */   private static SuperCraftBrothers scb;
/*  19:    */   
/*  20:    */   public static void setup(SuperCraftBrothers scb)
/*  21:    */   {
/*  22: 24 */     InventoryConfig.scb = scb;
/*  23:    */   }
/*  24:    */   
/*  25:    */   @SuppressWarnings("deprecation")
public static void giveItems(Player p)
/*  26:    */   {
/*  27: 30 */     ClassUtils.clearInventory(p);
/*  28: 31 */     giveInfoBook(p);
/*  29: 32 */     giveStatsBook(p);
/*  30: 33 */     giveGemShop(p);
/*  31:    */     
/*  32: 35 */     giveHatShop(p);
/*  33: 36 */     p.updateInventory();
/*  34:    */   }
/*  35:    */   
/*  36:    */   public static void giveGemShop(Player p)
/*  37:    */   {
/*  38: 41 */     int g = PlayerStats.getPlayerGems(p.getName());
/*  39: 42 */     ArrayList<String> d = new ArrayList<String>();
/*  40: 43 */     d.add(ChatColor.GRAY + "Total: " + ChatColor.GOLD + g);
/*  41:    */     
/*  42: 45 */     ItemStack gs = new ItemStack(Material.EMERALD, 1);
/*  43: 46 */     ItemMeta im = gs.getItemMeta();
/*  44: 47 */     im.setDisplayName(ChatColor.GREEN + "Character Gem Shop " + ChatColor.GRAY + "(Click to Open)");
/*  45: 48 */     im.setLore(d);
/*  46: 49 */     gs.setItemMeta(im);
/*  47:    */     
/*  48: 51 */     p.getInventory().addItem(new ItemStack[] { gs });
/*  49:    */   }
/*  50:    */   
/*  51:    */   public static void giveHatShop(Player p)
/*  52:    */   {
/*  53: 56 */     int g = PlayerStats.getPlayerGems(p.getName());
/*  54: 57 */     ArrayList<String> d = new ArrayList<String>();
/*  55: 58 */     d.add(ChatColor.GRAY + "Total: " + ChatColor.GOLD + g);
/*  56:    */     
/*  57: 60 */     ItemStack gs = new ItemStack(Material.EMERALD, 1);
/*  58: 61 */     ItemMeta im = gs.getItemMeta();
/*  59: 62 */     im.setDisplayName(ChatColor.GREEN + "Hat Shop " + ChatColor.GRAY + "(Click to Open)");
/*  60: 63 */     im.setLore(d);
/*  61: 64 */     gs.setItemMeta(im);
/*  62:    */     
/*  63: 66 */     p.getInventory().addItem(new ItemStack[] { gs });
/*  64:    */   }
/*  65:    */   
/*  66:    */   public static void giveMapShop(Player p)
/*  67:    */   {
/*  68: 71 */     int g = PlayerStats.getPlayerGems(p.getName());
/*  69: 72 */     ArrayList<String> d = new ArrayList<String>();
/*  70: 73 */     d.add(ChatColor.GRAY + "Total: " + ChatColor.GOLD + g);
/*  71:    */     
/*  72: 75 */     ItemStack gs = new ItemStack(Material.EMERALD, 1);
/*  73: 76 */     ItemMeta im = gs.getItemMeta();
/*  74: 77 */     im.setDisplayName(ChatColor.GREEN + "Map Shop " + ChatColor.GRAY + "(Click to Open)");
/*  75: 78 */     im.setLore(d);
/*  76: 79 */     gs.setItemMeta(im);
/*  77:    */     
/*  78: 81 */     p.getInventory().addItem(new ItemStack[] { gs });
/*  79:    */   }
/*  80:    */   
/*  81:    */   public static void giveStatsBook(Player player)
/*  82:    */   {
/*  83: 86 */     int w = PlayerStats.getPlayerWins(player.getName());
/*  84: 87 */     int k = PlayerStats.getPlayerKills(player.getName());
/*  85: 88 */     int d = PlayerStats.getPlayerDeaths(player.getName());
/*  86: 89 */     int g = PlayerStats.getPlayerGems(player.getName());
/*  87: 90 */     int kdr = 0;
/*  88: 91 */     if (d != 0) {
/*  89: 92 */       kdr = k / d;
/*  90:    */     } else {
/*  91: 94 */       kdr = k;
/*  92:    */     }
/*  93: 95 */     List<String> pages = new ArrayList<String>();
/*  94: 96 */     pages.add(
/*  95: 97 */       ChatColor.BLACK + "" +  ChatColor.BOLD + ChatColor.UNDERLINE + player.getName() + 
/*  96: 98 */       "\n\n" + ChatColor.RESET + 
/*  97: 99 */       ChatColor.BLACK + ChatColor.BOLD + "  Wins: " + ChatColor.RESET + w + 
/*  98:100 */       "\n" + ChatColor.RESET + 
/*  99:101 */       ChatColor.BLACK + ChatColor.BOLD + "  Kills: " + ChatColor.RESET + k + 
/* 100:102 */       "\n" + ChatColor.RESET + 
/* 101:103 */       ChatColor.BLACK + ChatColor.BOLD + "  Deaths: " + ChatColor.RESET + d + 
/* 102:104 */       "\n" + ChatColor.RESET + 
/* 103:105 */       ChatColor.BLACK + ChatColor.BOLD + "  KDR: " + ChatColor.RESET + kdr + 
/* 104:106 */       "\n" + ChatColor.RESET + 
/* 105:107 */       ChatColor.BLACK + ChatColor.BOLD + "  Gems: " + ChatColor.RESET + g + 
/* 106:108 */       "\n" + ChatColor.RESET);
/* 107:    */     
/* 108:    */ 
/* 109:111 */     ItemStack b = new ItemStack(Material.WRITTEN_BOOK);
/* 110:112 */     BookMeta bm = (BookMeta)b.getItemMeta();
/* 111:113 */     bm.setTitle(ChatColor.AQUA + "Player Stats");
/* 112:114 */     bm.setAuthor("zombiekiller753");
/* 113:115 */     bm.setPages(pages);
/* 114:116 */     b.setItemMeta(bm);
/* 115:117 */     player.getInventory().addItem(new ItemStack[] { b });
/* 116:    */   }
/* 117:    */   
/* 118:    */   public static void giveInfoBook(Player p)
/* 119:    */   {
/* 120:121 */     List<String> pages = scb.getConfig().getStringList("book");
/* 121:122 */     for (int i = 0; i < pages.size(); i++)
/* 122:    */     {
/* 123:124 */       String temp = (String)pages.get(i);
/* 124:125 */       temp = ChatColor.translateAlternateColorCodes('&', temp);
/* 125:126 */       pages.set(i, temp);
/* 126:    */     }
/* 127:128 */     ItemStack b = new ItemStack(Material.WRITTEN_BOOK);
/* 128:129 */     BookMeta bm = (BookMeta)b.getItemMeta();
/* 129:130 */     bm.setTitle("SuperCraftBrothers");
/* 130:131 */     bm.setAuthor("zombiekiller753");
/* 131:132 */     bm.setPages(pages);
/* 132:133 */     b.setItemMeta(bm);
/* 133:134 */     p.getInventory().addItem(new ItemStack[] { b });
/* 134:    */   }
/* 135:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.InventoryConfig
 * JD-Core Version:    0.7.0.1
 */