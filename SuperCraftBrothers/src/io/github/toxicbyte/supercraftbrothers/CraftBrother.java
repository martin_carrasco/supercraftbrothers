/*   1:    */ package io.github.toxicbyte.supercraftbrothers;
/*   2:    */ 
/*   3:    */ import io.github.toxicbyte.supercraftbrothers.utilities.ClassUtils;

/*   4:    */ import java.lang.reflect.Method;
/*   5:    */ import java.util.Random;

/*   6:    */ import org.bukkit.Bukkit;
/*   7:    */ import org.bukkit.ChatColor;
/*   8:    */ import org.bukkit.Material;
/*   9:    */ import org.bukkit.enchantments.Enchantment;
/*  10:    */ import org.bukkit.entity.Player;
/*  11:    */ import org.bukkit.inventory.ItemStack;
/*  13:    */ import org.bukkit.potion.PotionEffect;
/*  14:    */ import org.bukkit.scheduler.BukkitRunnable;
/*  15:    */ 
/*  16:    */ public class CraftBrother
/*  17:    */ {
/*  18:    */   private int lives;
/*  19:    */   private int kills;
/*  20:    */   private int deaths;
/*  21:    */   private int bowStage;
/*  22:    */   private int livesAtRegen;
/*  23:    */   private boolean isInLobby;
/*  24:    */   private boolean respawning;
/*  25:    */   
/*  26:    */   public static enum respawnreason
/*  27:    */   {
/*  28: 22 */     TOLOBBY("tolobby"),  TOGAME("togame"),  TOMAIN("tomain");
/*  29:    */     
/*  30:    */     private String reason;
/*  31:    */     
/*  32:    */     private respawnreason(String r)
/*  33:    */     {
/*  34: 28 */       this.reason = r;
/*  35:    */     }
/*  36:    */     
/*  37:    */     public String toString()
/*  38:    */     {
/*  39: 32 */       return this.reason;
/*  40:    */     }
/*  41:    */   }
/*  42:    */   
/*  43: 42 */   private boolean spectating = false;
/*  44:    */   private String lastDamagedBy;
/*  45:    */   private String name;
/*  46:    */   private String currentClass;
/*  47:    */   private SCBGame currentGame;
/*  48:    */   private SuperCraftBrothers plugin;
/*  49:    */   private respawnreason reason;
/*  50:    */   
/*  51:    */   public CraftBrother(String name, SuperCraftBrothers plugin)
/*  52:    */   {
/*  53: 52 */     this.name = name;
/*  54: 53 */     this.plugin = plugin;
/*  55: 54 */     this.lives = 5;
/*  56: 55 */     this.kills = 0;
/*  57:    */   }
/*  58:    */   
/*  59:    */   @SuppressWarnings("deprecation")
public Player getPlayer()
/*  60:    */   {
/*  61: 58 */     return Bukkit.getPlayer(this.name);
/*  62:    */   }
/*  63:    */   
/*  64:    */   public int getLivesLeft()
/*  65:    */   {
/*  66: 59 */     return this.lives;
/*  67:    */   }
/*  68:    */   
/*  69:    */   public int getKills()
/*  70:    */   {
/*  71: 60 */     return this.kills;
/*  72:    */   }
/*  73:    */   
/*  74:    */   public int getDeaths()
/*  75:    */   {
/*  76: 61 */     return this.deaths;
/*  77:    */   }
/*  78:    */   
/*  79:    */   public int getRank()
/*  80:    */   {
/*  81: 62 */     return PlayerStats.getPlayerRank(this.name);
/*  82:    */   }
/*  83:    */   
/*  84:    */   public boolean isInLobby()
/*  85:    */   {
/*  86: 63 */     return this.isInLobby;
/*  87:    */   }
/*  88:    */   
/*  89:    */   public boolean isInGame()
/*  90:    */   {
/*  91: 64 */     return !this.isInLobby;
/*  92:    */   }
/*  93:    */   
/*  94:    */   public SCBGame getCurrentGame()
/*  95:    */   {
/*  96: 65 */     return this.currentGame;
/*  97:    */   }
/*  98:    */   
/*  99:    */   public String getCurrentClass()
/* 100:    */   {
/* 101: 66 */     return this.currentClass;
/* 102:    */   }
/* 103:    */   
/* 104:    */   public String getLastDamagedBy()
/* 105:    */   {
/* 106: 67 */     return this.lastDamagedBy;
/* 107:    */   }
/* 108:    */   
/* 109:    */   public boolean isRespawning()
/* 110:    */   {
/* 111: 68 */     return this.respawning;
/* 112:    */   }
/* 113:    */   
/* 114:    */   public respawnreason getReason()
/* 115:    */   {
/* 116: 69 */     return this.reason;
/* 117:    */   }
/* 118:    */   
/* 119:    */   public boolean isSpectating()
/* 120:    */   {
/* 121: 70 */     return this.spectating;
/* 122:    */   }
/* 123:    */   
/* 124:    */   public void setLivesLeft(int i)
/* 125:    */   {
/* 126: 72 */     this.lives = i;
/* 127:    */   }
/* 128:    */   
/* 129:    */   public void setKills(int i)
/* 130:    */   {
/* 131: 73 */     this.kills = i;
/* 132:    */   }
/* 133:    */   
/* 134:    */   public void setDeaths(int i)
/* 135:    */   {
/* 136: 74 */     this.deaths = i;
/* 137:    */   }
/* 138:    */   
/* 139:    */   public void setInLobby(boolean b)
/* 140:    */   {
/* 141: 75 */     this.isInLobby = b;
/* 142:    */   }
/* 143:    */   
/* 144:    */   public void setCurrentGame(SCBGame g)
/* 145:    */   {
/* 146: 76 */     this.currentGame = g;
/* 147:    */   }
/* 148:    */   
/* 149:    */   public void setLastDamagedBy(String lastDamagedBy)
/* 150:    */   {
/* 151: 77 */     this.lastDamagedBy = lastDamagedBy;
/* 152:    */   }
/* 153:    */   
/* 154:    */   public void setRespawning(boolean b)
/* 155:    */   {
/* 156: 78 */     this.respawning = b;
/* 157:    */   }
/* 158:    */   
/* 159:    */   public void setSpectating(boolean b)
/* 160:    */   {
/* 161: 79 */     this.spectating = b;
/* 162:    */   }
/* 163:    */   
/* 164:    */   public void setReason(respawnreason r)
/* 165:    */   {
/* 166: 80 */     this.reason = r;
/* 167:    */   }
/* 168:    */   
/* 169:    */   public void setCurrentClass(String c)
/* 170:    */   {
/* 171: 83 */     this.currentClass = c;
/* 172: 84 */     getPlayer().sendMessage(ChatColor.GREEN + "You have chosen the " + ClassUtils.getName(c));
/* 173:    */   }
/* 174:    */   
/* 175:    */   @SuppressWarnings("deprecation")
public void apply()
/* 176:    */   {
/* 177: 90 */     if (this.currentClass == null)
/* 178:    */     {
/* 179: 92 */       Random random = new Random();
/* 180: 93 */       int i = random.nextInt(15);
/* 181: 94 */       setCurrentClass(io.github.toxicbyte.supercraftbrothers.menus.ClassMenu.classes[i]);
/* 182:    */     }
/* 183:    */     try
/* 184:    */     {
/* 185: 98 */       ClassUtils.clearInventory(getPlayer());
/* 186: 99 */       String className = "io.github.toxicbyte.supercraftbrothers.classes." + this.currentClass.toLowerCase();
/* 187:100 */       Class<?> c = Class.forName(className);
/* 188:101 */       Method[] m = c.getDeclaredMethods();
/* 189:102 */       Object carObj = null;
/* 190:103 */       carObj = c.newInstance();
/* 191:104 */       Method callM = m[0];
/* 192:105 */       callM.invoke(carObj, new Object[] { this });
/* 193:106 */       getPlayer().updateInventory();
/* 194:    */     }
/* 195:    */     catch (Exception e)
/* 196:    */     {
/* 197:110 */       e.printStackTrace();
/* 198:111 */       getCurrentGame().leaveGame(getPlayer(), false);
/* 199:    */     }
/* 200:    */   }
/* 201:    */   
/* 202:    */   public void startBowRegen()
/* 203:    */   {
/* 204:116 */     this.bowStage = 1;
/* 205:117 */     this.livesAtRegen = this.lives;
/* 206:118 */     BukkitRunnable br = new BukkitRunnable()
/* 207:    */     {
/* 208:    */       @SuppressWarnings("deprecation")
public void run()
/* 209:    */       {
/* 210:124 */         if (CraftBrother.this.livesAtRegen != CraftBrother.this.lives)
/* 211:    */         {
/* 212:125 */           cancel();
/* 213:    */         }
/* 214:    */         else
/* 215:    */         {
/* 216:128 */           ItemStack bow = new ItemStack(Material.BOW, 1);
/* 217:129 */           bow.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
/* 218:130 */           if (CraftBrother.this.bowStage < 11)
/* 219:    */           {
/* 220:132 */             if (CraftBrother.this.getPlayer() == null)
/* 221:    */             {
/* 222:134 */               cancel();
/* 223:    */             }
/* 224:136 */             else if ((CraftBrother.this.getPlayer().getInventory().getItem(2) != null) && (CraftBrother.this.getPlayer().getInventory().getItem(2).getType().equals(Material.BOW)))
/* 225:    */             {
/* 226:139 */               int d = 360 - CraftBrother.this.bowStage * 36;
/* 227:140 */               bow.setDurability((short)d);
/* 228:141 */               CraftBrother.this.getPlayer().getInventory().setItem(2, bow);
/* 229:142 */               CraftBrother.this.getPlayer().updateInventory();
/* 230:143 */               CraftBrother.this.bowStage += 1;
/* 231:    */             }
/* 232:    */           }
/* 233:    */           else
/* 234:    */           {
/* 235:148 */             cancel();
/* 236:149 */             CraftBrother.this.bowStage = 1;
/* 237:    */           }
/* 238:    */         }
/* 239:    */       }
/* 240:153 */     };
/* 241:154 */     br.runTaskTimer(this.plugin, 0L, 20L);
/* 242:    */   }
/* 243:    */   
/* 244:    */   public void heal()
/* 245:    */   {
/* 246:158 */     Player player = getPlayer();
/* 247:159 */     player.setFoodLevel(20);
/* 248:160 */     player.setExp(0.0F);
/* 249:161 */     player.setExhaustion(0.0F);
/* 250:162 */     player.setHealth(20.0D);
/* 251:163 */     player.setFoodLevel(20);
/* 252:164 */     player.setFireTicks(0);
/* 253:165 */     for (PotionEffect effect : player.getActivePotionEffects()) {
/* 254:167 */       player.removePotionEffect(effect.getType());
/* 255:    */     }
/* 256:    */   }
/* 257:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.CraftBrother
 * JD-Core Version:    0.7.0.1
 */