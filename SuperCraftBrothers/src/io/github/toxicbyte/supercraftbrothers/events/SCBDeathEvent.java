/*  1:   */ package io.github.toxicbyte.supercraftbrothers.events;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.SCBGame;

/*  5:   */ import org.bukkit.event.Event;
/*  6:   */ import org.bukkit.event.HandlerList;
/*  7:   */ 
/*  8:   */ public class SCBDeathEvent
/*  9:   */   extends Event
/* 10:   */ {
/* 11:11 */   private static HandlerList handlers = new HandlerList();
/* 12:   */   private CraftBrother killed;
/* 13:   */   private CraftBrother killer;
/* 14:   */   private SCBGame game;
/* 15:   */   private String deathmessage;
/* 16:   */   
/* 17:   */   public SCBDeathEvent(CraftBrother killed, CraftBrother killer, SCBGame game, String deathmessage)
/* 18:   */   {
/* 19:19 */     this.killed = killed;
/* 20:20 */     this.killer = killer;
/* 21:21 */     this.game = game;
/* 22:22 */     this.deathmessage = deathmessage;
/* 23:   */   }
/* 24:   */   
/* 25:   */   public HandlerList getHandlers()
/* 26:   */   {
/* 27:27 */     return handlers;
/* 28:   */   }
/* 29:   */   
/* 30:   */   public static HandlerList getHandlerList()
/* 31:   */   {
/* 32:31 */     return handlers;
/* 33:   */   }
/* 34:   */   
/* 35:   */   public CraftBrother getKilled()
/* 36:   */   {
/* 37:35 */     return this.killed;
/* 38:   */   }
/* 39:   */   
/* 40:   */   public CraftBrother getKiller()
/* 41:   */   {
/* 42:39 */     return this.killer;
/* 43:   */   }
/* 44:   */   
/* 45:   */   public SCBGame getGame()
/* 46:   */   {
/* 47:43 */     return this.game;
/* 48:   */   }
/* 49:   */   
/* 50:   */   public String getDeathMessage()
/* 51:   */   {
/* 52:47 */     return this.deathmessage;
/* 53:   */   }
/* 54:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.events.SCBDeathEvent
 * JD-Core Version:    0.7.0.1
 */