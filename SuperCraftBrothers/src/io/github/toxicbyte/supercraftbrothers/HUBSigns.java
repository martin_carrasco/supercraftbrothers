/*  1:   */ package io.github.toxicbyte.supercraftbrothers;
/*  7:   */ import org.bukkit.Bukkit;
/*  8:   */ import org.bukkit.ChatColor;
/* 10:   */ import org.bukkit.Material;
/* 12:   */ import org.bukkit.block.Block;
/* 13:   */ import org.bukkit.block.BlockFace;
/* 15:   */ 
/* 16:   */ public class HUBSigns
/* 17:   */ {
/* 18:   */   public static void setup(SuperCraftBrothers plugin)
/* 19:   */   {
/* 20:14 */     Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable()
/* 21:   */     {
/* 22:   */       public void run()
/* 23:   */       {
/* 24:19 */         HUBSigns.update();
/* 25:20 */         HUBSigns.checkPlayers();
/* 26:   */       }
/* 27:22 */     }, 0L, 10L);
/* 28:   */   }
/* 29:   */   
/* 30:   */   public static void update()
/* 31:   */   {
/* 32:27 */     for (SCBGame game : SCBGameManager.getInstance().getAllGames()) {
/* 33:29 */       if (game.getMap().getLobbySign() != null)
/* 34:   */       {
/* 35:31 */         if (!(game.getMap().getLobbySign().getBlock().getState() instanceof org.bukkit.block.Sign))
/* 36:   */         {
/* 37:33 */           Block b = game.getMap().getLobbySign().getBlock();
/* 38:34 */           BlockFace[] faces = { BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST };
/* 39:35 */           Material m = Material.WALL_SIGN;
/* 40:36 */           b.setType(m);
/* 41:37 */           org.bukkit.block.Sign s = (org.bukkit.block.Sign)b.getState();
/* 42:38 */           org.bukkit.material.Sign matSign = new org.bukkit.material.Sign(Material.WALL_SIGN);
/* 43:39 */           for (BlockFace face : faces) {
/* 44:41 */             if ((b.getRelative(face).getType() != Material.AIR) && (b.getRelative(face).getType() != Material.WALL_SIGN))
/* 45:   */             {
/* 46:43 */               matSign.setFacingDirection(face.getOppositeFace());
/* 47:44 */               break;
/* 48:   */             }
/* 49:   */           }
/* 50:47 */           s.setData(matSign);
/* 51:48 */           s.update();
/* 52:   */         }
/* 53:50 */         org.bukkit.block.Sign s = (org.bukkit.block.Sign)game.getMap().getLobbySign().getBlock().getState();
/* 54:51 */         if (game.isDisabled())
/* 55:   */         {
/* 56:53 */           s.setLine(0, ChatColor.BLUE + "Disabled");
/* 57:54 */           s.setLine(1, game.getName());
/* 58:55 */           s.setLine(2, "Players: -/4");
/* 59:56 */           s.setLine(3, "-s");
/* 60:   */         }
/* 61:   */         else
/* 62:   */         {
/* 63:60 */           if (game.isInGame())
/* 64:   */           {
/* 65:62 */             if (game.getAlive() == 0) {
/* 66:64 */               game.stopGame();
/* 67:   */             }
/* 68:66 */             s.setLine(0, ChatColor.GREEN + "In Progress");
/* 69:67 */             s.setLine(1, game.getName());
/* 70:68 */             s.setLine(2, "-- Players --");
/* 71:69 */             s.setLine(3, "Alive-" + game.getAlive() + "|Dead-" + game.getDead());
/* 72:   */           }
/* 73:71 */           if (game.isInLobby())
/* 74:   */           {
/* 75:72 */             s.setLine(0, ChatColor.RED + "Lobby");
/* 76:73 */             s.setLine(1, game.getName());
/* 77:74 */             s.setLine(2, "Players: " + game.getNumberIngameInstance() + "/4");
/* 78:75 */             s.setLine(3, game.getI() + "s");
/* 79:   */           }
/* 80:   */         }
/* 81:78 */         s.update();
/* 82:   */       }
/* 83:   */     }
/* 84:   */   }
/* 85:   */   
/* 86:   */   public static void checkPlayers()
/* 87:   */   {
/* 88:84 */     for (SCBGame g : SCBGameManager.getInstance().getAllGames())
/* 89:   */     {
/* 91:86 */       for (String a : g.getHashMap(0).keySet()) {
/* 92:88 */         if (g.getHashMap(0).get(a) == null)
/* 93:   */         {
/* 94:90 */           g.getHashMap(0).remove(a);
/* 95:91 */           SCBGameManager.getInstance().removeCraftBrother(a);
/* 96:   */         }
/* 97:   */       }
/* 98:94 */       for(String s : g.getHashMap(1).keySet())
/* 99:96 */       if (g.getHashMap(1).get(s) == null)
/* :0:   */       {
/* :1:98 */         g.getHashMap(1).remove(s);
/* :2:99 */         SCBGameManager.getInstance().removeCraftBrother(s);
/* :3:   */       }
/* :4:   */     }
/* :5:   */   }
/* :6:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.HUBSigns
 * JD-Core Version:    0.7.0.1
 */