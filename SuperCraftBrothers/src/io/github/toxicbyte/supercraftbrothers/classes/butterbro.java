/*  1:   */ package io.github.toxicbyte.supercraftbrothers.classes;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;

/*  4:   */ import java.util.ArrayList;

/*  5:   */ import org.bukkit.ChatColor;
/*  6:   */ import org.bukkit.Color;
/*  7:   */ import org.bukkit.Material;
/*  8:   */ import org.bukkit.enchantments.Enchantment;
/* 10:   */ import org.bukkit.inventory.ItemStack;
/* 12:   */ import org.bukkit.inventory.meta.ItemMeta;
/* 13:   */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/* 14:   */ import org.bukkit.inventory.meta.SkullMeta;
/* 15:   */ 
/* 16:   */ public class butterbro
/* 17:   */ {
/* 18:   */   public boolean apply(CraftBrother player)
/* 19:   */   {
/* 20:16 */     ItemStack h = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
/* 21:17 */     ItemStack c = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 22:18 */     ItemStack l = new ItemStack(Material.LEATHER_LEGGINGS);
/* 23:19 */     ItemStack b = new ItemStack(Material.LEATHER_BOOTS);
/* 24:   */     
/* 25:21 */     LeatherArmorMeta cam = (LeatherArmorMeta)c.getItemMeta();
/* 26:22 */     LeatherArmorMeta lam = (LeatherArmorMeta)l.getItemMeta();
/* 27:23 */     LeatherArmorMeta bam = (LeatherArmorMeta)b.getItemMeta();
/* 28:24 */     SkullMeta hsm = (SkullMeta)h.getItemMeta();
/* 29:   */     
/* 30:26 */     hsm.setOwner("scraftbrothers6");
/* 31:27 */     cam.setColor(Color.YELLOW);
/* 32:28 */     lam.setColor(Color.PURPLE);
/* 33:29 */     bam.setColor(Color.RED);
/* 34:   */     
/* 35:31 */     c.setItemMeta(cam);
/* 36:32 */     l.setItemMeta(lam);
/* 37:33 */     b.setItemMeta(bam);
/* 38:34 */     h.setItemMeta(hsm);
/* 39:   */     
/* 40:36 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 10);
/* 41:37 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
/* 42:   */     
/* 43:39 */     player.getPlayer().getInventory().setHelmet(h);
/* 44:40 */     player.getPlayer().getInventory().setChestplate(c);
/* 45:41 */     player.getPlayer().getInventory().setLeggings(l);
/* 46:42 */     player.getPlayer().getInventory().setBoots(b);
/* 47:   */     
/* 48:44 */     ItemStack i1 = new ItemStack(Material.GOLD_INGOT, 1);
/* 49:45 */     ItemStack i2 = new ItemStack(Material.RED_ROSE, 10);
/* 50:   */     
/* 51:47 */     ItemMeta im2 = i2.getItemMeta();
/* 52:48 */     im2.setDisplayName(ChatColor.RED + "Fireball Flower");
/* 53:49 */     ArrayList<String> im2l = new ArrayList<String>();
/* 54:50 */     im2l.add("Right click to launch a fireball");
/* 55:51 */     im2.setLore(im2l);
/* 56:52 */     i2.setItemMeta(im2);
/* 57:   */     
/* 58:54 */     i1.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
/* 59:55 */     i1.addUnsafeEnchantment(Enchantment.KNOCKBACK, 2);
/* 60:   */     
/* 61:57 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i1 });
/* 62:58 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i2 });
/* 63:59 */     return true;
/* 64:   */   }
/* 65:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.classes.butterbro
 * JD-Core Version:    0.7.0.1
 */