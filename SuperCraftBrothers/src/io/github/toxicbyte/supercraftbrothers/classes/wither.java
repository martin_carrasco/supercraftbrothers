/*  1:   */ package io.github.toxicbyte.supercraftbrothers.classes;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;

/*  4:   */ import org.bukkit.Color;
/*  5:   */ import org.bukkit.Material;
/*  6:   */ import org.bukkit.enchantments.Enchantment;
/*  8:   */ import org.bukkit.inventory.ItemStack;
/* 10:   */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/* 11:   */ import org.bukkit.inventory.meta.PotionMeta;
/* 12:   */ import org.bukkit.potion.PotionEffect;
/* 13:   */ import org.bukkit.potion.PotionEffectType;
/* 14:   */ 
/* 15:   */ public class wither
/* 16:   */ {
/* 17:   */   public boolean apply(CraftBrother player)
/* 18:   */   {
/* 19:18 */     ItemStack h = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
/* 20:19 */     ItemStack c = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 21:20 */     ItemStack l = new ItemStack(Material.LEATHER_LEGGINGS);
/* 22:21 */     ItemStack b = new ItemStack(Material.LEATHER_BOOTS);
/* 23:   */     
/* 24:23 */     LeatherArmorMeta cam = (LeatherArmorMeta)c.getItemMeta();
/* 25:24 */     LeatherArmorMeta lam = (LeatherArmorMeta)l.getItemMeta();
/* 26:25 */     LeatherArmorMeta bam = (LeatherArmorMeta)b.getItemMeta();
/* 27:   */     
/* 28:27 */     cam.setColor(Color.BLACK);
/* 29:28 */     lam.setColor(Color.BLACK);
/* 30:29 */     bam.setColor(Color.BLACK);
/* 31:30 */     c.setItemMeta(cam);
/* 32:31 */     l.setItemMeta(lam);
/* 33:32 */     b.setItemMeta(bam);
/* 34:   */     
/* 35:   */ 
/* 36:35 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 10);
/* 37:36 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
/* 38:   */     
/* 39:38 */     player.getPlayer().getInventory().setHelmet(h);
/* 40:39 */     player.getPlayer().getInventory().setChestplate(c);
/* 41:40 */     player.getPlayer().getInventory().setLeggings(l);
/* 42:41 */     player.getPlayer().getInventory().setBoots(b);
/* 43:   */     
/* 44:   */ 
/* 45:   */ 
/* 46:45 */     ItemStack bow = new ItemStack(Material.BOW, 1);
/* 47:46 */     ItemStack star = new ItemStack(Material.NETHER_STAR, 1);
/* 48:47 */     ItemStack arrow = new ItemStack(Material.ARROW, 1);
/* 49:48 */     ItemStack potion = new ItemStack(Material.POTION, 64, (short)16384);
/* 50:   */     
/* 51:50 */     PotionMeta meta = (PotionMeta)potion.getItemMeta();
/* 52:51 */     meta.addCustomEffect(new PotionEffect(PotionEffectType.WITHER, 200, 1), true);
/* 53:52 */     potion.setItemMeta(meta);
/* 54:   */     
/* 55:   */ 
/* 56:55 */     bow.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
/* 57:56 */     bow.addUnsafeEnchantment(Enchantment.ARROW_KNOCKBACK, 1);
/* 58:57 */     star.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
/* 59:   */     
/* 60:59 */     player.getPlayer().getInventory().addItem(new ItemStack[] { bow });
/* 61:60 */     player.getPlayer().getInventory().addItem(new ItemStack[] { star });
/* 62:61 */     player.getPlayer().getInventory().addItem(new ItemStack[] { arrow });
/* 63:62 */     player.getPlayer().getInventory().addItem(new ItemStack[] { potion });
/* 64:63 */     return true;
/* 65:   */   }
/* 66:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.classes.wither
 * JD-Core Version:    0.7.0.1
 */