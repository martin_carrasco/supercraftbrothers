/*  1:   */ package io.github.toxicbyte.supercraftbrothers.classes;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;

/*  4:   */ import org.bukkit.Color;
/*  5:   */ import org.bukkit.Material;
/*  6:   */ import org.bukkit.enchantments.Enchantment;
/*  8:   */ import org.bukkit.inventory.ItemStack;
/* 10:   */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/* 11:   */ import org.bukkit.inventory.meta.SkullMeta;
/* 12:   */ 
/* 13:   */ public class dweller
/* 14:   */ {
/* 15:   */   public boolean apply(CraftBrother player)
/* 16:   */   {
/* 17:15 */     ItemStack h = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
/* 18:16 */     ItemStack c = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 19:17 */     ItemStack l = new ItemStack(Material.LEATHER_LEGGINGS);
/* 20:18 */     ItemStack b = new ItemStack(Material.LEATHER_BOOTS);
/* 21:19 */     LeatherArmorMeta cam = (LeatherArmorMeta)c.getItemMeta();
/* 22:20 */     LeatherArmorMeta lam = (LeatherArmorMeta)l.getItemMeta();
/* 23:21 */     LeatherArmorMeta bam = (LeatherArmorMeta)b.getItemMeta();
/* 24:22 */     SkullMeta hsm = (SkullMeta)h.getItemMeta();
/* 25:23 */     hsm.setOwner("CavemanFilms");
/* 26:24 */     cam.setColor(Color.GREEN);
/* 27:25 */     lam.setColor(Color.BLACK);
/* 28:26 */     bam.setColor(Color.BLACK);
/* 29:27 */     c.setItemMeta(cam);
/* 30:28 */     l.setItemMeta(lam);
/* 31:29 */     b.setItemMeta(bam);
/* 32:30 */     h.setItemMeta(hsm);
/* 33:31 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 10);
/* 34:32 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
/* 35:33 */     player.getPlayer().getInventory().setHelmet(h);
/* 36:34 */     player.getPlayer().getInventory().setChestplate(c);
/* 37:35 */     player.getPlayer().getInventory().setLeggings(l);
/* 38:36 */     player.getPlayer().getInventory().setBoots(b);
/* 39:37 */     ItemStack i1 = new ItemStack(Material.BONE, 1);
/* 40:38 */     i1.addUnsafeEnchantment(Enchantment.KNOCKBACK, 2);
/* 41:39 */     i1.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
/* 42:40 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i1 });
/* 43:41 */     return true;
/* 44:   */   }
/* 45:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.classes.dweller
 * JD-Core Version:    0.7.0.1
 */