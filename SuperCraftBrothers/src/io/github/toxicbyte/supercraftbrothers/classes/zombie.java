/*  1:   */ package io.github.toxicbyte.supercraftbrothers.classes;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;

/*  4:   */ import org.bukkit.Color;
/*  5:   */ import org.bukkit.Material;
/*  6:   */ import org.bukkit.enchantments.Enchantment;
/*  8:   */ import org.bukkit.inventory.ItemStack;
/* 10:   */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/* 11:   */ 
/* 12:   */ public class zombie
/* 13:   */ {
/* 14:   */   public boolean apply(CraftBrother player)
/* 15:   */   {
/* 16:14 */     ItemStack h = new ItemStack(Material.SKULL_ITEM, 1, (short)2);
/* 17:15 */     ItemStack c = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 18:16 */     ItemStack l = new ItemStack(Material.LEATHER_LEGGINGS);
/* 19:17 */     ItemStack b = new ItemStack(Material.LEATHER_BOOTS);
/* 20:18 */     LeatherArmorMeta cam = (LeatherArmorMeta)c.getItemMeta();
/* 21:19 */     LeatherArmorMeta lam = (LeatherArmorMeta)l.getItemMeta();
/* 22:20 */     LeatherArmorMeta bam = (LeatherArmorMeta)b.getItemMeta();
/* 23:21 */     cam.setColor(Color.AQUA);
/* 24:22 */     lam.setColor(Color.BLUE);
/* 25:23 */     bam.setColor(Color.RED);
/* 26:24 */     c.setItemMeta(cam);
/* 27:25 */     l.setItemMeta(lam);
/* 28:26 */     b.setItemMeta(bam);
/* 29:27 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 10);
/* 30:28 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
/* 31:29 */     player.getPlayer().getInventory().setHelmet(h);
/* 32:30 */     player.getPlayer().getInventory().setChestplate(c);
/* 33:31 */     player.getPlayer().getInventory().setLeggings(l);
/* 34:32 */     player.getPlayer().getInventory().setBoots(b);
/* 35:33 */     ItemStack i1 = new ItemStack(Material.IRON_SPADE, 1);
/* 36:34 */     i1.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
/* 37:35 */     i1.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
/* 38:36 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i1 });
/* 39:37 */     return true;
/* 40:   */   }
/* 41:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.classes.zombie
 * JD-Core Version:    0.7.0.1
 */