/*  1:   */ package io.github.toxicbyte.supercraftbrothers.classes;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;

/*  4:   */ import org.bukkit.Color;
/*  5:   */ import org.bukkit.Material;
/*  6:   */ import org.bukkit.enchantments.Enchantment;
/*  8:   */ import org.bukkit.inventory.ItemStack;
/* 10:   */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/* 11:   */ 
/* 12:   */ public class cactus
/* 13:   */ {
/* 14:   */   public boolean apply(CraftBrother player)
/* 15:   */   {
/* 16:14 */     ItemStack h = new ItemStack(Material.CACTUS);
/* 17:15 */     ItemStack c = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 18:16 */     ItemStack l = new ItemStack(Material.LEATHER_LEGGINGS);
/* 19:17 */     ItemStack b = new ItemStack(Material.LEATHER_BOOTS);
/* 20:   */     
/* 21:19 */     LeatherArmorMeta cam = (LeatherArmorMeta)c.getItemMeta();
/* 22:20 */     LeatherArmorMeta lam = (LeatherArmorMeta)l.getItemMeta();
/* 23:21 */     LeatherArmorMeta bam = (LeatherArmorMeta)b.getItemMeta();
/* 24:22 */     cam.setColor(Color.GREEN);
/* 25:23 */     lam.setColor(Color.GREEN);
/* 26:24 */     bam.setColor(Color.GREEN);
/* 27:25 */     c.setItemMeta(cam);
/* 28:26 */     l.setItemMeta(lam);
/* 29:27 */     b.setItemMeta(bam);
/* 30:28 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 10);
/* 31:29 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
/* 32:30 */     b.addUnsafeEnchantment(Enchantment.THORNS, 1);
/* 33:31 */     l.addUnsafeEnchantment(Enchantment.THORNS, 1);
/* 34:32 */     c.addUnsafeEnchantment(Enchantment.THORNS, 1);
/* 35:33 */     h.addUnsafeEnchantment(Enchantment.THORNS, 1);
/* 36:34 */     player.getPlayer().getInventory().setHelmet(h);
/* 37:35 */     player.getPlayer().getInventory().setChestplate(c);
/* 38:36 */     player.getPlayer().getInventory().setLeggings(l);
/* 39:37 */     player.getPlayer().getInventory().setBoots(b);
/* 40:38 */     ItemStack i1 = new ItemStack(Material.WOOD_SWORD, 1);
/* 41:39 */     i1.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
/* 42:40 */     i1.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
/* 43:41 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i1 });
/* 44:42 */     return true;
/* 45:   */   }
/* 46:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.classes.cactus
 * JD-Core Version:    0.7.0.1
 */