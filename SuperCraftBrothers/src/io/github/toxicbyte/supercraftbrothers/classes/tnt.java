/*  1:   */ package io.github.toxicbyte.supercraftbrothers.classes;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;

/*  4:   */ import org.bukkit.Color;
/*  5:   */ import org.bukkit.Material;
/*  6:   */ import org.bukkit.enchantments.Enchantment;
/*  8:   */ import org.bukkit.inventory.ItemStack;
/* 10:   */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/* 11:   */ 
/* 12:   */ public class tnt
/* 13:   */ {
/* 14:   */   public boolean apply(CraftBrother player)
/* 15:   */   {
/* 16:15 */     ItemStack h = new ItemStack(Material.TNT);
/* 17:16 */     ItemStack c = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 18:17 */     ItemStack l = new ItemStack(Material.LEATHER_LEGGINGS);
/* 19:18 */     ItemStack b = new ItemStack(Material.LEATHER_BOOTS);
/* 20:   */     
/* 21:20 */     LeatherArmorMeta cam = (LeatherArmorMeta)c.getItemMeta();
/* 22:21 */     LeatherArmorMeta lam = (LeatherArmorMeta)l.getItemMeta();
/* 23:22 */     LeatherArmorMeta bam = (LeatherArmorMeta)b.getItemMeta();
/* 24:   */     
/* 25:24 */     cam.setColor(Color.RED);
/* 26:25 */     lam.setColor(Color.RED);
/* 27:26 */     bam.setColor(Color.RED);
/* 28:27 */     c.setItemMeta(cam);
/* 29:28 */     l.setItemMeta(lam);
/* 30:29 */     b.setItemMeta(bam);
/* 31:   */     
/* 32:   */ 
/* 33:32 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 10);
/* 34:33 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
/* 35:   */     
/* 36:35 */     player.getPlayer().getInventory().setHelmet(h);
/* 37:36 */     player.getPlayer().getInventory().setChestplate(c);
/* 38:37 */     player.getPlayer().getInventory().setLeggings(l);
/* 39:38 */     player.getPlayer().getInventory().setBoots(b);
/* 40:   */     
/* 41:   */ 
/* 42:   */ 
/* 43:42 */     ItemStack sword = new ItemStack(Material.WOOD_SWORD, 1);
/* 44:43 */     ItemStack tnt = new ItemStack(Material.TNT, 3);
/* 45:   */     
/* 46:45 */     sword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
/* 47:   */     
/* 48:47 */     player.getPlayer().getInventory().addItem(new ItemStack[] { sword });
/* 49:48 */     player.getPlayer().getInventory().addItem(new ItemStack[] { tnt });
/* 50:49 */     return true;
/* 51:   */   }
/* 52:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.classes.tnt
 * JD-Core Version:    0.7.0.1
 */