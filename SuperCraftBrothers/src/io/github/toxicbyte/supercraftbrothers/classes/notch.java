/*  1:   */ package io.github.toxicbyte.supercraftbrothers.classes;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;

/*  4:   */ import org.bukkit.Color;
/*  5:   */ import org.bukkit.Material;
/*  6:   */ import org.bukkit.enchantments.Enchantment;
/*  8:   */ import org.bukkit.inventory.ItemStack;
/* 10:   */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/* 11:   */ import org.bukkit.inventory.meta.SkullMeta;
/* 12:   */ 
/* 13:   */ public class notch
/* 14:   */ {
/* 15:   */   public boolean apply(CraftBrother player)
/* 16:   */   {
/* 17:16 */     ItemStack h = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
/* 18:17 */     ItemStack c = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 19:18 */     ItemStack l = new ItemStack(Material.LEATHER_LEGGINGS);
/* 20:19 */     ItemStack b = new ItemStack(Material.LEATHER_BOOTS);
/* 21:   */     
/* 22:21 */     LeatherArmorMeta cam = (LeatherArmorMeta)c.getItemMeta();
/* 23:22 */     LeatherArmorMeta lam = (LeatherArmorMeta)l.getItemMeta();
/* 24:23 */     LeatherArmorMeta bam = (LeatherArmorMeta)b.getItemMeta();
/* 25:24 */     SkullMeta hsm = (SkullMeta)h.getItemMeta();
/* 26:   */     
/* 27:26 */     hsm.setOwner("Notch");
/* 28:27 */     cam.setColor(Color.RED);
/* 29:28 */     lam.setColor(Color.RED);
/* 30:29 */     bam.setColor(Color.BLACK);
/* 31:30 */     c.setItemMeta(cam);
/* 32:31 */     l.setItemMeta(lam);
/* 33:32 */     b.setItemMeta(bam);
/* 34:33 */     h.setItemMeta(hsm);
/* 35:   */     
/* 36:   */ 
/* 37:36 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 10);
/* 38:37 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
/* 39:   */     
/* 40:39 */     player.getPlayer().getInventory().setHelmet(h);
/* 41:40 */     player.getPlayer().getInventory().setChestplate(c);
/* 42:41 */     player.getPlayer().getInventory().setLeggings(l);
/* 43:42 */     player.getPlayer().getInventory().setBoots(b);
/* 44:   */     
/* 45:   */ 
/* 46:   */ 
/* 47:46 */     ItemStack rod = new ItemStack(Material.STONE_SWORD, 1);
/* 48:47 */     ItemStack bow = new ItemStack(Material.GRASS, 1);
/* 49:   */     
/* 50:49 */     player.getPlayer().getInventory().addItem(new ItemStack[] { rod });
/* 51:50 */     player.getPlayer().getInventory().addItem(new ItemStack[] { bow });
/* 52:51 */     return true;
/* 53:   */   }
/* 54:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.classes.notch
 * JD-Core Version:    0.7.0.1
 */