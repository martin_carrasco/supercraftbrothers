/*  1:   */ package io.github.toxicbyte.supercraftbrothers.classes;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;

/*  4:   */ import java.util.ArrayList;

/*  5:   */ import org.bukkit.ChatColor;
/*  6:   */ import org.bukkit.Color;
/*  7:   */ import org.bukkit.Material;
/*  8:   */ import org.bukkit.enchantments.Enchantment;
/* 10:   */ import org.bukkit.inventory.ItemStack;
/* 12:   */ import org.bukkit.inventory.meta.ItemMeta;
/* 13:   */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/* 14:   */ import org.bukkit.inventory.meta.SkullMeta;
/* 15:   */ 
/* 16:   */ public class witch
/* 17:   */ {
/* 18:   */   public boolean apply(CraftBrother player)
/* 19:   */   {
/* 20:16 */     ItemStack h = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
/* 21:17 */     ItemStack c = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 22:18 */     ItemStack l = new ItemStack(Material.LEATHER_LEGGINGS);
/* 23:19 */     ItemStack b = new ItemStack(Material.LEATHER_BOOTS);
/* 24:20 */     LeatherArmorMeta cam = (LeatherArmorMeta)c.getItemMeta();
/* 25:21 */     LeatherArmorMeta lam = (LeatherArmorMeta)l.getItemMeta();
/* 26:22 */     LeatherArmorMeta bam = (LeatherArmorMeta)b.getItemMeta();
/* 27:23 */     SkullMeta hsm = (SkullMeta)h.getItemMeta();
/* 28:24 */     hsm.setOwner("scrafbrothers4");
/* 29:25 */     cam.setColor(Color.PURPLE);
/* 30:26 */     lam.setColor(Color.PURPLE);
/* 31:27 */     bam.setColor(Color.PURPLE);
/* 32:28 */     c.setItemMeta(cam);
/* 33:29 */     l.setItemMeta(lam);
/* 34:30 */     b.setItemMeta(bam);
/* 35:31 */     h.setItemMeta(hsm);
/* 36:32 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 10);
/* 37:33 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
/* 38:34 */     player.getPlayer().getInventory().setHelmet(h);
/* 39:35 */     player.getPlayer().getInventory().setChestplate(c);
/* 40:36 */     player.getPlayer().getInventory().setLeggings(l);
/* 41:37 */     player.getPlayer().getInventory().setBoots(b);
/* 42:38 */     ItemStack i1 = new ItemStack(Material.WHEAT, 1);
/* 43:39 */     ItemStack i2 = new ItemStack(Material.POTION, 10, (short)16428);
/* 44:40 */     ItemStack i3 = new ItemStack(Material.POTION, 3, (short)16388);
/* 45:41 */     ItemStack i4 = new ItemStack(Material.POTION, 2, (short)16424);
/* 46:42 */     ItemMeta im1 = i1.getItemMeta();
/* 47:43 */     im1.setDisplayName(ChatColor.DARK_RED + "Magic Broom");
/* 48:44 */     ArrayList<String> im1l = new ArrayList<String>();
/* 49:45 */     im1l.add("Use it to jump in the air");
/* 50:46 */     im1l.add("up to four times in a row");
/* 51:47 */     im1.setLore(im1l);
/* 52:48 */     i1.setItemMeta(im1);
/* 53:49 */     i1.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
/* 54:50 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i1 });
/* 55:51 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i2 });
/* 56:52 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i3 });
/* 57:53 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i4 });
/* 58:54 */     return true;
/* 59:   */   }
/* 60:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.classes.witch
 * JD-Core Version:    0.7.0.1
 */