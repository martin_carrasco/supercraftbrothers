/*  1:   */ package io.github.toxicbyte.supercraftbrothers.classes;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;

/*  4:   */ import org.bukkit.Color;
/*  5:   */ import org.bukkit.Material;
/*  6:   */ import org.bukkit.enchantments.Enchantment;
/*  8:   */ import org.bukkit.inventory.ItemStack;
/* 10:   */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/* 11:   */ 
/* 12:   */ public class creeper
/* 13:   */ {
/* 14:   */   public boolean apply(CraftBrother player)
/* 15:   */   {
/* 16:14 */     ItemStack h = new ItemStack(Material.SKULL_ITEM, 1, (short)4);
/* 17:15 */     ItemStack c = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 18:16 */     ItemStack l = new ItemStack(Material.LEATHER_LEGGINGS);
/* 19:17 */     ItemStack b = new ItemStack(Material.LEATHER_BOOTS);
/* 20:18 */     LeatherArmorMeta cam = (LeatherArmorMeta)c.getItemMeta();
/* 21:19 */     LeatherArmorMeta lam = (LeatherArmorMeta)l.getItemMeta();
/* 22:20 */     LeatherArmorMeta bam = (LeatherArmorMeta)b.getItemMeta();
/* 23:21 */     cam.setColor(Color.GREEN);
/* 24:22 */     lam.setColor(Color.GREEN);
/* 25:23 */     bam.setColor(Color.GREEN);
/* 26:24 */     c.setItemMeta(cam);
/* 27:25 */     l.setItemMeta(lam);
/* 28:26 */     b.setItemMeta(bam);
/* 29:27 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 10);
/* 30:28 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
/* 31:29 */     player.getPlayer().getInventory().setHelmet(h);
/* 32:30 */     player.getPlayer().getInventory().setChestplate(c);
/* 33:31 */     player.getPlayer().getInventory().setLeggings(l);
/* 34:32 */     player.getPlayer().getInventory().setBoots(b);
/* 35:33 */     ItemStack i1 = new ItemStack(Material.SULPHUR, 1);
/* 36:34 */     ItemStack i2 = new ItemStack(Material.POTION, 64, (short)16396);
/* 37:35 */     ItemStack i3 = new ItemStack(Material.TNT, 10);
/* 38:36 */     i1.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
/* 39:37 */     i1.addUnsafeEnchantment(Enchantment.KNOCKBACK, 2);
/* 40:38 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i1 });
/* 41:39 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i3 });
/* 42:40 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i2 });
/* 43:41 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i2 });
/* 44:42 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i2 });
/* 45:43 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i2 });
/* 46:44 */     return true;
/* 47:   */   }
/* 48:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.classes.creeper
 * JD-Core Version:    0.7.0.1
 */