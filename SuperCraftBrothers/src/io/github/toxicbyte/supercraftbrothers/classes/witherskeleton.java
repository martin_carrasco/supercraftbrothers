/*  1:   */ package io.github.toxicbyte.supercraftbrothers.classes;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;

/*  4:   */ import org.bukkit.Color;
/*  5:   */ import org.bukkit.Material;
/*  6:   */ import org.bukkit.enchantments.Enchantment;
/*  8:   */ import org.bukkit.inventory.ItemStack;
/* 10:   */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/* 11:   */ import org.bukkit.inventory.meta.SkullMeta;
/* 12:   */ 
/* 13:   */ public class witherskeleton
/* 14:   */ {
/* 15:   */   public boolean apply(CraftBrother player)
/* 16:   */   {
/* 17:14 */     ItemStack h = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
/* 18:15 */     ItemStack c = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 19:16 */     ItemStack l = new ItemStack(Material.LEATHER_LEGGINGS);
/* 20:17 */     ItemStack b = new ItemStack(Material.LEATHER_BOOTS);
/* 21:18 */     LeatherArmorMeta cam = (LeatherArmorMeta)c.getItemMeta();
/* 22:19 */     LeatherArmorMeta lam = (LeatherArmorMeta)l.getItemMeta();
/* 23:20 */     LeatherArmorMeta bam = (LeatherArmorMeta)b.getItemMeta();
/* 24:21 */     SkullMeta hsm = (SkullMeta)h.getItemMeta();
/* 25:22 */     hsm.setOwner("scraftbrothers8");
/* 26:23 */     cam.setColor(Color.BLACK);
/* 27:24 */     lam.setColor(Color.BLACK);
/* 28:25 */     bam.setColor(Color.BLACK);
/* 29:26 */     c.setItemMeta(cam);
/* 30:27 */     l.setItemMeta(lam);
/* 31:28 */     b.setItemMeta(bam);
/* 32:29 */     h.setItemMeta(hsm);
/* 33:30 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 10);
/* 34:31 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
/* 35:32 */     player.getPlayer().getInventory().setHelmet(h);
/* 36:33 */     player.getPlayer().getInventory().setChestplate(c);
/* 37:34 */     player.getPlayer().getInventory().setLeggings(l);
/* 38:35 */     player.getPlayer().getInventory().setBoots(b);
/* 39:36 */     ItemStack i1 = new ItemStack(Material.EYE_OF_ENDER, 1);
/* 40:37 */     ItemStack i2 = new ItemStack(Material.ENDER_PEARL, 5);
/* 41:38 */     ItemStack i3 = new ItemStack(Material.BOW, 1);
/* 42:39 */     ItemStack i4 = new ItemStack(Material.ARROW, 1);
/* 43:40 */     i1.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 1);
/* 44:41 */     i1.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
/* 45:42 */     i3.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
/* 46:43 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i1 });
/* 47:44 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i2 });
/* 48:45 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i3 });
/* 49:46 */     player.getPlayer().getInventory().addItem(new ItemStack[] { i4 });
/* 50:47 */     return true;
/* 51:   */   }
/* 52:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.classes.witherskeleton
 * JD-Core Version:    0.7.0.1
 */