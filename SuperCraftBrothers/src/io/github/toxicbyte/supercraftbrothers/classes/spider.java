/*  1:   */ package io.github.toxicbyte.supercraftbrothers.classes;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;

/*  4:   */ import org.bukkit.Color;
/*  5:   */ import org.bukkit.Material;
/*  6:   */ import org.bukkit.enchantments.Enchantment;
/*  8:   */ import org.bukkit.inventory.ItemStack;
/* 10:   */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/* 11:   */ import org.bukkit.inventory.meta.SkullMeta;
/* 12:   */ import org.bukkit.potion.PotionEffect;
/* 13:   */ import org.bukkit.potion.PotionEffectType;
/* 14:   */ 
/* 15:   */ public class spider
/* 16:   */ {
/* 17:   */   public boolean apply(CraftBrother player)
/* 18:   */   {
/* 19:18 */     ItemStack h = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
/* 20:19 */     ItemStack c = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 21:20 */     ItemStack l = new ItemStack(Material.LEATHER_LEGGINGS);
/* 22:21 */     ItemStack b = new ItemStack(Material.LEATHER_BOOTS);
/* 23:   */     
/* 24:23 */     LeatherArmorMeta cam = (LeatherArmorMeta)c.getItemMeta();
/* 25:24 */     LeatherArmorMeta lam = (LeatherArmorMeta)l.getItemMeta();
/* 26:25 */     LeatherArmorMeta bam = (LeatherArmorMeta)b.getItemMeta();
/* 27:26 */     SkullMeta hsm = (SkullMeta)h.getItemMeta();
/* 28:   */     
/* 29:28 */     hsm.setOwner("Kelevra_V");
/* 30:29 */     cam.setColor(Color.RED);
/* 31:30 */     lam.setColor(Color.RED);
/* 32:31 */     bam.setColor(Color.RED);
/* 33:32 */     c.setItemMeta(cam);
/* 34:33 */     l.setItemMeta(lam);
/* 35:34 */     b.setItemMeta(bam);
/* 36:35 */     h.setItemMeta(hsm);
/* 37:   */     
/* 38:   */ 
/* 39:38 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 10);
/* 40:39 */     b.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
/* 41:   */     
/* 42:41 */     player.getPlayer().getInventory().setHelmet(h);
/* 43:42 */     player.getPlayer().getInventory().setChestplate(c);
/* 44:43 */     player.getPlayer().getInventory().setLeggings(l);
/* 45:44 */     player.getPlayer().getInventory().setBoots(b);
/* 46:   */     
/* 47:   */ 
/* 48:   */ 
/* 49:48 */     ItemStack rod = new ItemStack(Material.SPIDER_EYE, 1);
/* 50:   */     
/* 51:50 */     rod.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
/* 52:51 */     rod.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
/* 53:   */     
/* 54:53 */     player.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 2147483647, 1));
/* 55:54 */     player.getPlayer().getInventory().addItem(new ItemStack[] { rod });
/* 56:55 */     return true;
/* 57:   */   }
/* 58:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.classes.spider
 * JD-Core Version:    0.7.0.1
 */