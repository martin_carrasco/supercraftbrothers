/*   1:    */ package io.github.toxicbyte.supercraftbrothers;
/*   2:    */ 
/*   3:    */ import io.github.toxicbyte.supercraftbrothers.utilities.ClassUtils;

/*   4:    */ import java.util.ArrayList;
/*   5:    */ import java.util.Arrays;
/*   7:    */ import java.util.HashMap;
/*   8:    */ import java.util.List;

/*   9:    */ import org.bukkit.Bukkit;
/*  10:    */ import org.bukkit.ChatColor;
/*  11:    */ import org.bukkit.Location;
/*  14:    */ import org.bukkit.entity.Player;
/*  15:    */ import org.bukkit.potion.PotionEffect;
/*  16:    */ import org.bukkit.potion.PotionEffectType;
/*  17:    */ import org.bukkit.scheduler.BukkitRunnable;
/*  19:    */ import org.bukkit.scoreboard.DisplaySlot;
/*  20:    */ import org.bukkit.scoreboard.Objective;
/*  21:    */ import org.bukkit.scoreboard.Score;
/*  22:    */ import org.bukkit.scoreboard.Scoreboard;
/*  24:    */ 
/*  25:    */ public class SCBGame
/*  26:    */ {
/*  27:    */   private String name;
/*  28:    */   private SCBMap map;
/*  29: 23 */   private int i = 30;
/*  30: 24 */   private int TaskId = 0;
/*  31: 25 */   private boolean inLobby = true;
/*  32: 26 */   private boolean isDisabled = false;
/*  33:    */   private SuperCraftBrothers plugin;
/*  34: 28 */   private HashMap<String, CraftBrother> ingameinstance = new HashMap<String, CraftBrother>();
/*  35: 29 */   private HashMap<String, CraftBrother> ingame = new HashMap<String, CraftBrother>();
/*  36:    */   
/*  37:    */   public SCBGame(SuperCraftBrothers plugin, String name, boolean isDisabled)
/*  38:    */   {
/*  39: 32 */     this.name = name;
/*  40: 33 */     this.map = new SCBMap(this, plugin);
/*  41: 34 */     this.map.load();
/*  42: 35 */     this.plugin = plugin;
/*  43: 36 */     this.isDisabled = isDisabled;
/*  44:    */   }
/*  45:    */   
/*  46:    */   public String getName()
/*  47:    */   {
/*  48: 40 */     return this.name;
/*  49:    */   }
/*  50:    */   
/*  51:    */   public SCBMap getMap()
/*  52:    */   {
/*  53: 43 */     return this.map;
/*  54:    */   }
/*  55:    */   
/*  56:    */   public HashMap<String, CraftBrother> getHashMap(int i)
/*  57:    */   {
/*  58: 47 */     switch (i)
/*  59:    */     {
/*  60:    */     case 0: 
/*  61: 50 */       return this.ingameinstance;
/*  62:    */     case 1: 
/*  63: 52 */       return this.ingame;
/*  64:    */     }
/*  65: 54 */     return null;
/*  66:    */   }
/*  67:    */   
/*  68:    */   public void joinLobby(Player bro)
/*  69:    */   {
/*  70: 59 */     if (this.ingameinstance.containsKey(bro.getName())) {
/*  71: 60 */       return;
/*  72:    */     }
/*  73: 61 */     if (getNumberIngameInstance() > 3)
/*  74:    */     {
/*  75: 62 */       bro.sendMessage(ChatColor.RED + "This lobby is full! Try joining another lobby");
/*  76: 63 */       return;
/*  77:    */     }
/*  78: 65 */     SCBGameManager.getInstance().addCraftBrother(bro.getName());
/*  79: 66 */     CraftBrother cBro = SCBGameManager.getInstance().getCraftBrother(bro.getName());
/*  80: 67 */     cBro.setReason(CraftBrother.respawnreason.TOLOBBY);
/*  81: 68 */     cBro.setInLobby(true);
/*  82: 69 */     cBro.setCurrentGame(this);
/*  83: 70 */     this.ingameinstance.put(bro.getName(), cBro);
/*  84: 71 */     bro.teleport(this.map.getClassLobby());
/*  85: 72 */     InventoryConfig.giveItems(bro);
/*  86: 73 */     GemScoreboard.addPlayer(bro);
/*  87: 74 */     bro.sendMessage(ChatColor.GREEN + "You have joined " + ChatColor.YELLOW + getName() + ChatColor.GREEN + ". Choose your class by clicking on one of the signs.");
/*  88: 75 */     if (getNumberIngameInstance() == 4) {
/*  89: 76 */       startLobbyCountdown();
/*  90:    */     }
/*  91:    */   }
/*  92:    */   
/*  93:    */   public void leaveLobby(Player bro, boolean fromEvent)
/*  94:    */   {
/*  95: 81 */     String name = bro.getName();
/*  96: 82 */     if (!this.ingameinstance.containsKey(name)) {
/*  97: 83 */       return;
/*  98:    */     }
/*  99: 84 */     this.ingameinstance.remove(name);
/* 100: 85 */     if (!fromEvent)
/* 101:    */     {
/* 102: 87 */       bro.getPlayer().setAllowFlight(true);
/* 103: 88 */       bro.teleport(SCBGameManager.getInstance().getMainLobby());
/* 104: 89 */       InventoryConfig.giveItems(bro);
/* 105: 90 */       GemScoreboard.addPlayer(bro);
/* 106: 91 */       bro.sendMessage(ChatColor.GREEN + "You have left the game and returned to the main lobby.");
/* 107:    */     }
/* 108: 93 */     SCBGameManager.getInstance().removeCraftBrother(name);
/* 109:    */   }
/* 110:    */   
/* 111:    */   public void joinGame(Player bro)
/* 112:    */   {
/* 113: 98 */     this.ingame.put(bro.getName(), SCBGameManager.getInstance().getCraftBrother(bro));
/* 114:    */   }
/* 115:    */   
/* 116:    */   public void leaveGame(Player bro, boolean fromEvent)
/* 117:    */   {
/* 118:102 */     String name = bro.getName();
/* 119:103 */     if ((!this.ingame.containsKey(name)) || (!this.ingameinstance.containsKey(name))) {
/* 120:104 */       return;
/* 121:    */     }
/* 122:105 */     this.ingame.remove(name);
/* 123:106 */     this.ingameinstance.remove(name);
/* 124:107 */     broadcast(ChatColor.RED + name + " has left the game");
/* 125:108 */     CraftBrother bro1 = SCBGameManager.getInstance().getCraftBrother(bro.getName());
/* 126:109 */     if (!fromEvent) {
/* 127:111 */       if (bro1.isRespawning())
/* 128:    */       {
/* 129:113 */         bro1.setReason(CraftBrother.respawnreason.TOMAIN);
/* 130:    */       }
/* 131:    */       else
/* 132:    */       {
/* 133:117 */         bro.getPlayer().setAllowFlight(true);
/* 134:118 */         bro.teleport(SCBGameManager.getInstance().getMainLobby());
/* 135:119 */         GemScoreboard.addPlayer(bro);
/* 136:120 */         InventoryConfig.giveItems(bro);
/* 137:121 */         bro.sendMessage(ChatColor.RED + "You left the game");
/* 138:    */       }
/* 139:    */     }
/* 140:124 */     SCBGameManager.getInstance().removeCraftBrother(name);
/* 141:125 */     arenaStats();
/* 142:126 */     checkWin();
/* 143:    */   }
/* 144:    */   
/* 145:    */   public void checkWin()
/* 146:    */   {
/* 147:131 */     if (this.ingame.size() == 1) {
/* 148:133 */       winGame((CraftBrother)this.ingame.values().toArray()[0]);
/* 149:    */     }
/* 150:135 */     if (this.ingame.size() == 0) {
/* 151:137 */       stopGame();
/* 152:    */     }
/* 153:    */   }
/* 154:    */   
/* 155:    */   public void startGame()
/* 156:    */   {
/* 157:143 */     if (isInLobby())
/* 158:    */     {
/* 159:145 */       List<Location> spawns = Arrays.asList(new Location[] { this.map.getSp1(), this.map.getSp2(), this.map.getSp3(), this.map.getSp4() });
/* 160:146 */       List<CraftBrother> bros = new ArrayList<CraftBrother>(this.ingameinstance.values());
/* 161:147 */       for (int i = 0; i < bros.size(); i++) {
/* 162:148 */         bros.get(i).getPlayer().teleport(spawns.get(i));
/* 163:    */       }
/* 164:150 */       for (CraftBrother bro : bros)
/* 165:    */       {
/* 166:152 */         bro.apply();
/* 167:153 */         bro.setInLobby(false);
/* 168:154 */         joinGame(bro.getPlayer());
/* 169:155 */         bro.getPlayer().setAllowFlight(true);
/* 170:156 */         bro.setReason(CraftBrother.respawnreason.TOGAME);
/* 171:157 */         bro.heal();
/* 172:    */       }
/* 173:159 */       setInLobby(false);
/* 174:160 */       arenaStats();
/* 175:    */     }
/* 176:    */   }
/* 177:    */   
/* 178:    */   public void spectate(Player bro)
/* 179:    */   {
/* 180:165 */     Location sp1 = this.map.getSp1();
/* 181:166 */     Location spec = this.map.getSpec();
/* 182:167 */     if (spec != null)
/* 183:    */     {
/* 184:169 */       SCBGameManager.getInstance().getCraftBrother(bro).setSpectating(true);
/* 185:170 */       bro.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 50000, 2));
/* 186:171 */       bro.teleport(spec);
/* 187:    */     }
/* 188:    */     else
/* 189:    */     {
/* 190:175 */       SCBGameManager.getInstance().getCraftBrother(bro).setSpectating(true);
/* 191:176 */       bro.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 50000, 2));
/* 192:177 */       bro.teleport(sp1);
/* 193:    */     }
/* 194:    */   }
/* 195:    */   
/* 196:    */   @SuppressWarnings("deprecation")
public void arenaStats()
/* 197:    */   {
/* 198:182 */     Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
/* 199:183 */     CraftBrother[] cb = new CraftBrother[this.ingame.size()];
/* 200:184 */     cb = this.ingame.values().toArray(new CraftBrother[0]);
/* 201:185 */     Objective o = board.registerNewObjective("lives", "dummy");
/* 202:186 */     o.setDisplaySlot(DisplaySlot.SIDEBAR);
/* 203:187 */     o.setDisplayName(this.name);
/* 204:    */     
/* 205:189 */     Score[] scores = new Score[cb.length];
/* 206:190 */     for (int i = 0; i < cb.length; i++)
/* 207:    */     {
/* 208:192 */       scores[i] = o.getScore(cb[i].getPlayer());
/* 209:193 */       scores[i].setScore(cb[i].getLivesLeft());
/* 210:194 */       cb[i].getPlayer().setScoreboard(board);
/* 211:    */     }
/* 212:    */   }
/* 213:    */   
/* 214:    */   public void stopGame()
/* 215:    */   {
/* 216:199 */     List<CraftBrother> bros = new ArrayList<CraftBrother>(this.ingameinstance.values());
/* 217:200 */     for (CraftBrother bro : bros) {
/* 218:202 */       if ((bro != null) && (bro.getPlayer() != null))
/* 219:    */       {
/* 220:204 */         bro.getPlayer().sendMessage(ChatColor.RED + "This game has been stopped by an admin");
/* 221:205 */         bro.heal();
/* 222:206 */         bro.getPlayer().setAllowFlight(false);
/* 223:207 */         bro.getPlayer().teleport(SCBGameManager.getInstance().getMainLobby());
/* 224:208 */         GemScoreboard.addPlayer(bro.getPlayer());
/* 225:209 */         InventoryConfig.giveItems(bro.getPlayer());
/* 226:210 */         SCBGameManager.getInstance().removeCraftBrother(bro.getPlayer().getName());
/* 227:211 */         GemScoreboard.addPlayer(bro.getPlayer());
/* 228:    */       }
/* 229:    */     }
/* 230:214 */     if (this.TaskId > 0)
/* 231:    */     {
/* 232:216 */       Bukkit.getServer().getScheduler().cancelTask(this.TaskId);
/* 233:217 */       this.i = 30;
/* 234:    */     }
/* 235:219 */     this.ingameinstance.clear();
/* 236:220 */     this.ingame.clear();
/* 237:221 */     setInLobby(true);
/* 238:    */   }
/* 239:    */   
/* 240:    */   public void disableArena()
/* 241:    */   {
/* 242:225 */     if (this.plugin.getConfig().isBoolean("map." + this.name + ".disabled"))
/* 243:    */     {
/* 244:227 */       this.plugin.getConfig().set("map." + this.name + ".disabled", Boolean.valueOf(true));
/* 245:228 */       this.isDisabled = true;
/* 246:229 */       this.plugin.saveConfig();
/* 247:    */     }
/* 248:    */   }
/* 249:    */   
/* 250:    */   public void enableArena()
/* 251:    */   {
/* 252:235 */     if (this.plugin.getConfig().isBoolean("map." + this.name + ".disabled"))
/* 253:    */     {
/* 254:237 */       this.plugin.getConfig().set("map." + this.name + ".disabled", Boolean.valueOf(false));
/* 255:238 */       this.isDisabled = false;
/* 256:239 */       this.plugin.saveConfig();
/* 257:    */     }
/* 258:    */   }
/* 259:    */   
/* 260:    */   public void winGame(CraftBrother bro)
/* 261:    */   {
/* 262:245 */     Player p = bro.getPlayer();
/* 263:246 */     String s = p.getName();
/* 264:    */     
/* 265:248 */     Bukkit.broadcastMessage(p.getName() + ChatColor.GREEN + " has just won on " + ChatColor.YELLOW + getName() + ChatColor.GREEN + "!");
/* 266:249 */     PlayerStats.setPlayerGems(s, 2, PlayerStats.getPlayerRank(s), false);
/* 267:250 */     PlayerStats.setPlayerWins(s, 1);
/* 268:251 */     PlayerStats.setPlayerKills(s, bro.getKills());
/* 269:252 */     PlayerStats.setPlayerDeaths(s, bro.getDeaths());
/* 270:    */     
/* 271:254 */     ClassUtils.clearInventory(p);
/* 272:255 */     bro.heal();
/* 273:256 */     p.teleport(SCBGameManager.getInstance().getMainLobby());
/* 274:257 */     InventoryConfig.giveItems(p);
/* 275:258 */     SCBGameManager.getInstance().removeCraftBrother(s);
/* 276:259 */     for (CraftBrother cb : this.ingameinstance.values()) {
/* 277:261 */       if (cb != null)
/* 278:    */       {
/* 279:263 */         cb.getPlayer().teleport(SCBGameManager.getInstance().getMainLobby());
/* 280:264 */         SCBGameManager.getInstance().removeCraftBrother(bro.getPlayer().getName());
/* 281:    */       }
/* 282:    */     }
/* 283:267 */     for (CraftBrother cb : this.ingame.values()) {
/* 284:269 */       if (cb != null)
/* 285:    */       {
/* 286:271 */         cb.getPlayer().teleport(SCBGameManager.getInstance().getMainLobby());
/* 287:272 */         SCBGameManager.getInstance().removeCraftBrother(bro.getPlayer().getName());
/* 288:    */       }
/* 289:    */     }
/* 290:275 */     this.ingameinstance.clear();
/* 291:276 */     this.ingame.clear();
/* 292:277 */     setInLobby(true);
/* 293:278 */     GemScoreboard.addPlayer(bro.getPlayer());
/* 294:    */   }
/* 295:    */   
/* 296:    */   public int getNumberIngameInstance()
/* 297:    */   {
/* 298:282 */     return this.ingameinstance.size();
/* 299:    */   }
/* 300:    */   
/* 301:    */   public int getNumberIngame()
/* 302:    */   {
/* 303:286 */     return this.ingame.size();
/* 304:    */   }
/* 305:    */   
/* 306:    */   public void broadcast(String message)
/* 307:    */   {
/* 308:291 */     for (CraftBrother bro : this.ingame.values()) {
/* 309:293 */       if ((bro != null) && 
/* 310:294 */         (bro.getPlayer() != null)) {
/* 311:295 */         bro.getPlayer().sendMessage(message);
/* 312:    */       }
/* 313:    */     }
/* 314:    */   }
/* 315:    */   
/* 316:    */   public boolean isInLobby()
/* 317:    */   {
/* 318:300 */     return this.inLobby;
/* 319:    */   }
/* 320:    */   
/* 321:    */   public boolean isDisabled()
/* 322:    */   {
/* 323:304 */     return this.isDisabled;
/* 324:    */   }
/* 325:    */   
/* 326:    */   public boolean isInGame()
/* 327:    */   {
/* 328:308 */     return !this.inLobby;
/* 329:    */   }
/* 330:    */   
/* 331:    */   public void setInLobby(boolean b)
/* 332:    */   {
/* 333:312 */     this.inLobby = b;
/* 334:    */   }
/* 335:    */   
/* 336:    */   public int getAlive()
/* 337:    */   {
/* 338:316 */     return getNumberIngame();
/* 339:    */   }
/* 340:    */   
/* 341:    */   public int getDead()
/* 342:    */   {
/* 343:320 */     return 4 - getNumberIngame();
/* 344:    */   }
/* 345:    */   
/* 346:    */   public int getI()
/* 347:    */   {
/* 348:323 */     return this.i;
/* 349:    */   }
/* 350:    */   
/* 351:    */   public void playerEliminated(CraftBrother killed)
/* 352:    */   {
/* 353:328 */     if (this.ingame.containsKey(killed.getPlayer().getName()))
/* 354:    */     {
/* 355:331 */       this.ingame.remove(killed.getPlayer().getName());
/* 356:332 */       this.ingameinstance.remove(killed.getPlayer().getName());
/* 357:333 */       broadcast(killed.getPlayer().getName() + ChatColor.RED + " has lost!");
/* 358:334 */       PlayerStats.setPlayerKills(killed.getPlayer().getName(), killed.getKills());
/* 359:335 */       PlayerStats.setPlayerDeaths(killed.getPlayer().getName(), killed.getDeaths());
/* 360:336 */       if (this.ingame.size() == 1)
/* 361:    */       {
/* 362:338 */         killed.setReason(CraftBrother.respawnreason.TOMAIN);
/* 363:339 */         PlayerStats.setPlayerGems(killed.getPlayer().getName(), 1, PlayerStats.getPlayerRank(killed.getPlayer().getName()), false);
/* 364:    */       }
/* 365:    */       else
/* 366:    */       {
/* 367:343 */         killed.setReason(CraftBrother.respawnreason.TOLOBBY);
/* 368:    */       }
/* 369:345 */       GemScoreboard.addPlayer(killed.getPlayer());
/* 370:346 */       arenaStats();
/* 371:347 */       checkWin();
/* 372:348 */       SCBGameManager.getInstance().removeCraftBrother(killed.getPlayer().getName());
/* 373:    */     }
/* 374:    */   }
/* 375:    */   
/* 376:    */   public void startLobbyCountdown()
/* 377:    */   {
/* 378:354 */     if ((isInLobby()) && (this.i == 30))
/* 379:    */     {
/* 380:356 */       BukkitRunnable br = new BukkitRunnable()
/* 381:    */       {
/* 382:    */         public void run()
/* 383:    */         {
/* 384:361 */           SCBGame.this.TaskId = getTaskId();
/* 385:362 */           if (SCBGame.this.i == 30)
/* 386:    */           {
/* 387:364 */             SCBGame.this.broadcast(ChatColor.GREEN + "The game will begin in " + ChatColor.YELLOW + "30 seconds" + ChatColor.GREEN + "!");
/* 388:365 */             for (CraftBrother bro : SCBGame.this.ingameinstance.values())
/* 389:    */             {
/* 390:367 */               bro.getPlayer().setLevel(SCBGame.this.i);
/* 391:368 */               SCBGame.this.i -= 1;
/* 392:    */             }
/* 393:    */           }
/* 394:373 */           else if (SCBGame.this.i > 0)
/* 395:    */           {
/* 396:375 */             SCBGame.this.i -= 1;
/* 397:376 */             for (CraftBrother bro : SCBGame.this.ingameinstance.values()) {
/* 398:378 */               bro.getPlayer().setLevel(SCBGame.this.i);
/* 399:    */             }
/* 400:    */           }
/* 401:    */           else
/* 402:    */           {
/* 403:383 */             SCBGame.this.startGame();
/* 404:384 */             cancel();
/* 405:385 */             SCBGame.this.TaskId = 0;
/* 406:386 */             SCBGame.this.i = 30;
/* 407:387 */             for (CraftBrother bro : SCBGame.this.ingameinstance.values()) {
/* 408:389 */               bro.getPlayer().setLevel(0);
/* 409:    */             }
/* 410:    */           }
/* 411:    */         }
/* 412:394 */       };
/* 413:395 */       br.runTaskTimer(this.plugin, 0L, 20L);
/* 414:    */     }
/* 415:    */   }
/* 416:    */
 }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.SCBGame
 * JD-Core Version:    0.7.0.1
 */