/*   1:    */ package io.github.toxicbyte.supercraftbrothers.menus;
/*   2:    */ 
/*   3:    */ import io.github.toxicbyte.supercraftbrothers.listeners.PlayerInteractListener;

/*   4:    */ import java.util.Arrays;
/*   6:    */ import java.util.HashMap;

/*   7:    */ import org.bukkit.Bukkit;
/*  10:    */ import org.bukkit.entity.Player;
/*  11:    */ import org.bukkit.event.EventHandler;
/*  12:    */ import org.bukkit.event.EventPriority;
/*  13:    */ import org.bukkit.event.Listener;
/*  14:    */ import org.bukkit.event.inventory.InventoryClickEvent;
/*  15:    */ import org.bukkit.event.inventory.InventoryCloseEvent;
/*  16:    */ import org.bukkit.inventory.Inventory;
/*  17:    */ import org.bukkit.inventory.ItemStack;
/*  18:    */ import org.bukkit.inventory.meta.ItemMeta;
/*  19:    */ import org.bukkit.plugin.Plugin;
/*  22:    */ 
/*  23:    */ public class GemClassShopIM
/*  24:    */   implements Listener
/*  25:    */ {
/*  26:    */   private String name;
/*  27:    */   private int size;
/*  28:    */   private Plugin plugin;
/*  29:    */   private String[] optionNames;
/*  30:    */   private ItemStack[] optionIcons;
/*  31: 27 */   private static HashMap<String, Inventory> inventories = new HashMap<String, Inventory>();
/*  32:    */   
/*  33:    */   public GemClassShopIM(String name, int size, Plugin plugin)
/*  34:    */   {
/*  35: 30 */     this.name = name;
/*  36: 31 */     this.size = size;
/*  37: 32 */     this.plugin = plugin;
/*  38: 33 */     this.optionNames = new String[size];
/*  39: 34 */     this.optionIcons = new ItemStack[size];
/*  40: 35 */     plugin.getServer().getPluginManager().registerEvents(this, plugin);
/*  41:    */   }
/*  42:    */   
/*  43:    */   public GemClassShopIM setOption(int position, ItemStack icon, String name, String... info)
/*  44:    */   {
/*  45: 39 */     this.optionNames[position] = name;
/*  46: 40 */     this.optionIcons[position] = setItemNameAndLore(icon, name, info);
/*  47: 41 */     return this;
/*  48:    */   }
/*  49:    */   
/*  50:    */   public void open(Player player)
/*  51:    */   {
/*  52: 46 */     if (inventories.values().contains(player.getName()))
/*  53:    */     {
/*  54: 48 */       Inventory inventory = (Inventory)inventories.get(player.getName());
/*  55: 49 */       for (int y = 0; y < this.optionIcons.length; y++) {
/*  56: 50 */         if (this.optionIcons[y] != null) {
/*  57: 51 */           inventory.setItem(y, this.optionIcons[y]);
/*  58:    */         }
/*  59:    */       }
/*  60: 54 */       player.openInventory(inventory);
/*  61: 55 */       inventories.put(player.getName(), inventory);
/*  62:    */     }
/*  63:    */     else
/*  64:    */     {
/*  65: 59 */       Inventory inventory = Bukkit.createInventory(player, this.size, this.name);
/*  66: 60 */       for (int y = 0; y < this.optionIcons.length; y++) {
/*  67: 61 */         if (this.optionIcons[y] != null) {
/*  68: 62 */           inventory.setItem(y, this.optionIcons[y]);
/*  69:    */         }
/*  70:    */       }
/*  71: 65 */       player.openInventory(inventory);
/*  72: 66 */       inventories.put(player.getName(), inventory);
/*  73:    */     }
/*  74:    */   }
/*  75:    */   
/*  76:    */   @EventHandler(priority=EventPriority.MONITOR)
/*  77:    */   void onInventoryClick(InventoryClickEvent event)
/*  78:    */   {
/*  79: 73 */     if (inventories.get(event.getWhoClicked().getName()) != null)
/*  80:    */     {
/*  81: 75 */       event.setCancelled(true);
/*  82: 76 */       int slot = event.getRawSlot();
/*  83: 77 */       if ((slot >= 0) && (slot < this.size) && (this.optionNames[slot] != null))
/*  84:    */       {
/*  85: 79 */         GemClassShop gs = (GemClassShop)PlayerInteractListener.gcsmenus.get(event.getWhoClicked().getName());
/*  86: 80 */         gs.onOptionClick(event.getInventory().getItem(slot).getItemMeta().getDisplayName(), slot);
/*  87: 81 */         inventories.remove(event.getWhoClicked().getName());
/*  88: 82 */         final Player p = (Player)event.getWhoClicked();
/*  89: 83 */         Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable()
/*  90:    */         {
/*  91:    */           public void run()
/*  92:    */           {
/*  93: 87 */             p.closeInventory();
/*  94:    */           }
/*  95: 89 */         }, 1L);
/*  96:    */       }
/*  97:    */     }
/*  98:    */   }
/*  99:    */   
/* 100:    */   @EventHandler(priority=EventPriority.MONITOR)
/* 101:    */   void onInventoryClose(InventoryCloseEvent event)
/* 102:    */   {
/* 103: 96 */     if (inventories.get(event.getPlayer().getName()) != null) {
/* 104: 98 */       inventories.remove(event.getPlayer().getName());
/* 105:    */     }
/* 106:    */   }
/* 107:    */   
/* 108:    */   static void removeFromList(Player p)
/* 109:    */   {
/* 110:103 */     if (inventories.get(p.getName()) != null) {
/* 111:105 */       inventories.remove(p.getName());
/* 112:    */     }
/* 113:    */   }
/* 114:    */   
/* 115:    */   private ItemStack setItemNameAndLore(ItemStack item, String name, String[] lore)
/* 116:    */   {
/* 117:109 */     ItemMeta im = item.getItemMeta();
/* 118:110 */     im.setDisplayName(name);
/* 119:111 */     im.setLore(Arrays.asList(lore));
/* 120:112 */     item.setItemMeta(im);
/* 121:113 */     return item;
/* 122:    */   }
/* 123:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.menus.GemClassShopIM
 * JD-Core Version:    0.7.0.1
 */