/*  1:   */ package io.github.toxicbyte.supercraftbrothers.menus;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.PlayerStats;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.SuperCraftBrothers;

/*  7:   */ import org.bukkit.ChatColor;
/*  8:   */ import org.bukkit.Material;
/*  9:   */ import org.bukkit.entity.Player;
/* 10:   */ import org.bukkit.inventory.ItemStack;
/* 11:   */ 
/* 12:   */ public class GemClassMenu
/* 13:   */ {
/* 14:   */   private GemClassMenuIM gemclass;
/* 15:   */   private Player p;
/* 16:   */   private CraftBrother bro;
/* 17:   */   
/* 18:   */   public GemClassMenu(SuperCraftBrothers scb, Player p)
/* 19:   */   {
/* 20:21 */     this.p = p;
/* 21:22 */     this.gemclass = new GemClassMenuIM("§2Gem Characters", 27, scb);
/* 22:   */   }
/* 23:   */   
/* 24:25 */   public String[] descs = new String[7];
/* 25:26 */   public static int[] prices = { 100, 50, 100, 100, 100, 200, 50 };
/* 26:27 */   public static String[] classes = { "ButterBro", "Chicken", "Notch", "Ocelot", "Witch", "WitherSkeleton", "Tnt" };
/* 27:28 */   public static ItemStack[] items = {
/* 28:29 */     new ItemStack(Material.GOLD_INGOT, 1), 
/* 29:30 */     new ItemStack(Material.EGG, 1), 
/* 30:31 */     new ItemStack(Material.GRASS, 1), 
/* 31:32 */     new ItemStack(Material.MONSTER_EGG, 1), 
/* 32:33 */     new ItemStack(Material.POTION, 1), 
/* 33:34 */     new ItemStack(Material.LEATHER_CHESTPLATE, 1), 
/* 34:35 */     new ItemStack(Material.TNT, 1) };
/* 35:   */   
/* 36:   */   public void openMenu()
/* 37:   */   {
/* 38:39 */     this.bro = SCBGameManager.getInstance().getCraftBrother(this.p);
/* 39:40 */     setupShop(this.p);
/* 40:41 */     for (int i = 0; i < classes.length; i++) {
/* 41:43 */       this.gemclass.setOption(i, items[i], classes[i], new String[] { this.descs[i] });
/* 42:   */     }
/* 43:45 */     this.gemclass.open(this.p);
/* 44:   */   }
/* 45:   */   
/* 46:   */   public void setupShop(Player p)
/* 47:   */   {
/* 48:50 */     for (int i = 0; i < classes.length; i++) {
/* 49:52 */       if (PlayerStats.getPlayerClass(p.getName(), classes[i].toLowerCase())) {
/* 50:53 */         this.descs[i] = "§aPurchased!";
/* 51:   */       } else {
/* 52:55 */         this.descs[i] = "§cYou have not yet purchased this class";
/* 53:   */       }
/* 54:   */     }
/* 55:   */   }
/* 56:   */   
/* 57:   */   public void onOptionClick(String c)
/* 58:   */   {
/* 59:61 */     if (this.p.hasPermission("scb.class." + c.toLowerCase())) {
/* 60:63 */       this.bro.setCurrentClass(c);
/* 61:67 */     } else if (PlayerStats.getPlayerClass(this.bro.getPlayer().getName(), c.toLowerCase())) {
/* 62:68 */       this.bro.setCurrentClass(c);
/* 63:   */     } else {
/* 64:70 */       this.bro.getPlayer().sendMessage(ChatColor.RED + "You do not own this class, buy it in the Gem Class Shop");
/* 65:   */     }
/* 66:72 */     GemClassMenuIM.removeFromList(this.p);
/* 67:   */   }
/* 68:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.menus.GemClassMenu
 * JD-Core Version:    0.7.0.1
 */