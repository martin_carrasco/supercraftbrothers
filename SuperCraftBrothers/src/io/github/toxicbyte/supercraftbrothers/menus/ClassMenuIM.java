/*  1:   */ package io.github.toxicbyte.supercraftbrothers.menus;
/*  2:   */ 
/*  3:   */ import java.util.Arrays;
/*  4:   */ import org.bukkit.Bukkit;
/*  6:   */ import org.bukkit.entity.Player;
/*  7:   */ import org.bukkit.event.EventHandler;
/*  8:   */ import org.bukkit.event.EventPriority;
/*  9:   */ import org.bukkit.event.Listener;
/* 10:   */ import org.bukkit.event.inventory.InventoryClickEvent;
/* 11:   */ import org.bukkit.inventory.Inventory;
/* 12:   */ import org.bukkit.inventory.ItemStack;
/* 13:   */ import org.bukkit.inventory.meta.ItemMeta;
/* 14:   */ import org.bukkit.plugin.Plugin;
/* 17:   */ 
/* 18:   */ public class ClassMenuIM
/* 19:   */   implements Listener
/* 20:   */ {
/* 21:   */   private String name;
/* 22:   */   private int size;
/* 23:   */   private Plugin plugin;
/* 24:   */   private String[] optionNames;
/* 25:   */   private ItemStack[] optionIcons;
/* 26:   */   private static Inventory inventory;
/* 27:   */   
/* 28:   */   public ClassMenuIM(String name, int size, Plugin plugin)
/* 29:   */   {
/* 30:26 */     this.name = name;
/* 31:27 */     this.size = size;
/* 32:28 */     this.plugin = plugin;
/* 33:29 */     this.optionNames = new String[size];
/* 34:30 */     this.optionIcons = new ItemStack[size];
/* 35:31 */     plugin.getServer().getPluginManager().registerEvents(this, plugin);
/* 36:   */   }
/* 37:   */   
/* 38:   */   public ClassMenuIM setOption(int position, ItemStack icon, String name, String... info)
/* 39:   */   {
/* 40:35 */     this.optionNames[position] = name;
/* 41:36 */     this.optionIcons[position] = setItemNameAndLore(icon, name, info);
/* 42:37 */     return this;
/* 43:   */   }
/* 44:   */   
/* 45:   */   public void open(Player player)
/* 46:   */   {
/* 47:42 */     if (inventory != null)
/* 48:   */     {
/* 49:44 */       Inventory i = inventory;
/* 50:45 */       player.openInventory(i);
/* 51:46 */       return;
/* 52:   */     }
/* 53:50 */     Inventory i = Bukkit.createInventory(player, this.size, this.name);
/* 54:51 */     for (int y = 0; y < this.optionIcons.length; y++) {
/* 55:52 */       if (this.optionIcons[y] != null) {
/* 56:53 */         i.setItem(y, this.optionIcons[y]);
/* 57:   */       }
/* 58:   */     }
/* 59:56 */     player.openInventory(i);
/* 60:57 */     inventory = i;
/* 61:   */   }
/* 62:   */   
/* 63:   */   @EventHandler(priority=EventPriority.MONITOR)
/* 64:   */   void onInventoryClick(InventoryClickEvent event)
/* 65:   */   {
/* 66:64 */     if (event.getInventory().getTitle().equals(this.name))
/* 67:   */     {
/* 68:66 */       event.setCancelled(true);
/* 69:67 */       int slot = event.getRawSlot();
/* 70:68 */       if ((slot >= 0) && (slot < this.size) && (this.optionNames[slot] != null))
/* 71:   */       {
/* 72:70 */         ClassMenu.onOptionClick((Player)event.getWhoClicked(), slot, event.getInventory().getItem(slot).getItemMeta().getDisplayName());
/* 73:71 */         final Player p = (Player)event.getWhoClicked();
/* 74:72 */         Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable()
/* 75:   */         {
/* 76:   */           public void run()
/* 77:   */           {
/* 78:76 */             p.closeInventory();
/* 79:   */           }
/* 80:78 */         }, 1L);
/* 81:   */       }
/* 82:   */     }
/* 83:   */   }
/* 84:   */   
/* 85:   */   private ItemStack setItemNameAndLore(ItemStack item, String name, String[] lore)
/* 86:   */   {
/* 87:84 */     ItemMeta im = item.getItemMeta();
/* 88:85 */     im.setDisplayName(name);
/* 89:86 */     im.setLore(Arrays.asList(lore));
/* 90:87 */     item.setItemMeta(im);
/* 91:88 */     return item;
/* 92:   */   }
/* 93:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.menus.ClassMenuIM
 * JD-Core Version:    0.7.0.1
 */