/*   1:    */ package io.github.toxicbyte.supercraftbrothers.menus;
/*   2:    */ 
/*   3:    */ /*   6:    */ import io.github.toxicbyte.supercraftbrothers.GemScoreboard;
import io.github.toxicbyte.supercraftbrothers.PlayerStats;
import io.github.toxicbyte.supercraftbrothers.SuperCraftBrothers;
import io.github.toxicbyte.supercraftbrothers.utilities.ClassUtils;

/*   7:    */ import java.io.File;
/*   8:    */ import java.io.IOException;

/*   9:    */ import org.bukkit.ChatColor;
/*  10:    */ import org.bukkit.Material;
/*  11:    */ import org.bukkit.configuration.file.FileConfiguration;
/*  12:    */ import org.bukkit.configuration.file.YamlConfiguration;
/*  13:    */ import org.bukkit.entity.Player;
/*  14:    */ import org.bukkit.inventory.ItemStack;
/*  15:    */ 
/*  16:    */ public class GemClassShop
/*  17:    */ {
/*  18:    */   public GemClassShopIM gemclassshop;
/*  19: 21 */   public Player p = null;
/*  20:    */   
/*  21:    */   public GemClassShop(SuperCraftBrothers scb, Player player)
/*  22:    */   {
/*  23: 25 */     this.gemclassshop = new GemClassShopIM("§2Character Gem Shop", 27, scb);
/*  24: 26 */     this.p = player;
/*  25:    */   }
/*  26:    */   
/*  27: 29 */   public String[] descs = new String[7];
/*  28: 30 */   public static int[] prices = { 100, 50, 100, 100, 100, 200, 50 };
/*  29: 31 */   public static String[] classes = { "ButterBro", "Chicken", "Notch", "Ocelot", "Witch", "WitherSkeleton", "Tnt" };
/*  30: 32 */   public static ItemStack[] items = {
/*  31: 33 */     new ItemStack(Material.GOLD_INGOT, 1), 
/*  32: 34 */     new ItemStack(Material.EGG, 1), 
/*  33: 35 */     new ItemStack(Material.GRASS, 1), 
/*  34: 36 */     new ItemStack(Material.MONSTER_EGG, 1), 
/*  35: 37 */     new ItemStack(Material.POTION, 1), 
/*  36: 38 */     new ItemStack(Material.LEATHER_CHESTPLATE, 1), 
/*  37: 39 */     new ItemStack(Material.TNT, 1) };
/*  38:    */   
/*  39:    */   public void openMenu()
/*  40:    */   {
/*  41: 44 */     setupShop(this.p);
/*  42: 45 */     for (int i = 0; i < classes.length; i++) {
/*  43: 47 */       this.gemclassshop.setOption(i, items[i], classes[i], new String[] { this.descs[i] });
/*  44:    */     }
/*  45: 49 */     this.gemclassshop.open(this.p);
/*  46:    */   }
/*  47:    */   
/*  48:    */   public void onOptionClick(String c, int s)
/*  49:    */   {
/*  50: 54 */     if (this.p.hasPermission("scb.class." + c.toLowerCase())) {
/*  51: 56 */       buyClass(this.p, c.toLowerCase(), prices[s]);
/*  52: 60 */     } else if (!PlayerStats.getPlayerClass(this.p.getName(), c.toLowerCase())) {
/*  53: 62 */       if (canBuyClass(this.p, c.toLowerCase(), prices[s]))
/*  54:    */       {
/*  55: 64 */         buyClass(this.p, c.toLowerCase(), prices[s]);
/*  56: 65 */         GemScoreboard.addPlayer(this.p);
/*  57:    */       }
/*  58:    */     }
/*  59: 69 */     GemClassShopIM.removeFromList(this.p);
/*  60:    */   }
/*  61:    */   
/*  62:    */   public void setupShop(Player p)
/*  63:    */   {
/*  64: 74 */     for (int i = 0; i < classes.length; i++) {
/*  65: 76 */       if (PlayerStats.getPlayerClass(p.getName(), classes[i].toLowerCase())) {
/*  66: 77 */         this.descs[i] = "§aPurchased!";
/*  67:    */       } else {
/*  68: 79 */         this.descs[i] = ("§6Price: §a" + prices[i] + " gems");
/*  69:    */       }
/*  70:    */     }
/*  71:    */   }
/*  72:    */   
/*  73:    */   public boolean canBuyClass(Player p, String c, int g)
/*  74:    */   {
/*  75: 84 */     File pf = new File("plugins" + File.separator + "SuperCraftBrothers" + File.separator + "users" + File.separator + p.getName() + ".yml");
/*  76: 85 */     FileConfiguration pc = YamlConfiguration.loadConfiguration(pf);
/*  77: 86 */     int gems = pc.getInt("gems");
/*  78: 87 */     if (gems >= g) {
/*  79: 89 */       return true;
/*  80:    */     }
/*  81: 93 */     p.sendMessage(ChatColor.RED + "You do not have enough Gems to unlock this character");
/*  82: 94 */     return false;
/*  83:    */   }
/*  84:    */   
/*  85:    */   public void buyClass(Player p, String c, int g)
/*  86:    */   {
/*  87: 99 */     File pf = new File("plugins" + File.separator + "SuperCraftBrothers" + File.separator + "users" + File.separator + p.getName() + ".yml");
/*  88:100 */     FileConfiguration pc = YamlConfiguration.loadConfiguration(pf);
/*  89:101 */     int gems = pc.getInt("gems");
/*  90:102 */     int ng = gems - g;
/*  91:103 */     pc.set("gems", Integer.valueOf(ng));
/*  92:104 */     pc.set("unlocked_characters." + c, Boolean.valueOf(true));
/*  93:    */     try
/*  94:    */     {
/*  95:105 */       pc.save(pf);
/*  96:    */     }
/*  97:    */     catch (IOException e)
/*  98:    */     {
/*  99:105 */       e.printStackTrace();
/* 100:    */     }
/* 101:106 */     p.sendMessage(ChatColor.GREEN + "You have just bought the " + ClassUtils.getName(c));
/* 102:    */   }
/* 103:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.menus.GemClassShop
 * JD-Core Version:    0.7.0.1
 */