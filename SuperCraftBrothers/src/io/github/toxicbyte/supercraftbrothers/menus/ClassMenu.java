/*  1:   */ package io.github.toxicbyte.supercraftbrothers.menus;
/*  4:   */ import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.SuperCraftBrothers;

/*  6:   */ import org.bukkit.Material;
/*  7:   */ import org.bukkit.entity.Player;
/*  8:   */ import org.bukkit.inventory.ItemStack;
/*  9:   */ 
/* 10:   */ public class ClassMenu
/* 11:   */ {
/* 12:   */   public static ClassMenuIM menucharacter;
/* 13:14 */   public static String[] classes = {
/* 14:15 */     "Blaze", 
/* 15:16 */     "Cactus", 
/* 16:17 */     "Creeper", 
/* 17:18 */     "Enderman", 
/* 18:19 */     "Skeleton", 
/* 19:20 */     "Silverfish", 
/* 20:21 */     "Spider", 
/* 21:22 */     "Squid", 
/* 22:23 */     "Wither", 
/* 23:24 */     "Zombie", 
/* 24:25 */     "ZombiePigman", 
/* 25:26 */     "Dweller", 
/* 26:27 */     "Pig", 
/* 27:28 */     "Villager", 
/* 28:29 */     "Jeb" };
/* 29:32 */   public static ItemStack[] items = {
/* 30:33 */     new ItemStack(Material.BLAZE_ROD), 
/* 31:34 */     new ItemStack(Material.CACTUS), 
/* 32:35 */     new ItemStack(Material.SKULL_ITEM, 1), 
/* 33:36 */     new ItemStack(Material.ENDER_PEARL), 
/* 34:37 */     new ItemStack(Material.SKULL_ITEM), 
/* 35:38 */     new ItemStack(Material.FISHING_ROD), 
/* 36:39 */     new ItemStack(Material.WEB), 
/* 37:40 */     new ItemStack(Material.INK_SACK), 
/* 38:41 */     new ItemStack(Material.SKULL_ITEM, 1), 
/* 39:42 */     new ItemStack(Material.SKULL_ITEM, 1), 
/* 40:43 */     new ItemStack(Material.GOLD_CHESTPLATE), 
/* 41:44 */     new ItemStack(Material.BONE), 
/* 42:45 */     new ItemStack(Material.PORK), 
/* 43:46 */     new ItemStack(Material.WOOD_STEP), 
/* 44:47 */     new ItemStack(Material.WOOL) };
/* 45:50 */   public static String[][] descs = {
/* 46:51 */     { "Equipped with an", "incinndary rod and bow" }, 
/* 47:52 */     { "Will cause damage", "to melee attacker" }, 
/* 48:53 */     { "Equipped with some TNT and", "lots of small bombs" }, 
/* 49:54 */     { "Able to teleport by", "throwing enderpearls" }, 
/* 50:55 */     { "Equipped with extra", "knockback bow" }, 
/* 51:56 */     { "Equipped with five", "silverfish spawn eggs" }, 
/* 52:57 */     { "Fast and 30% of", "poisoning on", "melee attacks" }, 
/* 53:58 */     { "With 25% chance of causing", "blindness and/or slowness", "on melee attacks" }, 
/* 54:59 */     { "Equipped with an Wither Skull Bow and", "Wither potions" }, 
/* 55:60 */     { "Equipped with a", "shovel that", "causes extra damage" }, 
/* 56:61 */     { "Equipped with two", "Zombie Pigman eggs" }, 
/* 57:62 */     { "Able to throw it's melee weapon", "to cause an explosion" }, 
/* 58:63 */     { "Will get extra", "speed when hit" }, 
/* 59:64 */     { "Throws rotten potatoes that cause", "slowness to nearby players" }, 
/* 60:65 */     { "Push player very", "far away" } };
/* 61:   */   
/* 62:   */   public ClassMenu(SuperCraftBrothers scb)
/* 63:   */   {
/* 64:69 */     menucharacter = new ClassMenuIM("§l§nIncluded Characters", 27, scb);
/* 65:70 */     for (int i = 0; i < classes.length; i++) {
/* 66:72 */       menucharacter.setOption(i, items[i], classes[i], descs[i]);
/* 67:   */     }
/* 68:   */   }
/* 69:   */   
/* 70:   */   public static void openMenu(Player p)
/* 71:   */   {
/* 72:78 */     menucharacter.open(p);
/* 73:   */   }
/* 74:   */   
/* 75:   */   public static void onOptionClick(Player p, int s, String c)
/* 76:   */   {
/* 77:83 */     SCBGameManager.getInstance().getCraftBrother(p).setCurrentClass(c);
/* 78:   */   }
/* 79:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.menus.ClassMenu
 * JD-Core Version:    0.7.0.1
 */