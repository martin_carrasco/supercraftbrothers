/*  1:   */ package io.github.toxicbyte.supercraftbrothers.menus;
/*  2:   */ 
/*  3:   */ import io.github.toxicbyte.supercraftbrothers.CraftBrother;
import io.github.toxicbyte.supercraftbrothers.PlayerStats;
import io.github.toxicbyte.supercraftbrothers.SCBGameManager;
import io.github.toxicbyte.supercraftbrothers.SuperCraftBrothers;

/*  7:   */ import org.bukkit.ChatColor;
/*  8:   */ import org.bukkit.Material;
/*  9:   */ import org.bukkit.entity.Player;
/* 10:   */ import org.bukkit.inventory.ItemStack;
/* 11:   */ 
/* 12:   */ public class VIPClassMenu
/* 13:   */ {
/* 14:   */   private VIPClassMenuIM vipclass;
/* 15:   */   private Player p;
/* 16:   */   private CraftBrother bro;
/* 17:   */   
/* 18:   */   public VIPClassMenu(SuperCraftBrothers scb, Player p)
/* 19:   */   {
/* 20:21 */     this.p = p;
/* 21:22 */     this.vipclass = new VIPClassMenuIM("§2VIP Characters", 27, scb);
/* 22:   */   }
/* 23:   */   
/* 24:25 */   public String[] descs = new String[2];
/* 25:26 */   public String[] classes = { "Mollstam", "SethBling" };
/* 26:27 */   public ItemStack[] items = {
/* 27:28 */     new ItemStack(Material.MAP), 
/* 28:29 */     new ItemStack(Material.REDSTONE) };
/* 29:   */   
/* 30:   */   public void openMenu()
/* 31:   */   {
/* 32:33 */     this.bro = SCBGameManager.getInstance().getCraftBrother(this.p);
/* 33:34 */     setupShop(this.p);
/* 34:35 */     for (int i = 0; i < this.classes.length; i++) {
/* 35:37 */       this.vipclass.setOption(i, this.items[i], this.classes[i], new String[] { this.descs[i] });
/* 36:   */     }
/* 37:39 */     this.vipclass.open(this.p);
/* 38:   */   }
/* 39:   */   
/* 40:   */   public void setupShop(Player p)
/* 41:   */   {
/* 42:44 */     for (int i = 0; i < this.classes.length; i++) {
/* 43:46 */       if (PlayerStats.getPlayerRank(p.getName()) > 1) {
/* 44:47 */         this.descs[i] = "§aYou may use this class!";
/* 45:   */       } else {
/* 46:49 */         this.descs[i] = "§cPlease purchase VIP or PRO to use this class";
/* 47:   */       }
/* 48:   */     }
/* 49:   */   }
/* 50:   */   
/* 51:   */   public void onOptionClick(String c)
/* 52:   */   {
/* 53:55 */     if ((PlayerStats.getPlayerRank(this.p.getName()) > 1) || (this.p.hasPermission("scb.class." + c.toLowerCase()))) {
/* 54:57 */       this.bro.setCurrentClass(c);
/* 55:   */     } else {
/* 56:60 */       this.bro.getPlayer().sendMessage(ChatColor.RED + "Please purchase VIP or PRO to use this class");
/* 57:   */     }
/* 58:61 */     VIPClassMenuIM.removeFromList(this.p);
/* 59:   */   }
/* 60:   */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.menus.VIPClassMenu
 * JD-Core Version:    0.7.0.1
 */