/*   1:    */ package io.github.toxicbyte.supercraftbrothers.menus;
/*   2:    */ 
/*   3:    */ import io.github.toxicbyte.supercraftbrothers.listeners.PlayerInteractListener;

/*   4:    */ import java.util.Arrays;
/*   5:    */ import java.util.HashMap;

/*   6:    */ import org.bukkit.Bukkit;
/*   9:    */ import org.bukkit.entity.Player;
/*  10:    */ import org.bukkit.event.EventHandler;
/*  11:    */ import org.bukkit.event.EventPriority;
/*  12:    */ import org.bukkit.event.Listener;
/*  13:    */ import org.bukkit.event.inventory.InventoryClickEvent;
/*  14:    */ import org.bukkit.event.inventory.InventoryCloseEvent;
/*  15:    */ import org.bukkit.inventory.Inventory;
/*  16:    */ import org.bukkit.inventory.ItemStack;
/*  17:    */ import org.bukkit.inventory.meta.ItemMeta;
/*  18:    */ import org.bukkit.plugin.Plugin;
/*  21:    */ 
/*  22:    */ public class GemClassMenuIM
/*  23:    */   implements Listener
/*  24:    */ {
/*  25:    */   private String name;
/*  26:    */   private int size;
/*  27:    */   private Plugin plugin;
/*  28:    */   private String[] optionNames;
/*  29:    */   private ItemStack[] optionIcons;
/*  30: 27 */   private static HashMap<String, Inventory> inventories = new HashMap<String, Inventory>();
/*  31:    */   
/*  32:    */   public GemClassMenuIM(String name, int size, Plugin plugin)
/*  33:    */   {
/*  34: 30 */     this.name = name;
/*  35: 31 */     this.size = size;
/*  36: 32 */     this.plugin = plugin;
/*  37: 33 */     this.optionNames = new String[size];
/*  38: 34 */     this.optionIcons = new ItemStack[size];
/*  39: 35 */     plugin.getServer().getPluginManager().registerEvents(this, plugin);
/*  40:    */   }
/*  41:    */   
/*  42:    */   public GemClassMenuIM setOption(int position, ItemStack icon, String name, String... info)
/*  43:    */   {
/*  44: 39 */     this.optionNames[position] = name;
/*  45: 40 */     this.optionIcons[position] = setItemNameAndLore(icon, name, info);
/*  46: 41 */     return this;
/*  47:    */   }
/*  48:    */   
/*  49:    */   public void open(Player player)
/*  50:    */   {
/*  51: 46 */     if (inventories.get(player.getName()) == null)
/*  52:    */     {
/*  53: 48 */       Inventory inventory = Bukkit.createInventory(player, this.size, this.name);
/*  54: 49 */       for (int y = 0; y < this.optionIcons.length; y++) {
/*  55: 50 */         if (this.optionIcons[y] != null) {
/*  56: 51 */           inventory.setItem(y, this.optionIcons[y]);
/*  57:    */         }
/*  58:    */       }
/*  59: 54 */       player.openInventory(inventory);
/*  60: 55 */       inventories.put(player.getName(), inventory);
/*  61:    */     }
/*  62:    */     else
/*  63:    */     {
/*  64: 59 */       Inventory inventory = (Inventory)inventories.get(player.getName());
/*  65: 60 */       for (int y = 0; y < this.optionIcons.length; y++) {
/*  66: 61 */         if (this.optionIcons[y] != null) {
/*  67: 62 */           inventory.setItem(y, this.optionIcons[y]);
/*  68:    */         }
/*  69:    */       }
/*  70: 65 */       player.openInventory(inventory);
/*  71: 66 */       inventories.put(player.getName(), inventory);
/*  72:    */     }
/*  73:    */   }
/*  74:    */   
/*  75:    */   @EventHandler(priority=EventPriority.MONITOR)
/*  76:    */   void onInventoryClick(InventoryClickEvent event)
/*  77:    */   {
/*  78: 74 */     if (inventories.get(event.getWhoClicked().getName()) != null)
/*  79:    */     {
/*  80: 76 */       event.setCancelled(true);
/*  81: 77 */       int slot = event.getRawSlot();
/*  82: 78 */       if ((slot >= 0) && (slot < this.size) && (this.optionNames[slot] != null))
/*  83:    */       {
/*  84: 80 */         GemClassMenu gm = (GemClassMenu)PlayerInteractListener.gcmmenus.get(event.getWhoClicked().getName());
/*  85: 81 */         gm.onOptionClick(event.getInventory().getItem(slot).getItemMeta().getDisplayName());
/*  86: 82 */         inventories.remove(event.getWhoClicked().getName());
/*  87: 83 */         final Player p = (Player)event.getWhoClicked();
/*  88: 84 */         Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable()
/*  89:    */         {
/*  90:    */           public void run()
/*  91:    */           {
/*  92: 88 */             p.closeInventory();
/*  93:    */           }
/*  94: 90 */         }, 1L);
/*  95:    */       }
/*  96:    */     }
/*  97:    */   }
/*  98:    */   
/*  99:    */   @EventHandler(priority=EventPriority.MONITOR)
/* 100:    */   void onInventoryClose(InventoryCloseEvent event)
/* 101:    */   {
/* 102: 97 */     if (inventories.get(event.getPlayer().getName()) != null) {
/* 103: 99 */       inventories.remove(event.getPlayer().getName());
/* 104:    */     }
/* 105:    */   }
/* 106:    */   
/* 107:    */   static void removeFromList(Player p)
/* 108:    */   {
/* 109:104 */     if (inventories.get(p.getName()) != null) {
/* 110:106 */       inventories.remove(p.getName());
/* 111:    */     }
/* 112:    */   }
/* 113:    */   
/* 114:    */   private ItemStack setItemNameAndLore(ItemStack item, String name, String[] lore)
/* 115:    */   {
/* 116:110 */     ItemMeta im = item.getItemMeta();
/* 117:111 */     im.setDisplayName(name);
/* 118:112 */     im.setLore(Arrays.asList(lore));
/* 119:113 */     item.setItemMeta(im);
/* 120:114 */     return item;
/* 121:    */   }
/* 122:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.menus.GemClassMenuIM
 * JD-Core Version:    0.7.0.1
 */