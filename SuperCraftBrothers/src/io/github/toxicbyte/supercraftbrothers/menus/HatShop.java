/*   1:    */ package io.github.toxicbyte.supercraftbrothers.menus;
/*   2:    */ 
/*   3:    */ import io.github.toxicbyte.supercraftbrothers.GemScoreboard;
import io.github.toxicbyte.supercraftbrothers.PlayerStats;
import io.github.toxicbyte.supercraftbrothers.SuperCraftBrothers;

/*   6:    */ import org.bukkit.Bukkit;
/*   7:    */ import org.bukkit.ChatColor;
/*   8:    */ import org.bukkit.Material;
/*   9:    */ import org.bukkit.entity.Player;
/*  10:    */ import org.bukkit.inventory.ItemStack;
/*  12:    */ 
/*  13:    */ public class HatShop
/*  14:    */ {
/*  15:    */   public HatShopIM hatshop;
/*  16: 16 */   public Player p = null;
/*  17:    */   
/*  18:    */   public HatShop(SuperCraftBrothers scb, Player p)
/*  19:    */   {
/*  20: 20 */     this.hatshop = new HatShopIM("§2Hat Shop", 54, scb);
/*  21: 21 */     this.p = p;
/*  22:    */   }
/*  23:    */   
/*  24: 24 */   public String[] descs = new String[53];
/*  25: 25 */   public int[] prices = { 50, 50, 50, 50, 150, 75, 100, 50, 100 };
/*  26: 26 */   public ItemStack[] items = {
/*  27: 27 */     new ItemStack(Material.SKULL_ITEM, 1), 
/*  28: 28 */     new ItemStack(Material.SKULL_ITEM, 1), 
/*  29: 29 */     new ItemStack(Material.SKULL_ITEM, 1), 
/*  30: 30 */     new ItemStack(Material.SKULL_ITEM, 1), 
/*  31: 31 */     new ItemStack(Material.COMMAND, 1), 
/*  32: 32 */     new ItemStack(Material.TNT, 1), 
/*  33: 33 */     new ItemStack(Material.MOB_SPAWNER, 1), 
/*  34: 34 */     new ItemStack(Material.GLASS, 1), 
/*  35: 35 */     new ItemStack(Material.CACTUS, 1) };
/*  36: 37 */   public String[] classes = {
/*  37: 38 */     "Skeleton Hat", 
/*  38: 39 */     "WitherSkeleton Hat", 
/*  39: 40 */     "Zombie Hat", 
/*  40: 41 */     "Creeper Hat", 
/*  41: 42 */     "Commander Hat", 
/*  42: 43 */     "TNT Hat", 
/*  43: 44 */     "Spawner Hat", 
/*  44: 45 */     "Spaceman Hat", 
/*  45: 46 */     "Cactus Hat" };
/*  46:    */   
/*  47:    */   public void openMenu()
/*  48:    */   {
/*  49: 51 */     setupShop(this.p);
/*  50: 52 */     this.hatshop.setOption(53, new ItemStack(Material.PAPER, 1), "Remove your hat", new String[] { "Click me to remove your hat" });
/*  51: 53 */     for (int i = 0; i < 9; i++) {
/*  52: 55 */       this.hatshop.setOption(i, this.items[i], this.classes[i], new String[] { this.descs[i] });
/*  53:    */     }
/*  54: 57 */     this.hatshop.open(this.p);
/*  55:    */   }
/*  56:    */   
/*  57:    */   public void setupShop(Player p)
/*  58:    */   {
/*  59: 62 */     for (int i = 0; i < this.classes.length; i++) {
/*  60: 64 */       if (PlayerStats.getPlayerHat(p.getName(), this.classes[i])) {
/*  61: 65 */         this.descs[i] = "§aPurchased! Click me to use";
/*  62:    */       } else {
/*  63: 67 */         this.descs[i] = ("§6Price: §a" + this.prices[i] + " gems");
/*  64:    */       }
/*  65:    */     }
/*  66:    */   }
/*  67:    */   
/*  68:    */   public boolean canBuyHat(Player p, String c, int g)
/*  69:    */   {
/*  70: 72 */     if (PlayerStats.getPlayerGems(p.getName()) >= g) {
/*  71: 73 */       return true;
/*  72:    */     }
/*  73: 76 */     p.sendMessage(ChatColor.RED + "You do not have enough Gems to unlock this hat");
/*  74: 77 */     return false;
/*  75:    */   }
/*  76:    */   
/*  77:    */   public void buyHat(Player p, String c, int g)
/*  78:    */   {
/*  79: 82 */     PlayerStats.setPlayerHat(p.getName(), c, g);
/*  80: 83 */     GemScoreboard.addPlayer(p);
/*  81: 84 */     p.sendMessage(ChatColor.GREEN + "You have just bought the " + c);
/*  82:    */   }
/*  83:    */   
/*  84:    */   @SuppressWarnings("deprecation")
public void setHat(String p, String h, int i)
/*  85:    */   {
/*  86: 88 */     Bukkit.getPlayer(p).getInventory().setHelmet(this.items[i]);
/*  87: 89 */     Bukkit.getPlayer(p).sendMessage(ChatColor.GREEN + "You are now wearing the " + h);
/*  88:    */   }
/*  89:    */   
/*  90:    */   public void onOptionClick(String c, int s)
/*  91:    */   {
/*  92: 94 */     if (s == 53)
/*  93:    */     {
/*  94: 96 */       this.p.getInventory().setHelmet(null);
/*  95: 97 */       return;
/*  96:    */     }
/*  97: 99 */     if (this.p.hasPermission("scb.hat." + c.toLowerCase().split(" ")[0])) {
/*  98:101 */       setHat(this.p.getName(), c, s);
/*  99:105 */     } else if (!PlayerStats.getPlayerHat(this.p.getName(), c))
/* 100:    */     {
/* 101:107 */       if (canBuyHat(this.p, c, this.prices[s])) {
/* 102:109 */         buyHat(this.p, c, this.prices[s]);
/* 103:    */       }
/* 104:    */     }
/* 105:    */     else {
/* 106:114 */       setHat(this.p.getName(), c, s);
/* 107:    */     }
/* 108:117 */     HatShopIM.removeFromList(this.p);
/* 109:    */   }
/* 110:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.menus.HatShop
 * JD-Core Version:    0.7.0.1
 */