/*   1:    */ package io.github.toxicbyte.supercraftbrothers;
/*   2:    */ 
/*   3:    */ import io.github.toxicbyte.supercraftbrothers.menus.GemClassShop;

/*   4:    */ import java.io.IOException;
/*   5:    */ import java.util.ArrayList;
/*   6:    */ import java.util.List;

/*   7:    */ import org.bukkit.Bukkit;
/*   8:    */ import org.bukkit.configuration.file.FileConfiguration;
/*  10:    */ 
/*  11:    */ public class PlayerStats
/*  12:    */ {
/*  13:    */   public static int getPlayerGems(String p)
/*  14:    */   {
/*  15: 16 */     return SuperCraftBrothers.getInstance().getPlayerConfig(p).getInt("gems");
/*  16:    */   }
/*  17:    */   
/*  18:    */   @SuppressWarnings("deprecation")
public static void setPlayerGems(String p, int g, int r, boolean rawAdd)
/*  19:    */   {
/*  20: 20 */     FileConfiguration c = SuperCraftBrothers.getInstance().getPlayerConfig(p);
/*  21: 21 */     if (!rawAdd)
/*  22:    */     {
/*  23: 23 */       if ((Bukkit.getPlayer(p).hasPermission("scb.multiplier.two")) && (r < 2)) {
/*  24: 24 */         r = 2;
/*  25:    */       }
/*  26: 25 */       if ((Bukkit.getPlayer(p).hasPermission("scb.multiplier.three")) && (r < 3)) {
/*  27: 26 */         r = 3;
/*  28:    */       }
/*  29: 27 */       if ((Bukkit.getPlayer(p).hasPermission("scb.multiplier.four")) && (r < 4)) {
/*  30: 28 */         r = 4;
/*  31:    */       }
/*  32:    */     }
/*  33: 30 */     c.set("gems", Integer.valueOf(c.getInt("gems") + g * r));
/*  34:    */     try
/*  35:    */     {
/*  36: 32 */       c.save(SuperCraftBrothers.getInstance().getPlayerFile(p));
/*  37: 33 */       GemScoreboard.addPlayer(Bukkit.getPlayer(p));
/*  38:    */     }
/*  39:    */     catch (IOException e)
/*  40:    */     {
/*  41: 35 */       e.printStackTrace();
/*  42:    */     }
/*  43:    */   }
/*  44:    */   
/*  45:    */   public static int getPlayerWins(String p)
/*  46:    */   {
/*  47: 41 */     return SuperCraftBrothers.getInstance().getPlayerConfig(p).getInt("wins");
/*  48:    */   }
/*  49:    */   
/*  50:    */   public static void setPlayerWins(String p, int w)
/*  51:    */   {
/*  52: 45 */     FileConfiguration c = SuperCraftBrothers.getInstance().getPlayerConfig(p);
/*  53: 46 */     c.set("wins", Integer.valueOf(c.getInt("wins") + w));
/*  54:    */     try
/*  55:    */     {
/*  56: 48 */       c.save(SuperCraftBrothers.getInstance().getPlayerFile(p));
/*  57:    */     }
/*  58:    */     catch (IOException e)
/*  59:    */     {
/*  60: 50 */       e.printStackTrace();
/*  61:    */     }
/*  62:    */   }
/*  63:    */   
/*  64:    */   public static int getPlayerRank(String p)
/*  65:    */   {
/*  66: 56 */     String r = SuperCraftBrothers.getInstance().getPlayerConfig(p).getString("rank");
/*  67:    */     String str1;
/*  68: 57 */     switch ((str1 = r).hashCode())
/*  69:    */     {
/*  70:    */     case -2032180703: 
/*  71: 57 */       if (str1.equals("DEFAULT")) 
{
/*  72:    */         break;
/*  73:    */       }
/*  74:    */       break;
/*  75:    */     case 79501: 
/*  76: 57 */       if (str1.equals("PRO")) {}
/*  77:    */       break;
/*  78:    */     case 84989: 
/*  79: 57 */       if (!str1.equals("VIP"))
/*  80:    */       {
/*  82: 60 */         return 1;
/*  83:    */       }
/*  90:    */       break;
/*  91:    */     }
/*  93: 66 */     return 1;
/*  94:    */   }
/*  95:    */   
/*  96:    */   @SuppressWarnings("deprecation")
public static void setPlayerRank(String p, int r)
/*  97:    */   {
/*  98: 70 */     FileConfiguration c = SuperCraftBrothers.getInstance().getPlayerConfig(p);
/*  99: 71 */     switch (r)
/* 100:    */     {
/* 101:    */     case 1: 
/* 102: 74 */       c.set("rank", "DEFAULT");
/* 103: 75 */       break;
/* 104:    */     case 2: 
/* 105: 77 */       c.set("rank", "VIP");
/* 106: 78 */       break;
/* 107:    */     case 3: 
/* 108: 80 */       c.set("rank", "PRO");
/* 109:    */     }
/* 110:    */     try
/* 111:    */     {
/* 112: 84 */       c.save(SuperCraftBrothers.getInstance().getPlayerFile(p));
/* 113: 85 */       GemScoreboard.addPlayer(Bukkit.getPlayer(p));
/* 114:    */     }
/* 115:    */     catch (IOException e)
/* 116:    */     {
/* 117: 87 */       e.printStackTrace();
/* 118:    */     }
/* 119:    */   }
/* 120:    */   
/* 121:    */   public static int getPlayerKills(String p)
/* 122:    */   {
/* 123: 94 */     return SuperCraftBrothers.getInstance().getPlayerConfig(p).getInt("kills");
/* 124:    */   }
/* 125:    */   
/* 126:    */   public static void setPlayerKills(String p, int k)
/* 127:    */   {
/* 128: 98 */     FileConfiguration c = SuperCraftBrothers.getInstance().getPlayerConfig(p);
/* 129: 99 */     c.set("kills", Integer.valueOf(c.getInt("kills") + k));
/* 130:    */     try
/* 131:    */     {
/* 132:101 */       c.save(SuperCraftBrothers.getInstance().getPlayerFile(p));
/* 133:    */     }
/* 134:    */     catch (IOException e)
/* 135:    */     {
/* 136:103 */       e.printStackTrace();
/* 137:    */     }
/* 138:    */   }
/* 139:    */   
/* 140:    */   public static int getPlayerDeaths(String p)
/* 141:    */   {
/* 142:110 */     return SuperCraftBrothers.getInstance().getPlayerConfig(p).getInt("deaths");
/* 143:    */   }
/* 144:    */   
/* 145:    */   public static void setPlayerDeaths(String p, int d)
/* 146:    */   {
/* 147:114 */     FileConfiguration c = SuperCraftBrothers.getInstance().getPlayerConfig(p);
/* 148:115 */     c.set("deaths", Integer.valueOf(c.getInt("deaths") + d));
/* 149:    */     try
/* 150:    */     {
/* 151:117 */       c.save(SuperCraftBrothers.getInstance().getPlayerFile(p));
/* 152:    */     }
/* 153:    */     catch (IOException e)
/* 154:    */     {
/* 155:119 */       e.printStackTrace();
/* 156:    */     }
/* 157:    */   }
/* 158:    */   
/* 159:    */   public static boolean getPlayerClass(String p, String c)
/* 160:    */   {
/* 161:126 */     return SuperCraftBrothers.getInstance().getPlayerConfig(p).getBoolean("unlocked_characters." + c);
/* 162:    */   }
/* 163:    */   
/* 164:    */   public static List<String> getAllClasses(String p)
/* 165:    */   {
/* 166:130 */     List<String> classes = new ArrayList<String>();
/* 167:131 */     for (String s : GemClassShop.classes) {
/* 168:133 */       if (getPlayerClass(p, s)) {
/* 169:135 */         classes.add(s);
/* 170:    */       }
/* 171:    */     }
/* 172:138 */     return classes;
/* 173:    */   }
/* 174:    */   
/* 175:    */   public static boolean getPlayerHat(String p, String h)
/* 176:    */   {
/* 177:142 */     return SuperCraftBrothers.getInstance().getPlayerConfig(p).getBoolean("hat." + h);
/* 178:    */   }
/* 179:    */   
/* 180:    */   public static void setPlayerHat(String p, String h, int g)
/* 181:    */   {
/* 182:146 */     FileConfiguration c = SuperCraftBrothers.getInstance().getPlayerConfig(p);
/* 183:147 */     c.set("hat." + h, Boolean.valueOf(true));
/* 184:148 */     c.set("gems", Integer.valueOf(c.getInt("gems") - g));
/* 185:    */     try
/* 186:    */     {
/* 187:150 */       c.save(SuperCraftBrothers.getInstance().getPlayerFile(p));
/* 188:    */     }
/* 189:    */     catch (IOException e)
/* 190:    */     {
/* 191:152 */       e.printStackTrace();
/* 192:    */     }
/* 193:    */   }
/* 194:    */   
/* 195:    */   public static boolean getPlayerSpawning(String p)
/* 196:    */   {
/* 197:157 */     return SuperCraftBrothers.getInstance().getPlayerConfig(p).getBoolean("quitingame");
/* 198:    */   }
/* 199:    */   
/* 200:    */   public static void setPlayerSpawning(String p, boolean b)
/* 201:    */   {
/* 202:161 */     FileConfiguration c = SuperCraftBrothers.getInstance().getPlayerConfig(p);
/* 203:162 */     c.set("quitingame", Boolean.valueOf(b));
/* 204:    */     try
/* 205:    */     {
/* 206:164 */       c.save(SuperCraftBrothers.getInstance().getPlayerFile(p));
/* 207:    */     }
/* 208:    */     catch (IOException e)
/* 209:    */     {
/* 210:166 */       e.printStackTrace();
/* 211:    */     }
/* 212:    */   }
/* 213:    */   
/* 214:    */   public static boolean getPlayerMap(String p, String m)
/* 215:    */   {
/* 216:171 */     return SuperCraftBrothers.getInstance().getPlayerConfig(p).getBoolean("map." + m);
/* 217:    */   }
/* 218:    */ }


/* Location:           C:\Users\Martin\Desktop\scb.jar
 * Qualified Name:     com.gmail.samsun469.supercraftbrothers.PlayerStats
 * JD-Core Version:    0.7.0.1
 */